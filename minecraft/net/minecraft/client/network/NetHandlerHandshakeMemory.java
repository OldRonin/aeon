package net.minecraft.client.network;

import me.lordpankake.aeon.hook.NetworkManageHook;
import net.minecraft.network.EnumConnectionState;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.handshake.INetHandlerHandshakeServer;
import net.minecraft.network.handshake.client.C00Handshake;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.NetHandlerLoginServer;
import net.minecraft.util.IChatComponent;
import org.apache.commons.lang3.Validate;

public class NetHandlerHandshakeMemory implements INetHandlerHandshakeServer
{
	private final MinecraftServer mcServer;
	private final NetworkManageHook netManager;
	private static final String __OBFID = "CL_00001445";

	public NetHandlerHandshakeMemory(MinecraftServer p_i45287_1_, NetworkManageHook p_i45287_2_)
	{
		this.mcServer = p_i45287_1_;
		this.netManager = p_i45287_2_;
	}

	/**
	 * There are two recognized intentions for initiating a handshake: logging
	 * in and acquiring server status. The NetworkManager's protocol will be
	 * reconfigured according to the specified intention, although a
	 * login-intention must pass a versioncheck or receive a disconnect
	 * otherwise
	 */
	@Override
	public void processHandshake(C00Handshake p_147383_1_)
	{
		this.netManager.setConnectionState(p_147383_1_.func_149594_c());
	}

	/**
	 * Invoked when disconnecting, the parameter is a ChatComponent describing
	 * the reason for termination
	 */
	@Override
	public void onDisconnect(IChatComponent p_147231_1_)
	{
	}

	/**
	 * Allows validation of the connection state transition. Parameters: from,
	 * to (connection state). Typically throws IllegalStateException or
	 * UnsupportedOperationException if validation fails
	 */
	@Override
	public void onConnectionStateTransition(EnumConnectionState p_147232_1_, EnumConnectionState p_147232_2_)
	{
		Validate.validState(p_147232_2_ == EnumConnectionState.LOGIN || p_147232_2_ == EnumConnectionState.STATUS, "Unexpected protocol " + p_147232_2_, new Object[0]);
		switch (NetHandlerHandshakeMemory.SwitchEnumConnectionState.field_151263_a[p_147232_2_.ordinal()])
		{
		case 1:
			this.netManager.setNetHandler(new NetHandlerLoginServer(this.mcServer, this.netManager));
			break;
		case 2:
			throw new UnsupportedOperationException("NYI");
		default:
		}
	}

	/**
	 * For scheduled network tasks. Used in NetHandlerPlayServer to send
	 * keep-alive packets and in NetHandlerLoginServer for a login-timeout
	 */
	@Override
	public void onNetworkTick()
	{
	}
	static final class SwitchEnumConnectionState
	{
		static final int[] field_151263_a = new int[EnumConnectionState.values().length];
		private static final String __OBFID = "CL_00001446";
		static
		{
			try
			{
				field_151263_a[EnumConnectionState.LOGIN.ordinal()] = 1;
			} catch (final NoSuchFieldError var2)
			{
				;
			}
			try
			{
				field_151263_a[EnumConnectionState.STATUS.ordinal()] = 2;
			} catch (final NoSuchFieldError var1)
			{
				;
			}
		}
	}
}
