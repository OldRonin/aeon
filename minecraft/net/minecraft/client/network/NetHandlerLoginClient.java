package net.minecraft.client.network;

import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.AuthenticationUnavailableException;
import com.mojang.authlib.exceptions.InvalidCredentialsException;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import java.math.BigInteger;
import java.security.PublicKey;
import java.util.UUID;
import javax.crypto.SecretKey;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.hook.NetworkManageHook;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiDisconnected;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.network.EnumConnectionState;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.login.INetHandlerLoginClient;
import net.minecraft.network.login.client.C01PacketEncryptionResponse;
import net.minecraft.network.login.server.S00PacketDisconnect;
import net.minecraft.network.login.server.S01PacketEncryptionRequest;
import net.minecraft.network.login.server.S02PacketLoginSuccess;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.CryptManager;
import net.minecraft.util.IChatComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NetHandlerLoginClient implements INetHandlerLoginClient
{
	private static final Logger logger = LogManager.getLogger();
	private final Minecraft minecraft;
	private final GuiScreen field_147395_c;
	private final NetworkManageHook networkManager;
	private static final String __OBFID = "CL_00000876";

	public NetHandlerLoginClient(NetworkManageHook p_i45059_1_, Minecraft p_i45059_2_, GuiScreen p_i45059_3_)
	{
		this.networkManager = p_i45059_1_;
		this.minecraft = p_i45059_2_;
		this.field_147395_c = p_i45059_3_;
	}

	@Override
	public void handleEncryptionRequest(S01PacketEncryptionRequest encryptedRequest)
	{
		final SecretKey var2 = CryptManager.createNewSharedKey();
		final String var3 = encryptedRequest.func_149609_c();
		final PublicKey var4 = encryptedRequest.getPublicKey();
		final String var5 = new BigInteger(CryptManager.getServerIdHash(var3, var4, var2)).toString(16);
		final boolean nullServerData = this.minecraft.getServerData() == null || !this.minecraft.getServerData().func_152585_d();
		try
		{
			this.getSessionService().joinServer(this.minecraft.getSession().getGameProfile(), this.minecraft.getSession().getToken(), var5);
		} catch (final AuthenticationUnavailableException var8)
		{
			if (nullServerData)
			{
				this.networkManager.closeChannel(new ChatComponentTranslation("disconnect.loginFailedInfo", new Object[]
						{ new ChatComponentTranslation("disconnect.loginFailedInfo.serversUnavailable", new Object[0]) }));
				return;
			}
		} catch (final InvalidCredentialsException var9)
		{
			if (nullServerData)
			{
				this.networkManager.closeChannel(new ChatComponentTranslation("disconnect.loginFailedInfo", new Object[]
						{ new ChatComponentTranslation("disconnect.loginFailedInfo.invalidSession", new Object[0]) }));
				return;
			}
		} catch (final AuthenticationException var10)
		{
			if (nullServerData)
			{
				this.networkManager.closeChannel(new ChatComponentTranslation("disconnect.loginFailedInfo", new Object[]
						{ var10.getMessage() }));
				return;
			}
		}
		this.networkManager.scheduleOutboundPacket(new C01PacketEncryptionResponse(var2, var4, encryptedRequest.func_149607_e()), new GenericFutureListener[]
				{ new GenericFutureListener()
				{
					private static final String __OBFID = "CL_00000877";

					@Override
					public void operationComplete(Future p_operationComplete_1_)
					{
						NetHandlerLoginClient.this.networkManager.enableEncryption(var2);
					}
				} });
	}

	private MinecraftSessionService getSessionService()
	{
		return new YggdrasilAuthenticationService(this.minecraft.getProxy(), UUID.randomUUID().toString()).createMinecraftSessionService();
	}

	@Override
	public void handleLoginSuccess(S02PacketLoginSuccess p_147390_1_)
	{
		this.networkManager.setConnectionState(EnumConnectionState.PLAY);
	}

	/**
	 * Invoked when disconnecting, the parameter is a ChatComponent describing
	 * the reason for termination
	 */
	@Override
	public void onDisconnect(IChatComponent p_147231_1_)
	{
		// UUID uuidFromPlayerID =
		// UUIDTypeAdapter.fromString(Minecraft.getMinecraft().session.getPlayerID());
		// Log.write("onDisconnect: " + s);
		Log.write("onDisconnect: " + p_147231_1_.getUnformattedText());
		this.minecraft.displayGuiScreen(new GuiDisconnected(this.field_147395_c, "connect.failed", p_147231_1_));
	}

	/**
	 * Allows validation of the connection state transition. Parameters: from,
	 * to (connection state). Typically throws IllegalStateException or
	 * UnsupportedOperationException if validation fails
	 */
	@Override
	public void onConnectionStateTransition(EnumConnectionState p_147232_1_, EnumConnectionState p_147232_2_)
	{
		logger.debug("Switching protocol from " + p_147232_1_ + " to " + p_147232_2_);
		if (p_147232_2_ == EnumConnectionState.PLAY)
		{
			this.networkManager.setNetHandler(new NetHandlerPlayClient(this.minecraft, this.field_147395_c, this.networkManager));
		}
	}

	/**
	 * For scheduled network tasks. Used in NetHandlerPlayServer to send
	 * keep-alive packets and in NetHandlerLoginServer for a login-timeout
	 */
	@Override
	public void onNetworkTick()
	{
	}

	@Override
	public void handleDisconnect(S00PacketDisconnect p_147388_1_)
	{
		this.networkManager.closeChannel(p_147388_1_.func_149603_c());
	}
}
