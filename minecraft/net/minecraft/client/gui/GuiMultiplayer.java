package net.minecraft.client.gui;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import java.util.List;
import me.lordpankake.aeon.core.AeonSettings;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.GuiConnecting;
import net.minecraft.client.multiplayer.ServerData;
import net.minecraft.client.multiplayer.ServerList;
import net.minecraft.client.network.LanServerDetector;
import net.minecraft.client.network.OldServerPinger;
import net.minecraft.client.resources.I18n;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.input.Keyboard;

public class GuiMultiplayer extends GuiScreen implements GuiYesNoCallback
{
	private static final Logger logger = LogManager.getLogger();
	private final OldServerPinger field_146797_f = new OldServerPinger();
	private final GuiScreen field_146798_g;
	private ServerSelectionList serverList;
	private ServerList field_146804_i;
	private GuiButton field_146810_r;
	private GuiButton field_146809_s;
	private GuiButton field_146808_t;
	private GuiButton changeProtocal;
	private boolean field_146807_u;
	private boolean field_146806_v;
	private boolean field_146805_w;
	private boolean field_146813_x;
	private String field_146812_y;
	private ServerData field_146811_z;
	private LanServerDetector.LanServerList lanServers;
	private LanServerDetector.ThreadLanServerFind field_146800_B;
	private boolean field_146801_C;
	private static final String __OBFID = "CL_00000814";

	public GuiMultiplayer(GuiScreen p_i1040_1_)
	{
		this.field_146798_g = p_i1040_1_;
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui()
	{
		Keyboard.enableRepeatEvents(true);
		this.buttonList.clear();
		if (!this.field_146801_C)
		{
			this.field_146801_C = true;
			this.field_146804_i = new ServerList(this.mc);
			this.field_146804_i.loadServerList();
			this.lanServers = new LanServerDetector.LanServerList();
			try
			{
				this.field_146800_B = new LanServerDetector.ThreadLanServerFind(this.lanServers);
				this.field_146800_B.start();
			} catch (final Exception var2)
			{
				logger.warn("Unable to start LAN server detection: " + var2.getMessage());
			}
			this.serverList = new ServerSelectionList(this, this.mc, this.width, this.height, 32, this.height - 64, 36);
			this.serverList.func_148195_a(this.field_146804_i);
		} else
		{
			this.serverList.func_148122_a(this.width, this.height, 32, this.height - 64);
		}
		this.addButtons();
	}

	public void addButtons()
	{
		/*
		 * this.buttonList.add(this.changeProtocal = new GuiButton(69,
		 * this.width / 2 + 4 + 76, this.height - 52, 75, 20,
		 * "Change Version")); this.buttonList.add(this.field_146810_r = new
		 * GuiButton(7, this.width / 2 - 154, this.height - 28, 70, 20,
		 * I18n.format("selectServer.edit", new Object[0])));
		 * this.buttonList.add(this.field_146808_t = new GuiButton(2, this.width
		 * / 2 - 74, this.height - 28, 70, 20,
		 * I18n.format("selectServer.delete", new Object[0])));
		 * this.buttonList.add(this.field_146809_s = new GuiButton(1, this.width
		 * / 2 - 154, this.height - 52, 70, 20,
		 * I18n.format("selectServer.select", new Object[0])));
		 * this.buttonList.add(new GuiButton(4, this.width / 2 - 74, this.height
		 * - 52, 70, 20, I18n.format("selectServer.direct", new Object[0])));
		 * this.buttonList.add(new GuiButton(3, this.width / 2 + 4, this.height
		 * - 52, 70, 20, I18n.format("selectServer.add", new Object[0])));
		 * this.buttonList.add(new GuiButton(8, this.width / 2 + 4, this.height
		 * - 28, 70, 20, I18n.format("selectServer.refresh", new Object[0])));
		 * this.buttonList.add(new GuiButton(0, this.width / 2 + 4 + 76,
		 * this.height - 28, 75, 20, I18n.format("gui.cancel", new Object[0])));
		 */
		this.buttonList.add(this.field_146810_r = new GuiButton(7, this.width / 2 - 154, this.height - 28, 70, 20, I18n.format("selectServer.edit", new Object[0])));
		this.buttonList.add(this.field_146808_t = new GuiButton(2, this.width / 2 - 74, this.height - 28, 70, 20, I18n.format("selectServer.delete", new Object[0])));
		this.buttonList.add(this.field_146809_s = new GuiButton(1, this.width / 2 - 154, this.height - 52, 100, 20, I18n.format("selectServer.select", new Object[0])));
		this.buttonList.add(new GuiButton(4, this.width / 2 - 50, this.height - 52, 100, 20, I18n.format("selectServer.direct", new Object[0])));
		this.buttonList.add(new GuiButton(3, this.width / 2 + 4 + 50, this.height - 52, 100, 20, I18n.format("selectServer.add", new Object[0])));
		this.buttonList.add(new GuiButton(8, this.width / 2 + 4, this.height - 28, 70, 20, I18n.format("selectServer.refresh", new Object[0])));
		this.buttonList.add(new GuiButton(0, this.width / 2 + 4 + 76, this.height - 28, 75, 20, I18n.format("gui.cancel", new Object[0])));
		this.func_146790_a(this.serverList.func_148193_k());
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen()
	{
		super.updateScreen();
		if (this.lanServers.getWasUpdated())
		{
			final List var1 = this.lanServers.getLanServers();
			this.lanServers.setWasNotUpdated();
			this.serverList.func_148194_a(var1);
		}
		this.field_146797_f.func_147223_a();
	}

	/**
	 * "Called when the screen is unloaded. Used to disable keyboard repeat events."
	 */
	@Override
	public void onGuiClosed()
	{
		Keyboard.enableRepeatEvents(false);
		if (this.field_146800_B != null)
		{
			this.field_146800_B.interrupt();
			this.field_146800_B = null;
		}
		this.field_146797_f.func_147226_b();
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		if (button.enabled)
		{
			final GuiListExtended.IGuiListEntry var2 = this.serverList.func_148193_k() < 0 ? null : this.serverList.func_148180_b(this.serverList.func_148193_k());
			if (button.buttonID == 2 && var2 instanceof ServerListEntryNormal)
			{
				final String var9 = ((ServerListEntryNormal) var2).func_148296_a().serverName;
				if (var9 != null)
				{
					this.field_146807_u = true;
					final String var4 = I18n.format("selectServer.deleteQuestion", new Object[0]);
					final String var5 = "\'" + var9 + "\' " + I18n.format("selectServer.deleteWarning", new Object[0]);
					final String var6 = I18n.format("selectServer.deleteButton", new Object[0]);
					final String var7 = I18n.format("gui.cancel", new Object[0]);
					final GuiYesNo var8 = new GuiYesNo(this, var4, var5, var6, var7, this.serverList.func_148193_k());
					this.mc.displayGuiScreen(var8);
				}
			} else if (button.buttonID == 69)
			{
				if (AeonSettings.proto == 4)
				{
					AeonSettings.proto = 5;
					AeonSettings.mcVersion = "1.7.10";
				} else
				{
					AeonSettings.proto = 4;
					AeonSettings.mcVersion = "1.7.5";
				}
				Minecraft.getMinecraft().setTitle("Minecraft " + AeonSettings.mcVersion);
				this.func_146792_q();
			} else if (button.buttonID == 1)
			{
				this.func_146796_h();
			} else if (button.buttonID == 4)
			{
				this.field_146813_x = true;
				this.mc.displayGuiScreen(new GuiScreenServerList(this, this.field_146811_z = new ServerData(I18n.format("selectServer.defaultName", new Object[0]), "")));
			} else if (button.buttonID == 3)
			{
				this.field_146806_v = true;
				this.mc.displayGuiScreen(new GuiScreenAddServer(this, this.field_146811_z = new ServerData(I18n.format("selectServer.defaultName", new Object[0]), "")));
			} else if (button.buttonID == 7 && var2 instanceof ServerListEntryNormal)
			{
				this.field_146805_w = true;
				final ServerData var3 = ((ServerListEntryNormal) var2).func_148296_a();
				this.field_146811_z = new ServerData(var3.serverName, var3.serverIP);
				this.field_146811_z.func_152583_a(var3);
				this.mc.displayGuiScreen(new GuiScreenAddServer(this, this.field_146811_z));
			} else if (button.buttonID == 0)
			{
				this.mc.displayGuiScreen(this.field_146798_g);
			} else if (button.buttonID == 8)
			{
				this.func_146792_q();
			}
		}
	}

	private void func_146792_q()
	{
		this.mc.displayGuiScreen(new GuiMultiplayer(this.field_146798_g));
	}

	@Override
	public void confirmClicked(boolean p_73878_1_, int p_73878_2_)
	{
		final GuiListExtended.IGuiListEntry var3 = this.serverList.func_148193_k() < 0 ? null : this.serverList.func_148180_b(this.serverList.func_148193_k());
		if (this.field_146807_u)
		{
			this.field_146807_u = false;
			if (p_73878_1_ && var3 instanceof ServerListEntryNormal)
			{
				this.field_146804_i.removeServerData(this.serverList.func_148193_k());
				this.field_146804_i.saveServerList();
				this.serverList.func_148192_c(-1);
				this.serverList.func_148195_a(this.field_146804_i);
			}
			this.mc.displayGuiScreen(this);
		} else if (this.field_146813_x)
		{
			this.field_146813_x = false;
			if (p_73878_1_)
			{
				this.func_146791_a(this.field_146811_z);
			} else
			{
				this.mc.displayGuiScreen(this);
			}
		} else if (this.field_146806_v)
		{
			this.field_146806_v = false;
			if (p_73878_1_)
			{
				this.field_146804_i.addServerData(this.field_146811_z);
				this.field_146804_i.saveServerList();
				this.serverList.func_148192_c(-1);
				this.serverList.func_148195_a(this.field_146804_i);
			}
			this.mc.displayGuiScreen(this);
		} else if (this.field_146805_w)
		{
			this.field_146805_w = false;
			if (p_73878_1_ && var3 instanceof ServerListEntryNormal)
			{
				final ServerData var4 = ((ServerListEntryNormal) var3).func_148296_a();
				var4.serverName = this.field_146811_z.serverName;
				var4.serverIP = this.field_146811_z.serverIP;
				var4.func_152583_a(this.field_146811_z);
				this.field_146804_i.saveServerList();
				this.serverList.func_148195_a(this.field_146804_i);
			}
			this.mc.displayGuiScreen(this);
		}
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	 protected void keyTyped(char p_73869_1_, int p_73869_2_)
	{
		final int var3 = this.serverList.func_148193_k();
		final GuiListExtended.IGuiListEntry var4 = var3 < 0 ? null : this.serverList.func_148180_b(var3);
		if (p_73869_2_ == 63)
		{
			this.func_146792_q();
		} else
		{
			if (var3 >= 0)
			{
				if (p_73869_2_ == 200)
				{
					if (isShiftKeyDown())
					{
						if (var3 > 0 && var4 instanceof ServerListEntryNormal)
						{
							this.field_146804_i.swapServers(var3, var3 - 1);
							this.func_146790_a(this.serverList.func_148193_k() - 1);
							this.serverList.func_148145_f(-this.serverList.func_148146_j());
							this.serverList.func_148195_a(this.field_146804_i);
						}
					} else if (var3 > 0)
					{
						this.func_146790_a(this.serverList.func_148193_k() - 1);
						this.serverList.func_148145_f(-this.serverList.func_148146_j());
						if (this.serverList.func_148180_b(this.serverList.func_148193_k()) instanceof ServerListEntryLanScan)
						{
							if (this.serverList.func_148193_k() > 0)
							{
								this.func_146790_a(this.serverList.getSize() - 1);
								this.serverList.func_148145_f(-this.serverList.func_148146_j());
							} else
							{
								this.func_146790_a(-1);
							}
						}
					} else
					{
						this.func_146790_a(-1);
					}
				} else if (p_73869_2_ == 208)
				{
					if (isShiftKeyDown())
					{
						if (var3 < this.field_146804_i.countServers() - 1)
						{
							this.field_146804_i.swapServers(var3, var3 + 1);
							this.func_146790_a(var3 + 1);
							this.serverList.func_148145_f(this.serverList.func_148146_j());
							this.serverList.func_148195_a(this.field_146804_i);
						}
					} else if (var3 < this.serverList.getSize())
					{
						this.func_146790_a(this.serverList.func_148193_k() + 1);
						this.serverList.func_148145_f(this.serverList.func_148146_j());
						if (this.serverList.func_148180_b(this.serverList.func_148193_k()) instanceof ServerListEntryLanScan)
						{
							if (this.serverList.func_148193_k() < this.serverList.getSize() - 1)
							{
								this.func_146790_a(this.serverList.getSize() + 1);
								this.serverList.func_148145_f(this.serverList.func_148146_j());
							} else
							{
								this.func_146790_a(-1);
							}
						}
					} else
					{
						this.func_146790_a(-1);
					}
				} else if (p_73869_2_ != 28 && p_73869_2_ != 156)
				{
					super.keyTyped(p_73869_1_, p_73869_2_);
				} else
				{
					this.actionPerformed((GuiButton) this.buttonList.get(2));
				}
			} else
			{
				super.keyTyped(p_73869_1_, p_73869_2_);
			}
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	 public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_)
	{
		this.field_146812_y = null;
		this.drawDefaultBackground();
		this.serverList.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
		this.drawCenteredString(this.fontRendererObj, I18n.format("multiplayer.title", new Object[0]), this.width / 2, 20, 16777215);
		super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
		if (this.field_146812_y != null)
		{
			this.func_146283_a(Lists.newArrayList(Splitter.on("\n").split(this.field_146812_y)), p_73863_1_, p_73863_2_);
		}
	}

	public void func_146796_h()
	{
		final GuiListExtended.IGuiListEntry var1 = this.serverList.func_148193_k() < 0 ? null : this.serverList.func_148180_b(this.serverList.func_148193_k());
		if (var1 instanceof ServerListEntryNormal)
		{
			this.func_146791_a(((ServerListEntryNormal) var1).func_148296_a());
		} else if (var1 instanceof ServerListEntryLanDetected)
		{
			final LanServerDetector.LanServer var2 = ((ServerListEntryLanDetected) var1).func_148289_a();
			this.func_146791_a(new ServerData(var2.getServerMotd(), var2.getServerIpPort(), true));
		}
	}

	private void func_146791_a(ServerData p_146791_1_)
	{
		this.mc.displayGuiScreen(new GuiConnecting(this, this.mc, p_146791_1_));
	}

	public void func_146790_a(int p_146790_1_)
	{
		this.serverList.func_148192_c(p_146790_1_);
		final GuiListExtended.IGuiListEntry var2 = p_146790_1_ < 0 ? null : this.serverList.func_148180_b(p_146790_1_);
		this.field_146809_s.enabled = false;
		this.field_146810_r.enabled = false;
		this.field_146808_t.enabled = false;
		if (var2 != null && !(var2 instanceof ServerListEntryLanScan))
		{
			this.field_146809_s.enabled = true;
			if (var2 instanceof ServerListEntryNormal)
			{
				this.field_146810_r.enabled = true;
				this.field_146808_t.enabled = true;
			}
		}
	}

	public OldServerPinger func_146789_i()
	{
		return this.field_146797_f;
	}

	public void func_146793_a(String p_146793_1_)
	{
		this.field_146812_y = p_146793_1_;
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	  protected void mouseClicked(int p_73864_1_, int p_73864_2_, int p_73864_3_)
	{
		super.mouseClicked(p_73864_1_, p_73864_2_, p_73864_3_);
		this.serverList.func_148179_a(p_73864_1_, p_73864_2_, p_73864_3_);
	}

	@Override
	  protected void mouseMovedOrUp(int p_146286_1_, int p_146286_2_, int p_146286_3_)
	{
		super.mouseMovedOrUp(p_146286_1_, p_146286_2_, p_146286_3_);
		this.serverList.func_148181_b(p_146286_1_, p_146286_2_, p_146286_3_);
	}

	public ServerList func_146795_p()
	{
		return this.field_146804_i;
	}
}
