package net.minecraft.client.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public abstract class GuiSlot
{
	private final Minecraft minecraft;
	protected int width;
	private int height;
	protected int top;
	protected int bottom;
	protected int right;
	protected int left;
	protected final int slotHeight;
	private int scrollUpButtonID;
	private int scrollDownButtonID;
	protected int mouseX;
	protected int mouseY;
	protected boolean field_148163_i = true;
	private float field_148157_o = -2.0F;
	private float field_148170_p;
	private float field_148169_q;
	private int field_148168_r = -1;
	private long field_148167_s;
	private boolean showSelectionBox = true;
	private boolean field_148165_u;
	protected int field_148160_j;
	private boolean field_148164_v = true;
	private static final String __OBFID = "CL_00000679";

	public GuiSlot(Minecraft p_i1052_1_, int p_i1052_2_, int p_i1052_3_, int p_i1052_4_, int p_i1052_5_, int p_i1052_6_)
	{
		this.minecraft = p_i1052_1_;
		this.width = p_i1052_2_;
		this.height = p_i1052_3_;
		this.top = p_i1052_4_;
		this.bottom = p_i1052_5_;
		this.slotHeight = p_i1052_6_;
		this.left = 0;
		this.right = p_i1052_2_;
	}

	public void func_148122_a(int p_148122_1_, int p_148122_2_, int p_148122_3_, int p_148122_4_)
	{
		this.width = p_148122_1_;
		this.height = p_148122_2_;
		this.top = p_148122_3_;
		this.bottom = p_148122_4_;
		this.left = 0;
		this.right = p_148122_1_;
	}

	public void func_148130_a(boolean p_148130_1_)
	{
		this.showSelectionBox = p_148130_1_;
	}

	protected void func_148133_a(boolean p_148133_1_, int p_148133_2_)
	{
		this.field_148165_u = p_148133_1_;
		this.field_148160_j = p_148133_2_;
		if (!p_148133_1_)
		{
			this.field_148160_j = 0;
		}
	}

	protected abstract int getSize();

	protected abstract void elementClicked(int p_148144_1_, boolean p_148144_2_, int p_148144_3_, int p_148144_4_);

	protected abstract boolean isSelected(int p_148131_1_);

	protected int getContentHeight()
	{
		return this.getSize() * this.slotHeight + this.field_148160_j;
	}

	protected abstract void drawBackground();

	protected abstract void drawSlot(int p_148126_1_, int p_148126_2_, int p_148126_3_, int p_148126_4_, Tessellator p_148126_5_, int p_148126_6_, int p_148126_7_);

	protected void func_148129_a(int p_148129_1_, int p_148129_2_, Tessellator p_148129_3_)
	{
	}

	protected void func_148132_a(int p_148132_1_, int p_148132_2_)
	{
	}

	protected void func_148142_b(int p_148142_1_, int p_148142_2_)
	{
	}

	public int func_148124_c(int p_148124_1_, int p_148124_2_)
	{
		final int var3 = this.left + this.width / 2 - this.func_148139_c() / 2;
		final int var4 = this.left + this.width / 2 + this.func_148139_c() / 2;
		final int var5 = p_148124_2_ - this.top - this.field_148160_j + (int) this.field_148169_q - 4;
		final int var6 = var5 / this.slotHeight;
		return p_148124_1_ < this.func_148137_d() && p_148124_1_ >= var3 && p_148124_1_ <= var4 && var6 >= 0 && var5 >= 0 && var6 < this.getSize() ? var6 : -1;
	}

	public void registerScrollButtons(int up, int down)
	{
		this.scrollUpButtonID = up;
		this.scrollDownButtonID = down;
	}

	private void func_148121_k()
	{
		int var1 = this.func_148135_f();
		if (var1 < 0)
		{
			var1 /= 2;
		}
		if (!this.field_148163_i && var1 < 0)
		{
			var1 = 0;
		}
		if (this.field_148169_q < 0.0F)
		{
			this.field_148169_q = 0.0F;
		}
		if (this.field_148169_q > var1)
		{
			this.field_148169_q = var1;
		}
	}

	public int func_148135_f()
	{
		return this.getContentHeight() - (this.bottom - this.top - 4);
	}

	public int func_148148_g()
	{
		return (int) this.field_148169_q;
	}

	public boolean func_148141_e(int p_148141_1_)
	{
		return p_148141_1_ >= this.top && p_148141_1_ <= this.bottom;
	}

	public void func_148145_f(int p_148145_1_)
	{
		this.field_148169_q += p_148145_1_;
		this.func_148121_k();
		this.field_148157_o = -2.0F;
	}

	public void func_148147_a(GuiButton p_148147_1_)
	{
		if (p_148147_1_.enabled)
		{
			if (p_148147_1_.buttonID == this.scrollUpButtonID)
			{
				this.field_148169_q -= this.slotHeight * 2 / 3;
				this.field_148157_o = -2.0F;
				this.func_148121_k();
			} else if (p_148147_1_.buttonID == this.scrollDownButtonID)
			{
				this.field_148169_q += this.slotHeight * 2 / 3;
				this.field_148157_o = -2.0F;
				this.func_148121_k();
			}
		}
	}

	public void drawScreen(int x, int y, float p_148128_3_)
	{
		this.mouseX = x;
		this.mouseY = y;
		this.drawBackground();
		final int var4 = this.getSize();
		final int var5 = this.func_148137_d();
		final int var6 = var5 + 6;
		int var9;
		int var10;
		int var13;
		int var19;
		if (x > this.left && x < this.right && y > this.top && y < this.bottom)
		{
			if (Mouse.isButtonDown(0) && this.func_148125_i())
			{
				if (this.field_148157_o == -1.0F)
				{
					boolean var15 = true;
					if (y >= this.top && y <= this.bottom)
					{
						final int var8 = this.width / 2 - this.func_148139_c() / 2;
						var9 = this.width / 2 + this.func_148139_c() / 2;
						var10 = y - this.top - this.field_148160_j + (int) this.field_148169_q - 4;
						final int var11 = var10 / this.slotHeight;
						if (x >= var8 && x <= var9 && var11 >= 0 && var10 >= 0 && var11 < var4)
						{
							final boolean var12 = var11 == this.field_148168_r && Minecraft.getSystemTime() - this.field_148167_s < 250L;
							this.elementClicked(var11, var12, x, y);
							this.field_148168_r = var11;
							this.field_148167_s = Minecraft.getSystemTime();
						} else if (x >= var8 && x <= var9 && var10 < 0)
						{
							this.func_148132_a(x - var8, y - this.top + (int) this.field_148169_q - 4);
							var15 = false;
						}
						if (x >= var5 && x <= var6)
						{
							this.field_148170_p = -1.0F;
							var19 = this.func_148135_f();
							if (var19 < 1)
							{
								var19 = 1;
							}
							var13 = (int) ((float) ((this.bottom - this.top) * (this.bottom - this.top)) / (float) this.getContentHeight());
							if (var13 < 32)
							{
								var13 = 32;
							}
							if (var13 > this.bottom - this.top - 8)
							{
								var13 = this.bottom - this.top - 8;
							}
							this.field_148170_p /= (float) (this.bottom - this.top - var13) / (float) var19;
						} else
						{
							this.field_148170_p = 1.0F;
						}
						if (var15)
						{
							this.field_148157_o = y;
						} else
						{
							this.field_148157_o = -2.0F;
						}
					} else
					{
						this.field_148157_o = -2.0F;
					}
				} else if (this.field_148157_o >= 0.0F)
				{
					this.field_148169_q -= (y - this.field_148157_o) * this.field_148170_p;
					this.field_148157_o = y;
				}
			} else
			{
				for (; !this.minecraft.gameSettings.touchscreen && Mouse.next(); this.minecraft.currentScreen.handleMouseInput())
				{
					int var7 = Mouse.getEventDWheel();
					if (var7 != 0)
					{
						if (var7 > 0)
						{
							var7 = -1;
						} else if (var7 < 0)
						{
							var7 = 1;
						}
						this.field_148169_q += var7 * this.slotHeight / 2;
					}
				}
				this.field_148157_o = -1.0F;
			}
		}
		this.func_148121_k();
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_FOG);
		final Tessellator var16 = Tessellator.instance;
		this.minecraft.getTextureManager().bindTexture(Gui.optionsBackground);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		final float var17 = 32.0F;
		var16.startDrawingQuads();
		var16.setColorOpaque_I(2105376);
		var16.addVertexWithUV(this.left, this.bottom, 0.0D, this.left / var17, (this.bottom + (int) this.field_148169_q) / var17);
		var16.addVertexWithUV(this.right, this.bottom, 0.0D, this.right / var17, (this.bottom + (int) this.field_148169_q) / var17);
		var16.addVertexWithUV(this.right, this.top, 0.0D, this.right / var17, (this.top + (int) this.field_148169_q) / var17);
		var16.addVertexWithUV(this.left, this.top, 0.0D, this.left / var17, (this.top + (int) this.field_148169_q) / var17);
		var16.draw();
		var9 = this.left + this.width / 2 - this.func_148139_c() / 2 + 2;
		var10 = this.top + 4 - (int) this.field_148169_q;
		if (this.field_148165_u)
		{
			this.func_148129_a(var9, var10, var16);
		}
		this.func_148120_b(var9, var10, x, y);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		final byte var18 = 4;
		this.func_148136_c(0, this.top, 255, 255);
		this.func_148136_c(this.bottom, this.height, 255, 255);
		GL11.glEnable(GL11.GL_BLEND);
		OpenGlHelper.glBlendFunc(770, 771, 0, 1);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		var16.startDrawingQuads();
		var16.setColorRGBA_I(0, 0);
		var16.addVertexWithUV(this.left, this.top + var18, 0.0D, 0.0D, 1.0D);
		var16.addVertexWithUV(this.right, this.top + var18, 0.0D, 1.0D, 1.0D);
		var16.setColorRGBA_I(0, 255);
		var16.addVertexWithUV(this.right, this.top, 0.0D, 1.0D, 0.0D);
		var16.addVertexWithUV(this.left, this.top, 0.0D, 0.0D, 0.0D);
		var16.draw();
		var16.startDrawingQuads();
		var16.setColorRGBA_I(0, 255);
		var16.addVertexWithUV(this.left, this.bottom, 0.0D, 0.0D, 1.0D);
		var16.addVertexWithUV(this.right, this.bottom, 0.0D, 1.0D, 1.0D);
		var16.setColorRGBA_I(0, 0);
		var16.addVertexWithUV(this.right, this.bottom - var18, 0.0D, 1.0D, 0.0D);
		var16.addVertexWithUV(this.left, this.bottom - var18, 0.0D, 0.0D, 0.0D);
		var16.draw();
		var19 = this.func_148135_f();
		if (var19 > 0)
		{
			var13 = (this.bottom - this.top) * (this.bottom - this.top) / this.getContentHeight();
			if (var13 < 32)
			{
				var13 = 32;
			}
			if (var13 > this.bottom - this.top - 8)
			{
				var13 = this.bottom - this.top - 8;
			}
			int var14 = (int) this.field_148169_q * (this.bottom - this.top - var13) / var19 + this.top;
			if (var14 < this.top)
			{
				var14 = this.top;
			}
			var16.startDrawingQuads();
			var16.setColorRGBA_I(0, 255);
			var16.addVertexWithUV(var5, this.bottom, 0.0D, 0.0D, 1.0D);
			var16.addVertexWithUV(var6, this.bottom, 0.0D, 1.0D, 1.0D);
			var16.addVertexWithUV(var6, this.top, 0.0D, 1.0D, 0.0D);
			var16.addVertexWithUV(var5, this.top, 0.0D, 0.0D, 0.0D);
			var16.draw();
			var16.startDrawingQuads();
			var16.setColorRGBA_I(8421504, 255);
			var16.addVertexWithUV(var5, var14 + var13, 0.0D, 0.0D, 1.0D);
			var16.addVertexWithUV(var6, var14 + var13, 0.0D, 1.0D, 1.0D);
			var16.addVertexWithUV(var6, var14, 0.0D, 1.0D, 0.0D);
			var16.addVertexWithUV(var5, var14, 0.0D, 0.0D, 0.0D);
			var16.draw();
			var16.startDrawingQuads();
			var16.setColorRGBA_I(12632256, 255);
			var16.addVertexWithUV(var5, var14 + var13 - 1, 0.0D, 0.0D, 1.0D);
			var16.addVertexWithUV(var6 - 1, var14 + var13 - 1, 0.0D, 1.0D, 1.0D);
			var16.addVertexWithUV(var6 - 1, var14, 0.0D, 1.0D, 0.0D);
			var16.addVertexWithUV(var5, var14, 0.0D, 0.0D, 0.0D);
			var16.draw();
		}
		this.func_148142_b(x, y);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glShadeModel(GL11.GL_FLAT);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glDisable(GL11.GL_BLEND);
	}

	public void func_148143_b(boolean p_148143_1_)
	{
		this.field_148164_v = p_148143_1_;
	}

	public boolean func_148125_i()
	{
		return this.field_148164_v;
	}

	public int func_148139_c()
	{
		return 220;
	}

	protected void func_148120_b(int p_148120_1_, int p_148120_2_, int p_148120_3_, int p_148120_4_)
	{
		final int var5 = this.getSize();
		final Tessellator var6 = Tessellator.instance;
		for (int var7 = 0; var7 < var5; ++var7)
		{
			final int var8 = p_148120_2_ + var7 * this.slotHeight + this.field_148160_j;
			final int var9 = this.slotHeight - 4;
			if (var8 <= this.bottom && var8 + var9 >= this.top)
			{
				if (this.showSelectionBox && this.isSelected(var7))
				{
					final int var10 = this.left + this.width / 2 - this.func_148139_c() / 2;
					final int var11 = this.left + this.width / 2 + this.func_148139_c() / 2;
					GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
					GL11.glDisable(GL11.GL_TEXTURE_2D);
					var6.startDrawingQuads();
					var6.setColorOpaque_I(8421504);
					var6.addVertexWithUV(var10, var8 + var9 + 2, 0.0D, 0.0D, 1.0D);
					var6.addVertexWithUV(var11, var8 + var9 + 2, 0.0D, 1.0D, 1.0D);
					var6.addVertexWithUV(var11, var8 - 2, 0.0D, 1.0D, 0.0D);
					var6.addVertexWithUV(var10, var8 - 2, 0.0D, 0.0D, 0.0D);
					var6.setColorOpaque_I(0);
					var6.addVertexWithUV(var10 + 1, var8 + var9 + 1, 0.0D, 0.0D, 1.0D);
					var6.addVertexWithUV(var11 - 1, var8 + var9 + 1, 0.0D, 1.0D, 1.0D);
					var6.addVertexWithUV(var11 - 1, var8 - 1, 0.0D, 1.0D, 0.0D);
					var6.addVertexWithUV(var10 + 1, var8 - 1, 0.0D, 0.0D, 0.0D);
					var6.draw();
					GL11.glEnable(GL11.GL_TEXTURE_2D);
				}
				this.drawSlot(var7, p_148120_1_, var8, var9, var6, p_148120_3_, p_148120_4_);
			}
		}
	}

	protected int func_148137_d()
	{
		return this.width / 2 + 124;
	}

	private void func_148136_c(int p_148136_1_, int p_148136_2_, int p_148136_3_, int p_148136_4_)
	{
		final Tessellator var5 = Tessellator.instance;
		this.minecraft.getTextureManager().bindTexture(Gui.optionsBackground);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		final float var6 = 32.0F;
		var5.startDrawingQuads();
		var5.setColorRGBA_I(4210752, p_148136_4_);
		var5.addVertexWithUV(this.left, p_148136_2_, 0.0D, 0.0D, p_148136_2_ / var6);
		var5.addVertexWithUV(this.left + this.width, p_148136_2_, 0.0D, this.width / var6, p_148136_2_ / var6);
		var5.setColorRGBA_I(4210752, p_148136_3_);
		var5.addVertexWithUV(this.left + this.width, p_148136_1_, 0.0D, this.width / var6, p_148136_1_ / var6);
		var5.addVertexWithUV(this.left, p_148136_1_, 0.0D, 0.0D, p_148136_1_ / var6);
		var5.draw();
	}

	public void func_148140_g(int p_148140_1_)
	{
		this.left = p_148140_1_;
		this.right = p_148140_1_ + this.width;
	}

	public int func_148146_j()
	{
		return this.slotHeight;
	}
}
