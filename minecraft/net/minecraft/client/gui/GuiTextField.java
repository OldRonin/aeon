package net.minecraft.client.gui;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ChatAllowedCharacters;
import org.lwjgl.opengl.GL11;

public class GuiTextField extends Gui
{
	private final FontRenderer fontRenderer;
	private final int x;
	private final int y;
	private final int width;
	private final int height;
	private String text = "";
	private int maxStringLength = 32;
	private int cursorTickThingy;
	private boolean field_146215_m = true;
	private boolean field_146212_n = true;
	private boolean field_146213_o;
	private boolean field_146226_p = true;
	private int field_146225_q;
	private int field_146224_r;
	private int field_146223_s;
	private int field_146222_t = 14737632;
	private int field_146221_u = 7368816;
	private boolean field_146220_v = true;
	private static final String __OBFID = "CL_00000670";

	public GuiTextField(FontRenderer fontrend, int x, int y, int width, int height)
	{
		this.fontRenderer = fontrend;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	/**
	 * Increments the cursor counter
	 */
	public void updateCursorCounter()
	{
		++this.cursorTickThingy;
	}

	/**
	 * Sets the text of the textbox
	 */
	public void setText(String text)
	{
		if (text.length() > this.maxStringLength)
		{
			this.text = text.substring(0, this.maxStringLength);
		} else
		{
			this.text = text;
		}
		this.func_146202_e();
	}

	/**
	 * Returns the contents of the textbox
	 */
	public String getText()
	{
		return this.text;
	}

	public String func_146207_c()
	{
		final int var1 = this.field_146224_r < this.field_146223_s ? this.field_146224_r : this.field_146223_s;
		final int var2 = this.field_146224_r < this.field_146223_s ? this.field_146223_s : this.field_146224_r;
		return this.text.substring(var1, var2);
	}

	public String filterText(String text, String allowedChars)
	{
		final StringBuilder newtext = new StringBuilder();
		final char[] textChars = text.toCharArray();
		final int length = textChars.length;
		for (int i = 0; i < length; ++i)
		{
			final char currentChar = textChars[i];
			if (allowedChars.contains(currentChar + ""))
			{
				newtext.append(currentChar);
			}
		}
		return newtext.toString();
	}

	public void func_146191_b(String p_146191_1_)
	{
		String var2 = "";
		final String var3 = ChatAllowedCharacters.filerAllowedCharacters(p_146191_1_);
		final int var4 = this.field_146224_r < this.field_146223_s ? this.field_146224_r : this.field_146223_s;
		final int var5 = this.field_146224_r < this.field_146223_s ? this.field_146223_s : this.field_146224_r;
		final int var6 = this.maxStringLength - this.text.length() - (var4 - this.field_146223_s);
		final boolean var7 = false;
		if (this.text.length() > 0)
		{
			var2 = var2 + this.text.substring(0, var4);
		}
		int var8;
		if (var6 < var3.length())
		{
			var2 = var2 + var3.substring(0, var6);
			var8 = var6;
		} else
		{
			var2 = var2 + var3;
			var8 = var3.length();
		}
		if (this.text.length() > 0 && var5 < this.text.length())
		{
			var2 = var2 + this.text.substring(var5);
		}
		this.text = var2;
		this.func_146182_d(var4 - this.field_146223_s + var8);
	}

	public void func_146177_a(int p_146177_1_)
	{
		if (this.text.length() != 0)
		{
			if (this.field_146223_s != this.field_146224_r)
			{
				this.func_146191_b("");
			} else
			{
				this.func_146175_b(this.func_146187_c(p_146177_1_) - this.field_146224_r);
			}
		}
	}

	public void func_146175_b(int p_146175_1_)
	{
		if (this.text.length() != 0)
		{
			if (this.field_146223_s != this.field_146224_r)
			{
				this.func_146191_b("");
			} else
			{
				final boolean var2 = p_146175_1_ < 0;
				final int var3 = var2 ? this.field_146224_r + p_146175_1_ : this.field_146224_r;
				final int var4 = var2 ? this.field_146224_r : this.field_146224_r + p_146175_1_;
				String var5 = "";
				if (var3 >= 0)
				{
					var5 = this.text.substring(0, var3);
				}
				if (var4 < this.text.length())
				{
					var5 = var5 + this.text.substring(var4);
				}
				this.text = var5;
				if (var2)
				{
					this.func_146182_d(p_146175_1_);
				}
			}
		}
	}

	public int func_146187_c(int p_146187_1_)
	{
		return this.func_146183_a(p_146187_1_, this.func_146198_h());
	}

	public int func_146183_a(int p_146183_1_, int p_146183_2_)
	{
		return this.func_146197_a(p_146183_1_, this.func_146198_h(), true);
	}

	public int func_146197_a(int p_146197_1_, int p_146197_2_, boolean p_146197_3_)
	{
		int var4 = p_146197_2_;
		final boolean var5 = p_146197_1_ < 0;
		final int var6 = Math.abs(p_146197_1_);
		for (int var7 = 0; var7 < var6; ++var7)
		{
			if (var5)
			{
				while (p_146197_3_ && var4 > 0 && this.text.charAt(var4 - 1) == 32)
				{
					--var4;
				}
				while (var4 > 0 && this.text.charAt(var4 - 1) != 32)
				{
					--var4;
				}
			} else
			{
				final int var8 = this.text.length();
				var4 = this.text.indexOf(32, var4);
				if (var4 == -1)
				{
					var4 = var8;
				} else
				{
					while (p_146197_3_ && var4 < var8 && this.text.charAt(var4) == 32)
					{
						++var4;
					}
				}
			}
		}
		return var4;
	}

	public void func_146182_d(int p_146182_1_)
	{
		this.func_146190_e(this.field_146223_s + p_146182_1_);
	}

	public void func_146190_e(int p_146190_1_)
	{
		this.field_146224_r = p_146190_1_;
		final int var2 = this.text.length();
		if (this.field_146224_r < 0)
		{
			this.field_146224_r = 0;
		}
		if (this.field_146224_r > var2)
		{
			this.field_146224_r = var2;
		}
		this.func_146199_i(this.field_146224_r);
	}

	public void func_146196_d()
	{
		this.func_146190_e(0);
	}

	public void func_146202_e()
	{
		this.func_146190_e(this.text.length());
	}

	/**
	 * Call this method from your GuiScreen to process the keys into the textbox
	 */
	public boolean textboxKeyTyped(char p_146201_1_, int p_146201_2_)
	{
		if (!this.field_146213_o)
		{
			return false;
		} else
		{
			switch (p_146201_1_)
			{
			case 1:
				this.func_146202_e();
				this.func_146199_i(0);
				return true;
			case 3:
				GuiScreen.setClipboardString(this.func_146207_c());
				return true;
			case 22:
				if (this.field_146226_p)
				{
					this.func_146191_b(GuiScreen.getClipboardString());
				}
				return true;
			case 24:
				GuiScreen.setClipboardString(this.func_146207_c());
				if (this.field_146226_p)
				{
					this.func_146191_b("");
				}
				return true;
			default:
				switch (p_146201_2_)
				{
				case 14:
					if (GuiScreen.isCtrlKeyDown())
					{
						if (this.field_146226_p)
						{
							this.func_146177_a(-1);
						}
					} else if (this.field_146226_p)
					{
						this.func_146175_b(-1);
					}
					return true;
				case 199:
					if (GuiScreen.isShiftKeyDown())
					{
						this.func_146199_i(0);
					} else
					{
						this.func_146196_d();
					}
					return true;
				case 203:
					if (GuiScreen.isShiftKeyDown())
					{
						if (GuiScreen.isCtrlKeyDown())
						{
							this.func_146199_i(this.func_146183_a(-1, this.func_146186_n()));
						} else
						{
							this.func_146199_i(this.func_146186_n() - 1);
						}
					} else if (GuiScreen.isCtrlKeyDown())
					{
						this.func_146190_e(this.func_146187_c(-1));
					} else
					{
						this.func_146182_d(-1);
					}
					return true;
				case 205:
					if (GuiScreen.isShiftKeyDown())
					{
						if (GuiScreen.isCtrlKeyDown())
						{
							this.func_146199_i(this.func_146183_a(1, this.func_146186_n()));
						} else
						{
							this.func_146199_i(this.func_146186_n() + 1);
						}
					} else if (GuiScreen.isCtrlKeyDown())
					{
						this.func_146190_e(this.func_146187_c(1));
					} else
					{
						this.func_146182_d(1);
					}
					return true;
				case 207:
					if (GuiScreen.isShiftKeyDown())
					{
						this.func_146199_i(this.text.length());
					} else
					{
						this.func_146202_e();
					}
					return true;
				case 211:
					if (GuiScreen.isCtrlKeyDown())
					{
						if (this.field_146226_p)
						{
							this.func_146177_a(1);
						}
					} else if (this.field_146226_p)
					{
						this.func_146175_b(1);
					}
					return true;
				default:
					if (ChatAllowedCharacters.isAllowedCharacter(p_146201_1_))
					{
						if (this.field_146226_p)
						{
							this.func_146191_b(Character.toString(p_146201_1_));
						}
						return true;
					} else
					{
						return false;
					}
				}
			}
		}
	}

	/**
	 * Args: x, y, buttonClicked
	 */
	public void mouseClicked(int p_146192_1_, int p_146192_2_, int p_146192_3_)
	{
		final boolean var4 = p_146192_1_ >= this.x && p_146192_1_ < this.x + this.width && p_146192_2_ >= this.y && p_146192_2_ < this.y + this.height;
		if (this.field_146212_n)
		{
			this.setFocused(var4);
		}
		if (this.field_146213_o && p_146192_3_ == 0)
		{
			int var5 = p_146192_1_ - this.x;
			if (this.field_146215_m)
			{
				var5 -= 4;
			}
			final String var6 = this.fontRenderer.trimStringToWidth(this.text.substring(this.field_146225_q), this.func_146200_o());
			this.func_146190_e(this.fontRenderer.trimStringToWidth(var6, var5).length() + this.field_146225_q);
		}
	}

	/**
	 * Draws the textbox
	 */
	public void drawTextBox()
	{
		if (this.func_146176_q())
		{
			if (this.func_146181_i())
			{
				drawRect(this.x - 1, this.y - 1, this.x + this.width + 1, this.y + this.height + 1, -6250336);
				drawRect(this.x, this.y, this.x + this.width, this.y + this.height, -16777216);
			}
			final int var1 = this.field_146226_p ? this.field_146222_t : this.field_146221_u;
			final int var2 = this.field_146224_r - this.field_146225_q;
			int var3 = this.field_146223_s - this.field_146225_q;
			final String var4 = this.fontRenderer.trimStringToWidth(this.text.substring(this.field_146225_q), this.func_146200_o());
			final boolean var5 = var2 >= 0 && var2 <= var4.length();
			final boolean var6 = this.field_146213_o && this.cursorTickThingy / 6 % 2 == 0 && var5;
			final int var7 = this.field_146215_m ? this.x + 4 : this.x;
			final int var8 = this.field_146215_m ? this.y + (this.height - 8) / 2 : this.y;
			int var9 = var7;
			if (var3 > var4.length())
			{
				var3 = var4.length();
			}
			if (var4.length() > 0)
			{
				final String var10 = var5 ? var4.substring(0, var2) : var4;
				var9 = this.fontRenderer.drawStringWithShadow(var10, var7, var8, var1);
			}
			final boolean var13 = this.field_146224_r < this.text.length() || this.text.length() >= this.getMaxStringLength();
			int var11 = var9;
			if (!var5)
			{
				var11 = var2 > 0 ? var7 + this.width : var7;
			} else if (var13)
			{
				var11 = var9 - 1;
				--var9;
			}
			if (var4.length() > 0 && var5 && var2 < var4.length())
			{
				this.fontRenderer.drawStringWithShadow(var4.substring(var2), var9, var8, var1);
			}
			if (var6)
			{
				if (var13)
				{
					Gui.drawRect(var11, var8 - 1, var11 + 1, var8 + 1 + this.fontRenderer.FONT_HEIGHT, -3092272);
				} else
				{
					this.fontRenderer.drawStringWithShadow("_", var11, var8, var1);
				}
			}
			if (var3 != var2)
			{
				final int var12 = var7 + this.fontRenderer.getStringWidth(var4.substring(0, var3));
				this.func_146188_c(var11, var8 - 1, var12 - 1, var8 + 1 + this.fontRenderer.FONT_HEIGHT);
			}
		}
	}

	private void func_146188_c(int p_146188_1_, int p_146188_2_, int p_146188_3_, int p_146188_4_)
	{
		int var5;
		if (p_146188_1_ < p_146188_3_)
		{
			var5 = p_146188_1_;
			p_146188_1_ = p_146188_3_;
			p_146188_3_ = var5;
		}
		if (p_146188_2_ < p_146188_4_)
		{
			var5 = p_146188_2_;
			p_146188_2_ = p_146188_4_;
			p_146188_4_ = var5;
		}
		if (p_146188_3_ > this.x + this.width)
		{
			p_146188_3_ = this.x + this.width;
		}
		if (p_146188_1_ > this.x + this.width)
		{
			p_146188_1_ = this.x + this.width;
		}
		final Tessellator var6 = Tessellator.instance;
		GL11.glColor4f(0.0F, 0.0F, 255.0F, 255.0F);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_COLOR_LOGIC_OP);
		GL11.glLogicOp(GL11.GL_OR_REVERSE);
		var6.startDrawingQuads();
		var6.addVertex(p_146188_1_, p_146188_4_, 0.0D);
		var6.addVertex(p_146188_3_, p_146188_4_, 0.0D);
		var6.addVertex(p_146188_3_, p_146188_2_, 0.0D);
		var6.addVertex(p_146188_1_, p_146188_2_, 0.0D);
		var6.draw();
		GL11.glDisable(GL11.GL_COLOR_LOGIC_OP);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

	public void setMaxStringLength(int width)
	{
		this.maxStringLength = width;
		if (this.text.length() > width)
		{
			this.text = this.text.substring(0, width);
		}
	}

	public int getMaxStringLength()
	{
		return this.maxStringLength;
	}

	public int func_146198_h()
	{
		return this.field_146224_r;
	}

	public boolean func_146181_i()
	{
		return this.field_146215_m;
	}

	public void func_146185_a(boolean p_146185_1_)
	{
		this.field_146215_m = p_146185_1_;
	}

	public void func_146193_g(int p_146193_1_)
	{
		this.field_146222_t = p_146193_1_;
	}

	public void func_146204_h(int p_146204_1_)
	{
		this.field_146221_u = p_146204_1_;
	}

	/**
	 * Sets focus to this gui element
	 */
	public void setFocused(boolean p_146195_1_)
	{
		if (p_146195_1_ && !this.field_146213_o)
		{
			this.cursorTickThingy = 0;
		}
		this.field_146213_o = p_146195_1_;
	}

	/**
	 * Getter for the focused field
	 */
	public boolean isFocused()
	{
		return this.field_146213_o;
	}

	public void func_146184_c(boolean p_146184_1_)
	{
		this.field_146226_p = p_146184_1_;
	}

	public int func_146186_n()
	{
		return this.field_146223_s;
	}

	public int func_146200_o()
	{
		return this.func_146181_i() ? this.width - 8 : this.width;
	}

	public void func_146199_i(int p_146199_1_)
	{
		final int var2 = this.text.length();
		if (p_146199_1_ > var2)
		{
			p_146199_1_ = var2;
		}
		if (p_146199_1_ < 0)
		{
			p_146199_1_ = 0;
		}
		this.field_146223_s = p_146199_1_;
		if (this.fontRenderer != null)
		{
			if (this.field_146225_q > var2)
			{
				this.field_146225_q = var2;
			}
			final int var3 = this.func_146200_o();
			final String var4 = this.fontRenderer.trimStringToWidth(this.text.substring(this.field_146225_q), var3);
			final int var5 = var4.length() + this.field_146225_q;
			if (p_146199_1_ == this.field_146225_q)
			{
				this.field_146225_q -= this.fontRenderer.trimStringToWidth(this.text, var3, true).length();
			}
			if (p_146199_1_ > var5)
			{
				this.field_146225_q += p_146199_1_ - var5;
			} else if (p_146199_1_ <= this.field_146225_q)
			{
				this.field_146225_q -= this.field_146225_q - p_146199_1_;
			}
			if (this.field_146225_q < 0)
			{
				this.field_146225_q = 0;
			}
			if (this.field_146225_q > var2)
			{
				this.field_146225_q = var2;
			}
		}
	}

	public void func_146205_d(boolean p_146205_1_)
	{
		this.field_146212_n = p_146205_1_;
	}

	public boolean func_146176_q()
	{
		return this.field_146220_v;
	}

	public void func_146189_e(boolean p_146189_1_)
	{
		this.field_146220_v = p_146189_1_;
	}

	public int getX()
	{
		return this.x;
	}

	public int getY()
	{
		return this.y;
	}

	public int getWidth()
	{
		return this.width;
	}

	public int getHeight()
	{
		return this.height;
	}
}
