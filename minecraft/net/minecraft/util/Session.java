package net.minecraft.util;

import com.google.common.collect.Maps;
import com.mojang.authlib.Agent;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.InvalidCredentialsException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;
import com.mojang.util.UUIDTypeAdapter;
import java.net.Proxy;
import java.util.Map;
import java.util.UUID;

public class Session
{
	public static Session firstSesstion;
	public final String username;
	private final String playerID;
	private final String token;
	private final Session.Type field_152429_d;
	private static final String __OBFID = "CL_00000659";
	public GameProfile gprof;

	public Session(String username, String playerID, String token, String p_i1098_4_)
	{
		this.username = username;
		this.playerID = playerID;
		this.token = token;
		this.field_152429_d = Session.Type.getSessionType(p_i1098_4_);
	}

	public String getSessionID()
	{
		return "token:" + this.token + ":" + this.playerID;
	}

	public String getPlayerID()
	{
		return this.playerID;
	}

	public String getUsername()
	{
		return this.username;
	}

	public String getToken()
	{
		return this.token;
	}

	public GameProfile getGameProfile()
	{
		try
		{
			final UUID uuidFromPlayerID = UUIDTypeAdapter.fromString(this.getPlayerID());
			gprof = new GameProfile(uuidFromPlayerID, this.getUsername());
			return new GameProfile(uuidFromPlayerID, this.getUsername());
		} catch (final IllegalArgumentException var2)
		{
			return new GameProfile((UUID) null, this.getUsername());
		}
	}

	public Session.Type func_152428_f()
	{
		return this.field_152429_d;
	}
	public static enum Type
	{
		LEGACY("LEGACY", 0, "legacy"), MOJANG("MOJANG", 1, "mojang");
		private static final Map field_152425_c = Maps.newHashMap();
		private final String field_152426_d;
		private static final Session.Type[] $VALUES = new Session.Type[]
				{ LEGACY, MOJANG };
		private static final String __OBFID = "CL_00001851";

		private Type(String p_i1096_1_, int p_i1096_2_, String p_i1096_3_)
		{
			this.field_152426_d = p_i1096_3_;
		}

		public static Session.Type getSessionType(String p_152421_0_)
		{
			return (Session.Type) field_152425_c.get(p_152421_0_.toLowerCase());
		}
		static
		{
			final Session.Type[] var0 = values();
			final int var1 = var0.length;
			for (int var2 = 0; var2 < var1; ++var2)
			{
				final Session.Type var3 = var0[var2];
				field_152425_c.put(var3.field_152426_d, var3);
			}
		}
	}

	public static Session loginPassword(String username, String password)
	{
		if (username == null || username.length() <= 0 || password == null || password.length() <= 0)
		{
			return null;
		}
		final YggdrasilAuthenticationService a = new YggdrasilAuthenticationService(Proxy.NO_PROXY, "");
		final YggdrasilUserAuthentication b = (YggdrasilUserAuthentication) a.createUserAuthentication(Agent.MINECRAFT);
		b.setUsername(username);
		b.setPassword(password);
		try
		{
			b.logIn();
			return new Session(b.getSelectedProfile().getName(), b.getSelectedProfile().getId().toString(), b.getAuthenticatedToken(), "legacy");
		} catch (final InvalidCredentialsException e)
		{
			e.printStackTrace();
		} catch (final AuthenticationException e)
		{
			e.printStackTrace();
		} catch (final Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
