package net.minecraft.server.network;

import me.lordpankake.aeon.core.AeonSettings;
import me.lordpankake.aeon.hook.NetworkManageHook;
import io.netty.util.concurrent.GenericFutureListener;
import net.minecraft.network.EnumConnectionState;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.handshake.INetHandlerHandshakeServer;
import net.minecraft.network.handshake.client.C00Handshake;
import net.minecraft.network.login.server.S00PacketDisconnect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.IChatComponent;

public class NetHandlerHandshakeTCP implements INetHandlerHandshakeServer
{
	private final MinecraftServer field_147387_a;
	private final NetworkManageHook networkManager;
	private static final String __OBFID = "CL_00001456";

	public NetHandlerHandshakeTCP(MinecraftServer p_i45295_1_, NetworkManageHook p_i45295_2_)
	{
		this.field_147387_a = p_i45295_1_;
		this.networkManager = p_i45295_2_;
	}

	/**
	 * There are two recognized intentions for initiating a handshake: logging
	 * in and acquiring server status. The NetworkManager's protocol will be
	 * reconfigured according to the specified intention, although a
	 * login-intention must pass a versioncheck or receive a disconnect
	 * otherwise
	 */
	@Override
	public void processHandshake(C00Handshake handshake)
	{
		switch (NetHandlerHandshakeTCP.SwitchEnumConnectionState.field_151291_a[handshake.func_149594_c().ordinal()])
		{
		case 1:
			this.networkManager.setConnectionState(EnumConnectionState.LOGIN);
			ChatComponentText chatComp;
			if (handshake.getUsedProtocol() > AeonSettings.proto)
			{
				chatComp = new ChatComponentText("Outdated server! I\'m still on 1.7.10");
				this.networkManager.scheduleOutboundPacket(new S00PacketDisconnect(chatComp), new GenericFutureListener[0]);
				this.networkManager.closeChannel(chatComp);
			} else if (handshake.getUsedProtocol() < AeonSettings.proto)
			{
				chatComp = new ChatComponentText("Outdated client! Please use 1.7.10");
				this.networkManager.scheduleOutboundPacket(new S00PacketDisconnect(chatComp), new GenericFutureListener[0]);
				this.networkManager.closeChannel(chatComp);
			} else
			{
				this.networkManager.setNetHandler(new NetHandlerLoginServer(this.field_147387_a, this.networkManager));
			}
			break;
		case 2:
			this.networkManager.setConnectionState(EnumConnectionState.STATUS);
			this.networkManager.setNetHandler(new NetHandlerStatusServer(this.field_147387_a, this.networkManager));
			break;
		default:
			throw new UnsupportedOperationException("Invalid intention " + handshake.func_149594_c());
		}
	}

	/**
	 * Invoked when disconnecting, the parameter is a ChatComponent describing
	 * the reason for termination
	 */
	@Override
	public void onDisconnect(IChatComponent p_147231_1_)
	{
	}

	/**
	 * Allows validation of the connection state transition. Parameters: from,
	 * to (connection state). Typically throws IllegalStateException or
	 * UnsupportedOperationException if validation fails
	 */
	@Override
	public void onConnectionStateTransition(EnumConnectionState p_147232_1_, EnumConnectionState p_147232_2_)
	{
		if (p_147232_2_ != EnumConnectionState.LOGIN && p_147232_2_ != EnumConnectionState.STATUS)
		{
			throw new UnsupportedOperationException("Invalid state " + p_147232_2_);
		}
	}

	/**
	 * For scheduled network tasks. Used in NetHandlerPlayServer to send
	 * keep-alive packets and in NetHandlerLoginServer for a login-timeout
	 */
	@Override
	public void onNetworkTick()
	{
	}
	static final class SwitchEnumConnectionState
	{
		static final int[] field_151291_a = new int[EnumConnectionState.values().length];
		private static final String __OBFID = "CL_00001457";
		static
		{
			try
			{
				field_151291_a[EnumConnectionState.LOGIN.ordinal()] = 1;
			} catch (final NoSuchFieldError var2)
			{
				;
			}
			try
			{
				field_151291_a[EnumConnectionState.STATUS.ordinal()] = 2;
			} catch (final NoSuchFieldError var1)
			{
				;
			}
		}
	}
}
