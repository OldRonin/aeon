package net.minecraft.network.play.client;

import java.io.IOException;
import net.minecraft.network.INetHandler;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.INetHandlerPlayServer;

public class C03PacketPlayer extends Packet
{
	protected double field_149479_a;
	protected double field_149477_b;
	protected double field_149478_c;
	protected double field_149475_d;
	public float yaw;
	public float pitch;
	public boolean onGround;
	protected boolean field_149480_h;
	protected boolean field_149481_i;
	private static final String __OBFID = "CL_00001360";

	public C03PacketPlayer()
	{
	}

	public C03PacketPlayer(boolean p_i45256_1_)
	{
		this.onGround = p_i45256_1_;
	}

	public void processPacket(INetHandlerPlayServer p_148833_1_)
	{
		p_148833_1_.processPlayer(this);
	}

	/**
	 * Reads the raw packet data from the data stream.
	 */
	@Override
	public void readPacketData(PacketBuffer p_148837_1_) throws IOException
	{
		this.onGround = p_148837_1_.readUnsignedByte() != 0;
	}

	/**
	 * Writes the raw packet data to the data stream.
	 */
	@Override
	public void writePacketData(PacketBuffer p_148840_1_) throws IOException
	{
		p_148840_1_.writeByte(this.onGround ? 1 : 0);
	}

	public double func_149464_c()
	{
		return this.field_149479_a;
	}

	public double func_149467_d()
	{
		return this.field_149477_b;
	}

	public double func_149472_e()
	{
		return this.field_149478_c;
	}

	public double func_149471_f()
	{
		return this.field_149475_d;
	}

	public float func_149462_g()
	{
		return this.yaw;
	}

	public float func_149470_h()
	{
		return this.pitch;
	}

	public boolean func_149465_i()
	{
		return this.onGround;
	}

	public boolean func_149466_j()
	{
		return this.field_149480_h;
	}

	public boolean func_149463_k()
	{
		return this.field_149481_i;
	}

	public void func_149469_a(boolean p_149469_1_)
	{
		this.field_149480_h = p_149469_1_;
	}

	@Override
	public void processPacket(INetHandler p_148833_1_)
	{
		this.processPacket((INetHandlerPlayServer) p_148833_1_);
	}
	public static class C04PacketPlayerPosition extends C03PacketPlayer
	{
		private static final String __OBFID = "CL_00001361";

		public C04PacketPlayerPosition()
		{
			this.field_149480_h = true;
		}

		public C04PacketPlayerPosition(double p_i45253_1_, double p_i45253_3_, double p_i45253_5_, double p_i45253_7_, boolean p_i45253_9_)
		{
			this.field_149479_a = p_i45253_1_;
			this.field_149477_b = p_i45253_3_;
			this.field_149475_d = p_i45253_5_;
			this.field_149478_c = p_i45253_7_;
			this.onGround = p_i45253_9_;
			this.field_149480_h = true;
		}

		@Override
		public void readPacketData(PacketBuffer p_148837_1_) throws IOException
		{
			this.field_149479_a = p_148837_1_.readDouble();
			this.field_149477_b = p_148837_1_.readDouble();
			this.field_149475_d = p_148837_1_.readDouble();
			this.field_149478_c = p_148837_1_.readDouble();
			super.readPacketData(p_148837_1_);
		}

		@Override
		public void writePacketData(PacketBuffer p_148840_1_) throws IOException
		{
			p_148840_1_.writeDouble(this.field_149479_a);
			p_148840_1_.writeDouble(this.field_149477_b);
			p_148840_1_.writeDouble(this.field_149475_d);
			p_148840_1_.writeDouble(this.field_149478_c);
			super.writePacketData(p_148840_1_);
		}

		@Override
		public void processPacket(INetHandler p_148833_1_)
		{
			super.processPacket((INetHandlerPlayServer) p_148833_1_);
		}
	}
	public static class C05PacketPlayerLook extends C03PacketPlayer
	{
		private static final String __OBFID = "CL_00001363";

		public C05PacketPlayerLook()
		{
			this.field_149481_i = true;
		}

		public C05PacketPlayerLook(float yaw, float pitch, boolean p_i45255_3_)
		{
			this.yaw = yaw;
			this.pitch = pitch;
			this.onGround = p_i45255_3_;
			this.field_149481_i = true;
		}

		@Override
		public void readPacketData(PacketBuffer p_148837_1_) throws IOException
		{
			this.yaw = p_148837_1_.readFloat();
			this.pitch = p_148837_1_.readFloat();
			super.readPacketData(p_148837_1_);
		}

		@Override
		public void writePacketData(PacketBuffer p_148840_1_) throws IOException
		{
			p_148840_1_.writeFloat(this.yaw);
			p_148840_1_.writeFloat(this.pitch);
			super.writePacketData(p_148840_1_);
		}

		@Override
		public void processPacket(INetHandler p_148833_1_)
		{
			super.processPacket((INetHandlerPlayServer) p_148833_1_);
		}
	}
	public static class C06PacketPlayerPosLook extends C03PacketPlayer
	{
		private static final String __OBFID = "CL_00001362";

		public C06PacketPlayerPosLook()
		{
			this.field_149480_h = true;
			this.field_149481_i = true;
		}

		public C06PacketPlayerPosLook(double p_i45254_1_, double p_i45254_3_, double p_i45254_5_, double p_i45254_7_, float p_i45254_9_, float p_i45254_10_, boolean p_i45254_11_)
		{
			this.field_149479_a = p_i45254_1_;
			this.field_149477_b = p_i45254_3_;
			this.field_149475_d = p_i45254_5_;
			this.field_149478_c = p_i45254_7_;
			this.yaw = p_i45254_9_;
			this.pitch = p_i45254_10_;
			this.onGround = p_i45254_11_;
			this.field_149481_i = true;
			this.field_149480_h = true;
		}

		@Override
		public void readPacketData(PacketBuffer p_148837_1_) throws IOException
		{
			this.field_149479_a = p_148837_1_.readDouble();
			this.field_149477_b = p_148837_1_.readDouble();
			this.field_149475_d = p_148837_1_.readDouble();
			this.field_149478_c = p_148837_1_.readDouble();
			this.yaw = p_148837_1_.readFloat();
			this.pitch = p_148837_1_.readFloat();
			super.readPacketData(p_148837_1_);
		}

		@Override
		public void writePacketData(PacketBuffer p_148840_1_) throws IOException
		{
			p_148840_1_.writeDouble(this.field_149479_a);
			p_148840_1_.writeDouble(this.field_149477_b);
			p_148840_1_.writeDouble(this.field_149475_d);
			p_148840_1_.writeDouble(this.field_149478_c);
			p_148840_1_.writeFloat(this.yaw);
			p_148840_1_.writeFloat(this.pitch);
			super.writePacketData(p_148840_1_);
		}

		@Override
		public void processPacket(INetHandler p_148833_1_)
		{
			super.processPacket((INetHandlerPlayServer) p_148833_1_);
		}
	}
}
