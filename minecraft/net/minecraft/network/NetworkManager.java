package net.minecraft.network;

import com.google.common.collect.Queues;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelException;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.local.LocalChannel;
import io.netty.channel.local.LocalServerChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.TimeoutException;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GenericFutureListener;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.util.Queue;
import javax.crypto.SecretKey;
import me.lordpankake.aeon.hook.NetworkManageHook;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.CryptManager;
import net.minecraft.util.IChatComponent;
import net.minecraft.util.MessageDeserializer;
import net.minecraft.util.MessageDeserializer2;
import net.minecraft.util.MessageSerializer;
import net.minecraft.util.MessageSerializer2;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

public class NetworkManager extends SimpleChannelInboundHandler
{
	private static final Logger logger = LogManager.getLogger();
	public static final Marker logMarkerNetwork = MarkerManager.getMarker("NETWORK");
	public static final Marker logMarkerPackets = MarkerManager.getMarker("NETWORK_PACKETS", logMarkerNetwork);
	public static final Marker field_152461_c = MarkerManager.getMarker("NETWORK_STAT", logMarkerNetwork);
	public static final AttributeKey attrKeyConnectionState = new AttributeKey("protocol");
	public static final AttributeKey attrKeyReceivable = new AttributeKey("receivable_packets");
	public static final AttributeKey attrKeySendable = new AttributeKey("sendable_packets");
	public static final NioEventLoopGroup eventLoops = new NioEventLoopGroup(0, new ThreadFactoryBuilder().setNameFormat("Netty Client IO #%d").setDaemon(true).build());
	public static final NetworkStatistics netStats = new NetworkStatistics();
	/**
	 * Whether this NetworkManager deals with the client or server side of
	 * the connection
	 */
	private final boolean isClientSide;
	/**
	 * The queue for received, unprioritized packets that will be processed at
	 * the earliest opportunity
	 */
	protected final Queue receivedPacketsQueue = Queues.newConcurrentLinkedQueue();
	/** The queue for packets that require transmission */
	protected final Queue outboundPacketsQueue = Queues.newConcurrentLinkedQueue();
	/** The active channel */
	protected Channel channel;
	/** The address of the remote party */
	private SocketAddress socketAddress;
	/** The INetHandler instance responsible for processing received packets */
	protected INetHandler netHandler;
	/**
	 * The current connection state, being one of: HANDSHAKING, PLAY, STATUS,
	 * LOGIN
	 */
	protected EnumConnectionState connectionState;
	/** A String indicating why the network has shutdown. */
	private IChatComponent terminationReason;
	private boolean field_152463_r;
	private static final String __OBFID = "CL_00001240";

	public NetworkManager(boolean p_i45147_1_)
	{
		this.isClientSide = p_i45147_1_;
	}

	@Override
	public void channelActive(ChannelHandlerContext p_channelActive_1_) throws Exception
	{
		super.channelActive(p_channelActive_1_);
		this.channel = p_channelActive_1_.channel();
		this.socketAddress = this.channel.remoteAddress();
		this.setConnectionState(EnumConnectionState.HANDSHAKING);
	}

	/**
	 * Sets the new connection state and registers which packets this channel
	 * may send and receive
	 */
	public void setConnectionState(EnumConnectionState p_150723_1_)
	{
		this.connectionState = (EnumConnectionState) this.channel.attr(attrKeyConnectionState).getAndSet(p_150723_1_);
		this.channel.attr(attrKeyReceivable).set(p_150723_1_.func_150757_a(this.isClientSide));
		this.channel.attr(attrKeySendable).set(p_150723_1_.func_150754_b(this.isClientSide));
		this.channel.config().setAutoRead(true);
		logger.debug("Enabled auto read");
	}

	@Override
	public void channelInactive(ChannelHandlerContext p_channelInactive_1_)
	{
		this.closeChannel(new ChatComponentTranslation("disconnect.endOfStream", new Object[0]));
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext p_exceptionCaught_1_, Throwable p_exceptionCaught_2_)
	{
		ChatComponentTranslation var3;
		if (p_exceptionCaught_2_ instanceof TimeoutException)
		{
			var3 = new ChatComponentTranslation("disconnect.timeout", new Object[0]);
		} else
		{
			var3 = new ChatComponentTranslation("disconnect.genericReason", new Object[]
			{ "Internal Exception: " + p_exceptionCaught_2_ });
		}
		this.closeChannel(var3);
	}

	protected void channelRead0(ChannelHandlerContext p_channelRead0_1_, Packet p_channelRead0_2_)
	{
		if (this.channel.isOpen())
		{
			if (p_channelRead0_2_.hasPriority())
			{
				p_channelRead0_2_.processPacket(this.netHandler);
			} else
			{
				this.receivedPacketsQueue.add(p_channelRead0_2_);
			}
		}
	}

	/**
	 * Sets the NetHandler for this NetworkManager, no checks are made if
	 * this handler is suitable for the particular connection state (protocol)
	 */
	public void setNetHandler(INetHandler p_150719_1_)
	{
		Validate.notNull(p_150719_1_, "packetListener", new Object[0]);
		logger.debug("Set listener of {} to {}", new Object[]
		{ this, p_150719_1_ });
		this.netHandler = p_150719_1_;
	}


	/**
	 * Will commit the packet to the channel. If the current thread 'owns' the
	 * channel it will write and flush the packet, otherwise it will add a task
	 * for the channel eventloop thread to do that.
	 */
	protected void dispatchPacket(final Packet p_150732_1_, final GenericFutureListener[] p_150732_2_)
	{
		final EnumConnectionState var3 = EnumConnectionState.func_150752_a(p_150732_1_);
		final EnumConnectionState var4 = (EnumConnectionState) this.channel.attr(attrKeyConnectionState).get();
		if (var4 != var3)
		{
			logger.debug("Disabled auto read");
			this.channel.config().setAutoRead(false);
		}
		if (this.channel.eventLoop().inEventLoop())
		{
			if (var3 != var4)
			{
				this.setConnectionState(var3);
			}
			this.channel.writeAndFlush(p_150732_1_).addListeners(p_150732_2_).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
		} else
		{
			this.channel.eventLoop().execute(new Runnable()
			{
				private static final String __OBFID = "CL_00001241";

				@Override
				public void run()
				{
					if (var3 != var4)
					{
						NetworkManager.this.setConnectionState(var3);
					}
					NetworkManager.this.channel.writeAndFlush(p_150732_1_).addListeners(p_150732_2_).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
				}
			});
		}
	}

	/**
	 * Will iterate through the outboundPacketQueue and dispatch all Packets
	 */
	protected void flushOutboundQueue()
	{
		if (this.channel != null && this.channel.isOpen())
		{
			while (!this.outboundPacketsQueue.isEmpty())
			{
				final InboundHandlerTuplePacketListener var1 = (InboundHandlerTuplePacketListener) this.outboundPacketsQueue.poll();
				this.dispatchPacket(var1.field_150774_a, var1.field_150773_b);
			}
		}
	}

	/**
	 * Return the InetSocketAddress of the remote endpoint
	 */
	public SocketAddress getSocketAddress()
	{
		return this.socketAddress;
	}

	/**
	 * Closes the channel, the parameter can be used for an exit message (not
	 * certain how it gets sent)
	 */
	public void closeChannel(IChatComponent p_150718_1_)
	{
		if (this.channel.isOpen())
		{
			this.channel.close();
			this.terminationReason = p_150718_1_;
		}
	}

	/**
	 * True if this NetworkManager uses a memory connection (single player
	 * game). False may imply both an active TCP connection or simply no active
	 * connection at all
	 */
	public boolean isLocalChannel()
	{
		return this.channel instanceof LocalChannel || this.channel instanceof LocalServerChannel;
	}

	



	/**
	 * Adds an encoder+decoder to the channel pipeline. The parameter is the
	 * secret key used for encrypted communication
	 */
	public void enableEncryption(SecretKey p_150727_1_)
	{
		this.channel.pipeline().addBefore("splitter", "decrypt", new NettyEncryptingDecoder(CryptManager.func_151229_a(2, p_150727_1_)));
		this.channel.pipeline().addBefore("prepender", "encrypt", new NettyEncryptingEncoder(CryptManager.func_151229_a(1, p_150727_1_)));
		this.field_152463_r = true;
	}

	/**
	 * Returns true if this NetworkManager has an active channel, false
	 * otherwise
	 */
	public boolean isChannelOpen()
	{
		return this.channel != null && this.channel.isOpen();
	}

	/**
	 * Gets the current handler for processing packets
	 */
	public INetHandler getNetHandler()
	{
		return this.netHandler;
	}

	/**
	 * If this channel is closed, returns the exit message, null otherwise.
	 */
	public IChatComponent getExitMessage()
	{
		return this.terminationReason;
	}

	/**
	 * Switches the channel to manual reading modus
	 */
	public void disableAutoRead()
	{
		this.channel.config().setAutoRead(false);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext p_channelRead0_1_, Object p_channelRead0_2_)
	{
		this.channelRead0(p_channelRead0_1_, (Packet) p_channelRead0_2_);
	}
}
