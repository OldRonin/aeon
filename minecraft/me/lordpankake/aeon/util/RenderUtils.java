package me.lordpankake.aeon.util;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_LIGHTING;
import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_LINE_SMOOTH;
import static org.lwjgl.opengl.GL11.GL_LINE_SMOOTH_HINT;
import static org.lwjgl.opengl.GL11.GL_NICEST;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_SMOOTH;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glColor4b;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glHint;
import static org.lwjgl.opengl.GL11.glLineWidth;
import static org.lwjgl.opengl.GL11.glNormal3f;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glScalef;
import static org.lwjgl.opengl.GL11.glScissor;
import static org.lwjgl.opengl.GL11.glShadeModel;
import static org.lwjgl.opengl.GL11.glTexCoord2f;
import static org.lwjgl.opengl.GL11.glTranslated;
import static org.lwjgl.opengl.GL11.glVertex2d;
import static org.lwjgl.opengl.GL11.glVertex3d;
import static org.lwjgl.opengl.GL13.GL_MULTISAMPLE;
import static org.lwjgl.opengl.GL13.GL_SAMPLE_ALPHA_TO_COVERAGE;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Random;
import me.lordpankake.aeon.core.Aeon;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.util.AxisAlignedBB;

public class RenderUtils extends UtilBase
{
	private static final Random random = new Random();

	
	public static void scissorBox(int x, int y, int xend, int yend)
	{
		final int width = xend - x;
		final int height = yend - y;
		final ScaledResolution sr = new ScaledResolution(Minecraft.getMinecraft(), Minecraft.getMinecraft().displayWidth, Minecraft.getMinecraft().displayHeight);
		final int factor = sr.getScaleFactor();
		final int bottomY = sr.getScaledHeight() - yend;
		glScissor(x * factor, bottomY * factor, width * factor, height * factor);
	}

	public static void setupLineSmooth()
	{
		glEnable(GL_BLEND);
		glDisable(GL_LIGHTING);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_LINE_SMOOTH);
		glDisable(GL_TEXTURE_2D);
		glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_MULTISAMPLE);
		glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
		glShadeModel(GL_SMOOTH);
	}

	public static void drawLine(double startX, double startY, double startZ, double endX, double endY, double endZ, float thickness)
	{
		glPushMatrix();
		setupLineSmooth();
		glLineWidth(thickness);
		glBegin(GL_LINES);
		glVertex3d(startX, startY, startZ);
		glVertex3d(endX, endY, endZ);
		glEnd();
		glDisable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_LINE_SMOOTH);
		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_MULTISAMPLE);
		glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
		glPopMatrix();
	}

	public static void drawTexturedModalRect(int par1, int par2, int par3, int par4, int par5, int par6)
	{
		final float var7 = 0.00390625F;
		final float var8 = 0.00390625F;
		final Tessellator var9 = Tessellator.instance;
		var9.startDrawingQuads();
		var9.addVertexWithUV(par1 + 0, par2 + par6, 0, (par3 + 0) * var7, (par4 + par6) * var8);
		var9.addVertexWithUV(par1 + par5, par2 + par6, 0, (par3 + par5) * var7, (par4 + par6) * var8);
		var9.addVertexWithUV(par1 + par5, par2 + 0, 0, (par3 + par5) * var7, (par4 + 0) * var8);
		var9.addVertexWithUV(par1 + 0, par2 + 0, 0, (par3 + 0) * var7, (par4 + 0) * var8);
		var9.draw();
	}

	public static void drawTexturedModalRect(int textureId, int posX, int posY, int width, int height)
	{
		final double halfHeight = height / 2;
		final double halfWidth = width / 2;
		glDisable(GL_CULL_FACE);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glPushMatrix();
		glTranslated(posX + halfWidth, posY + halfHeight, 0);
		glScalef(width, height, 0.0f);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glColor4f(1F, 1F, 1F, 1f);
		glBegin(GL_TRIANGLES);
		glNormal3f(0, 0, 1);
		glTexCoord2f(1, 1);
		glVertex2d(1, 1);
		glTexCoord2f(0, 1);
		glVertex2d(-1, 1);
		glTexCoord2f(0, 0);
		glVertex2d(-1, -1);
		glTexCoord2f(0, 0);
		glVertex2d(-1, -1);
		glTexCoord2f(1, 0);
		glVertex2d(1, -1);
		glTexCoord2f(1, 1);
		glVertex2d(1, 1);
		glEnd();
		glDisable(GL_BLEND);
		glBindTexture(GL_TEXTURE_2D, 0);
		glPopMatrix();
	}

	public static int interpolateColor(int rgba1, int rgba2, float percent)
	{
		final int r1 = rgba1 & 0xFF, g1 = rgba1 >> 8 & 0xFF, b1 = rgba1 >> 16 & 0xFF, a1 = rgba1 >> 24 & 0xFF;
		final int r2 = rgba2 & 0xFF, g2 = rgba2 >> 8 & 0xFF, b2 = rgba2 >> 16 & 0xFF, a2 = rgba2 >> 24 & 0xFF;
		final int r = (int) (r1 < r2 ? r1 + (r2 - r1) * percent : r2 + (r1 - r2) * percent);
		final int g = (int) (g1 < g2 ? g1 + (g2 - g1) * percent : g2 + (g1 - g2) * percent);
		final int b = (int) (b1 < b2 ? b1 + (b2 - b1) * percent : b2 + (b1 - b2) * percent);
		final int a = (int) (a1 < a2 ? a1 + (a2 - a1) * percent : a2 + (a1 - a2) * percent);
		return r | g << 8 | b << 16 | a << 24;
	}

	public static void setColor(Color c)
	{
		glColor4f(c.getRed() / 255f, c.getGreen() / 255f, c.getBlue() / 255f, c.getAlpha() / 255f);
	}

	public static Color toColor(int rgba)
	{
		final int r = rgba & 0xFF, g = rgba >> 8 & 0xFF, b = rgba >> 16 & 0xFF, a = rgba >> 24 & 0xFF;
		return new Color(r, g, b, a);
	}

	public static int toRGBA(Color c)
	{
		return c.getRed() | c.getGreen() << 8 | c.getBlue() << 16 | c.getAlpha() << 24;
	}

	public static void setColor(int rgba)
	{
		final int r = rgba & 0xFF, g = rgba >> 8 & 0xFF, b = rgba >> 16 & 0xFF, a = rgba >> 24 & 0xFF;
		glColor4b((byte) r, (byte) g, (byte) b, (byte) a);
	}

	public static Point calculateMouseLocation()
	{
		final Minecraft minecraft = Minecraft.getMinecraft();
		int scale = minecraft.gameSettings.guiScale;
		if (scale == 0)
		{
			scale = 1000;
		}
		int scaleFactor = 0;
		while (scaleFactor < scale && minecraft.displayWidth / (scaleFactor + 1) >= 320 && minecraft.displayHeight / (scaleFactor + 1) >= 240)
		{
			scaleFactor++;
		}
		return new Point(Mouse.getX() / scaleFactor, minecraft.displayHeight / scaleFactor - Mouse.getY() / scaleFactor - 1);
	}
	
	public static void enableGL3D()
	{
		GL11.glDisable(3008);
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glDisable(3553);
		GL11.glDisable(2929);
		GL11.glDepthMask(false);
		GL11.glEnable(2884);
		mc.entityRenderer.disableLightmap(0.0D);
		/*
		 * if (Settings.isAntiAlias()) { GL11.glEnable(2848); GL11.glHint(3154,
		 * 4354); GL11.glHint(3155, 4354); }
		 */
		GL11.glLineWidth(1.0f);
	}

	public static void disableGL3D()
	{
		GL11.glEnable(3553);
		GL11.glEnable(2929);
		GL11.glDisable(3042);
		GL11.glEnable(3008);
		GL11.glDepthMask(true);
		GL11.glCullFace(1029);
		/*
		 * if (Settings.isAntiAlias()) { GL11.glDisable(2848); GL11.glHint(3154,
		 * 4352); GL11.glHint(3155, 4352); }
		 */
	}

	public static void drawRect(Rectangle rectangle, int color)
	{
		drawRect(rectangle.x, rectangle.y, rectangle.x + rectangle.width, rectangle.y + rectangle.height, color);
	}

	public static void drawRect(double x, double y, double x1, double y1, int color)
	{
		enableGL2D();
		glColor(color);
		drawRect(x, y, x1, y1);
		disableGL2D();
	}

	public static void drawBorderedRect(double x, double y, double x1, double y1, float width, int internalColor, int borderColor)
	{
		enableGL2D();
		glColor(internalColor);
		drawRect(x + width, y + width, x1 - width, y1 - width);
		glColor(borderColor);
		drawRect(x + width, y, x1 - width, y + width);
		drawRect(x, y, x + width, y1);
		drawRect(x1 - width, y, x1, y1);
		drawRect(x + width, y1 - width, x1 - width, y1);
		disableGL2D();
	}

	public static void drawBorderedRect(Rectangle rectangle, float width, int internalColor, int borderColor)
	{
		enableGL2D();
		final double x = rectangle.x;
		final double y = rectangle.y;
		final double x1 = rectangle.x + rectangle.width;
		final double y1 = rectangle.y + rectangle.height;
		glColor(internalColor);
		drawRect(x + width, y + width, x1 - width, y1 - width);
		glColor(borderColor);
		drawRect(x + 1.0D, y, x1 - 1.0D, y + width);
		drawRect(x, y, x + width, y1);
		drawRect(x1 - width, y, x1, y1);
		drawRect(x + 1.0D, y1 - width, x1 - 1.0D, y1);
		disableGL2D();
	}

	public static void drawRect(double x, double y, double x1, double y1, float r, float g, float b, float a)
	{
		enableGL2D();
		GL11.glColor4f(r, g, b, a);
		drawRect(x, y, x1, y1);
		disableGL2D();
	}

	public static void enableGL2D()
	{
		GL11.glDisable(2929);
		GL11.glEnable(3042);
		GL11.glDisable(3553);
		GL11.glBlendFunc(770, 771);
		GL11.glDepthMask(true);
		/*
		 * if (Settings.isAntiAlias()) { GL11.glEnable(2848); GL11.glHint(3154,
		 * 4354); GL11.glHint(3155, 4354); }
		 */
	}

	public static void disableGL2D()
	{
		GL11.glEnable(3553);
		GL11.glDisable(3042);
		GL11.glEnable(2929);
		/*
		 * if (Settings.isAntiAlias()) { GL11.glDisable(2848); GL11.glHint(3154,
		 * 4352); GL11.glHint(3155, 4352); }
		 */
	}

	public static void drawRect(double x, double y, double x1, double y1)
	{
		GL11.glBegin(7);
		GL11.glVertex2d(x, y1);
		GL11.glVertex2d(x1, y1);
		GL11.glVertex2d(x1, y);
		GL11.glVertex2d(x, y);
		GL11.glEnd();
	}

	public static void glColor(Color color)
	{
		GL11.glColor4f(color.getRed() / 255.0F, color.getGreen() / 255.0F, color.getBlue() / 255.0F, color.getAlpha() / 255.0F);
	}

	public static void glColor(int hex)
	{
		final float alpha = (hex >> 24 & 0xFF) / 255.0F;
		final float red = (hex >> 16 & 0xFF) / 255.0F;
		final float green = (hex >> 8 & 0xFF) / 255.0F;
		final float blue = (hex & 0xFF) / 255.0F;
		GL11.glColor4f(red, green, blue, alpha);
	}

	public static Color getRandomColor()
	{
		return getRandomColor(1000, 0.6F);
	}

	public static Color getRandomColor(int saturationRandom, float luminance)
	{
		final float hue = random.nextFloat();
		final float saturation = (random.nextInt(saturationRandom) + 1000) / saturationRandom + 1000.0F;
		return Color.getHSBColor(hue, saturation, luminance);
	}

	public static void draw3DString(FontRenderer fontRenderer, String text, float x, float y, float z, int color)
	{
		GL11.glPushMatrix();
		GL11.glTranslated(x, y, z);
		GL11.glNormal3f(0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-RenderManager.instance.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(RenderManager.instance.playerViewX, 1.0F, 0.0F, 0.0F);
		GL11.glScalef(-0.02666667F, -0.02666667F, 0.02666667F);
		fontRenderer.drawStringWithShadow(text, 0, 0, color);
		GL11.glPopMatrix();
	}

	public static void draw3DStringCentered(FontRenderer fontRenderer, String text, float x, float y, float z, int color)
	{
		GL11.glPushMatrix();
		GL11.glTranslated(x, y, z);
		GL11.glNormal3f(0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-RenderManager.instance.playerViewY, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(RenderManager.instance.playerViewX, 1.0F, 0.0F, 0.0F);
		GL11.glScalef(-0.02666667F, -0.02666667F, 0.02666667F);
		GL11.glEnable(3042);
		GL11.glBlendFunc(770, 771);
		GL11.glEnable(3553);
		fontRenderer.drawStringWithShadow(text, -fontRenderer.getStringWidth(text) / 2, 0, color);
		GL11.glDisable(3553);
		GL11.glPopMatrix();
	}

	public static void drawOutlinedBox(AxisAlignedBB boundingBox)
	{
		if (boundingBox == null)
		{
			return;
		}
		GL11.glBegin(3);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glEnd();
		GL11.glBegin(3);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glEnd();
		GL11.glBegin(1);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glEnd();
	}

	public static void renderCrosses(AxisAlignedBB boundingBox)
	{
		GL11.glBegin(1);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		GL11.glEnd();
	}

	public static void drawBox(AxisAlignedBB boundingBox)
	{
		if (boundingBox == null)
		{
			return;
		}
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.maxY, boundingBox.maxZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glEnd();
		GL11.glBegin(7);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.minZ);
		GL11.glVertex3d(boundingBox.minX, boundingBox.minY, boundingBox.maxZ);
		GL11.glVertex3d(boundingBox.maxX, boundingBox.minY, boundingBox.maxZ);
		GL11.glEnd();
	}

	public static void drawGradientRect(float x, float y, float x1, float y1, int topColor, int bottomColor)
	{
		enableGL2D();
		GL11.glShadeModel(7425);
		GL11.glBegin(7);
		glColor(topColor);
		GL11.glVertex2d(x, y1);
		GL11.glVertex2d(x1, y1);
		glColor(bottomColor);
		GL11.glVertex2d(x1, y);
		GL11.glVertex2d(x, y);
		GL11.glEnd();
		GL11.glShadeModel(7424);
		disableGL2D();
	}

	public static void prepareScissorBox(float x, float y, float x2, float y2)
	{
		final ScaledResolution scaledResolution = Aeon.getInstance().getUtils().getuScreen().getScaledResolution();
		final int factor = scaledResolution.getScaleFactor();
		GL11.glScissor((int) (x * factor), (int) ((scaledResolution.getScaledHeight() - y2) * factor), (int) ((x2 - x) * factor), (int) ((y2 - y) * factor));
	}

	public static void drawBlockESP(double x, double y, double z, float r, float g, float b, float thickness)
	{
		final EntityPlayerSP ep = mc.thePlayer;
		final double d = ep.lastTickPosX + (ep.posX - ep.lastTickPosX) * mc.timer.renderPartialTicks;
		final double d1 = ep.lastTickPosY + (ep.posY - ep.lastTickPosY) * mc.timer.renderPartialTicks;
		final double d2 = ep.lastTickPosZ + (ep.posZ - ep.lastTickPosZ) * mc.timer.renderPartialTicks;
		final double d3 = x - d;
		final double d4 = y - d1;
		final double d5 = z - d2;
		GL11.glPushMatrix();
		GL11.glDepthMask(false);
		beginSmoothLine();
		GL11.glLineWidth(1.0F);
		GL11.glColor4f(r, g, b, thickness);
		drawBox(d3, d4, d5, d3 + 1, d4 + 1, d5 + 1);
		GL11.glColor4f(r, g, b, thickness);
		drawOutlinedBox(d3, d4, d5, d3 + 1, d4 + 1, d5 + 1, 1.6F);
		GL11.glDepthMask(true);
		endSmoothLine();
		GL11.glPopMatrix();
	}

	public static void drawBlockESP2(double x, double y, double z, double x2, double y2, double z2, float r, float g, float b, float thickness)
	{
		final EntityPlayerSP ep = mc.thePlayer;
		final double d = ep.lastTickPosX + (ep.posX - ep.lastTickPosX) * mc.timer.renderPartialTicks;
		final double d1 = ep.lastTickPosY + (ep.posY - ep.lastTickPosY) * mc.timer.renderPartialTicks;
		final double d2 = ep.lastTickPosZ + (ep.posZ - ep.lastTickPosZ) * mc.timer.renderPartialTicks;
		final double leX = x - d;
		final double leY = y - d1;
		final double leZ = z - d2;
		GL11.glPushMatrix();
		GL11.glDepthMask(false);
		beginSmoothLine();
		GL11.glLineWidth(1.0F);
		GL11.glColor4f(r, g, b, thickness);
		final double xWid = Math.abs((x2 - x));
		final double yWid = Math.abs((y2 - y));
		final double zWid = Math.abs((z2 - z));
		//drawBox(xWid, yWid, zWid, leX , leY , leZ );
		GL11.glColor4f(r, g, b, thickness);
		drawOutlinedBox(leX, leY, leZ, xWid, yWid, zWid, 1.6F);
		GL11.glDepthMask(true);
		endSmoothLine();
		GL11.glPopMatrix();
	}

	public static void drawBox(double x, double y, double z, double x2, double y2, double z2)
	{
		glBegin(GL_QUADS);
		glVertex3d(x, y, z);
		glVertex3d(x, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z2);
		glEnd();
		glBegin(GL_QUADS);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z2);
		glEnd();
		glBegin(GL_QUADS);
		glVertex3d(x, y2, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y2, z2);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y2, z);
		glVertex3d(x, y2, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y2, z);
		glEnd();
		glBegin(GL_QUADS);
		glVertex3d(x, y, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y, z);
		glVertex3d(x, y, z2);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y, z);
		glEnd();
		glBegin(GL_QUADS);
		glVertex3d(x, y, z);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z2);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z);
		glEnd();
		glBegin(GL_QUADS);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z2);
		glEnd();
	}

	public static void drawOutlinedBox(double x, double y, double z, double x2, double y2, double z2, float l1)
	{
		glLineWidth(l1);
		glBegin(GL_LINES);
		glVertex3d(x, y, z);
		glVertex3d(x, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z2);
		glEnd();
		glBegin(GL_LINES);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z2);
		glEnd();
		glBegin(GL_LINES);
		glVertex3d(x, y2, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y2, z2);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y2, z);
		glVertex3d(x, y2, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y2, z);
		glEnd();
		glBegin(GL_LINES);
		glVertex3d(x, y, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y, z);
		glVertex3d(x, y, z2);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y, z);
		glEnd();
		glBegin(GL_LINES);
		glVertex3d(x, y, z);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z2);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z);
		glEnd();
		glBegin(GL_LINES);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z2);
		glEnd();
	}

	public static void disableDefaults()
	{
		GL11.glDisable(3042);
		GL11.glEnable(3553);
		GL11.glDisable(2848);
		GL11.glEnable(2896);
		GL11.glEnable(2929);
		GL11.glDisable(32925);
		GL11.glDisable(32926);
		GL11.glDepthMask(true);
		GL11.glDisable(GL13.GL_SAMPLE_ALPHA_TO_COVERAGE);
		GL11.glDisable(GL13.GL_MULTISAMPLE);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_BLEND);
		mc.entityRenderer.enableLightmap(0.0D);
	}

	public static void enableDefaults()
	{
		mc.entityRenderer.disableLightmap(0.0D);
		GL11.glEnable(3042);
		GL11.glDisable(2896);
		GL11.glDisable(2929);
		GL11.glEnable(2848);
		GL11.glDisable(3553);
		GL11.glHint(3154, 4354);
		GL11.glBlendFunc(770, 771);
		GL11.glEnable(32925);
		GL11.glEnable(32926);
		GL11.glShadeModel(7425);
		GL11.glLineWidth(0.8F);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LIGHTING);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL13.GL_MULTISAMPLE);
		GL11.glEnable(GL13.GL_SAMPLE_ALPHA_TO_COVERAGE);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glDepthMask(false);
	}

	public static void beginSmoothLine()
	{
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL13.GL_MULTISAMPLE);
	}

	public static void endSmoothLine()
	{
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glDisable(GL13.GL_MULTISAMPLE);
	}

	public static void drawLightLevel(int xx, int yy, int zz, float rED, float gREEN, float bLUE, float aLPHA)
	{
		final EntityPlayerSP ep = mc.thePlayer;
		final double d = ep.lastTickPosX + (ep.posX - ep.lastTickPosX) * mc.timer.renderPartialTicks;
		final double d1 = ep.lastTickPosY + (ep.posY - ep.lastTickPosY) * mc.timer.renderPartialTicks;
		final double d2 = ep.lastTickPosZ + (ep.posZ - ep.lastTickPosZ) * mc.timer.renderPartialTicks;
		final double d3 = xx - d;
		final double d4 = yy - d1;
		final double d5 = zz - d2;
		GL11.glPushMatrix();
		GL11.glDepthMask(false);
		beginSmoothLine();
		GL11.glLineWidth(1.0F);
		GL11.glColor4f(rED, gREEN, bLUE, aLPHA);
		drawBox(d3, d4 + 1, d5, d3 + 1, d4 + 1, d5 + 1);
		GL11.glColor4f(rED, gREEN, bLUE, aLPHA);
		drawOutlinedBox(d3, d4 + 1, d5, d3 + 1, d4 + 1, d5 + 1, 1.6F);
		GL11.glDepthMask(true);
		endSmoothLine();
		GL11.glPopMatrix();
	}
}
