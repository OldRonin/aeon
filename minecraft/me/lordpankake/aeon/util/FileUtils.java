package me.lordpankake.aeon.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import me.lordpankake.aeon.core.Log;

public class FileUtils
{
	public static List<String> read(File inputFile)
	{
		Log.write("Attempting to read from: " + inputFile.getName());
		if (inputFile.exists())
		{
			FileReader fileReader = null;
			BufferedReader bufferedFileReader = null;
			try
			{
				final List fileContentList = new ArrayList();
				fileReader = new FileReader(inputFile);
				bufferedFileReader = new BufferedReader(fileReader);
				String currentReadLine;
				while ((currentReadLine = bufferedFileReader.readLine()) != null)
				{
					fileContentList.add(currentReadLine);
				}
				Log.write("Reading complete: " + inputFile.getName());
				return fileContentList;
			} catch (final FileNotFoundException e)
			{
				Log.write("Reading FileNotFoundException: " + inputFile.getName());
				try
				{
					if (bufferedFileReader != null)
					{
						bufferedFileReader.close();
					}
					if (fileReader != null)
					{
						fileReader.close();
					}
				} catch (final IOException localIOException1)
				{
				}
			} catch (final IOException e)
			{
				Log.write("Reading IOException, cannot read from: " + inputFile.getName());
				try
				{
					if (bufferedFileReader != null)
					{
						bufferedFileReader.close();
					}
					if (fileReader != null)
					{
						fileReader.close();
					}
				} catch (final IOException localIOException2)
				{
				}
			} finally
			{
				try
				{
					if (bufferedFileReader != null)
					{
						bufferedFileReader.close();
					}
					if (fileReader != null)
					{
						fileReader.close();
					}
				} catch (final IOException localIOException3)
				{
				}
			}
		} else
		{
			final ArrayList<String> x = new ArrayList<String>();
			x.add(" ");
			write(inputFile, x, true);
		}
		Log.write("Reading encountered an error. Returning blank from: " + inputFile.getName());
		final ArrayList<String> x = new ArrayList<String>();
		x.add(" ");
		return x;
	}

	public static void write(File outputFile, List<String> writeContent, boolean override)
	{
		Log.write("Attempting to write to: " + outputFile.getName());
		FileWriter writer;
		try
		{
			ensureExistance(outputFile);
			writer = new FileWriter(outputFile.getAbsoluteFile());
			for (final String str : writeContent)
			{
				writer.write(str + "\n");
			}
			writer.close();
		} catch (final IOException e)
		{
			e.printStackTrace();
		}
		/*
		 * File temp = new File(outputFile.getParent()); if (!temp.exists()) {
		 * temp.mkdirs(); }
		 * 
		 * Log.write("Attempting to write to: " + outputFile.getName());
		 * BufferedWriter bufferedFileWriter = null; FileWriter fileWriter =
		 * null; try { Log.write("Creating file writers for: " +
		 * outputFile.getName()); Log.write("Content: " +
		 * writeContent.toString());
		 * 
		 * fileWriter = new FileWriter(outputFile, !override);
		 * bufferedFileWriter = new BufferedWriter(fileWriter);
		 * 
		 * Log.write("Writing content for: " + outputFile.getName());
		 * 
		 * for (String outputLine : writeContent) {
		 * bufferedFileWriter.write(outputLine); bufferedFileWriter.flush();
		 * bufferedFileWriter.newLine(); }
		 * 
		 * Log.write("Writing complete: " + outputFile.getName()); } catch
		 * (IOException e) { Log.write("Writing IOException: " +
		 * outputFile.getName()); Log.write("Writing IOException: " +
		 * e.getMessage()); try { if (bufferedFileWriter != null) {
		 * bufferedFileWriter.close(); } if (fileWriter != null)
		 * fileWriter.close(); } catch (IOException e2) {
		 * Log.write("Couldn't close the writer: " + outputFile.getName()); } }
		 * finally { try { if (bufferedFileWriter != null) {
		 * bufferedFileWriter.close(); } if (fileWriter != null)
		 * fileWriter.close(); } catch (IOException e) {
		 * Log.write("Writing IOException when closing: " +
		 * outputFile.getName()); } }
		 */
	}

	public static boolean ensureExistance(File targetFile)
	{
		final File temp = new File(targetFile.getParent());
		if (!temp.exists())
		{
			temp.mkdirs();
		}
		if (!targetFile.exists())
		{
			try
			{
				targetFile.createNewFile();
			} catch (final IOException e)
			{
				System.err.println("IOException thrown, can't create " + targetFile.getName());
			}
		}
		return targetFile.exists();
	}
}
