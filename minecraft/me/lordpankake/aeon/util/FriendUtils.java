package me.lordpankake.aeon.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;

public class FriendUtils extends UtilBase
{
	public static ArrayList friends = new ArrayList<String>();

	public boolean containsFriend(String friend)
	{
		if (friends.contains(friend.toLowerCase()))
		{
			return true;
		}
		return false;
	}

	public void addFriend(String swellChap)
	{
		Log.write(swellChap + " added to 'friends'");
		friends.add(swellChap.toLowerCase());
	}

	public void removeFriend(String faggot)
	{
		if (friends.contains(faggot.toLowerCase()))
		{
			friends.remove(faggot.toLowerCase());
			Log.write(faggot + " removed from 'friends'.");
		} else
		{
			Log.write(faggot + " could not be removed from 'friends' because it doesn't contain that name.");
		}
	}

	public void clearFriends()
	{
		friends.clear();
	}

	public List<String> getFriends()
	{
		return friends;
	}

	public void saveFriends()
	{
		final File saveFile = new File("Aeon" + File.separator + "Friends" + File.separator + "friends.txt");
		final ArrayList<String> content = new ArrayList<String>();
		for (final String f : getFriends())
		{
			content.add(f);
		}
		FileUtils.write(saveFile, content, true);
	}

	public void loadFriends()
	{
		final File settings = new File("Aeon" + File.separator + "Friends" + File.separator + "friends.txt");
		Aeon.getInstance().getUtils().getFile();
		final ArrayList<String> content = (ArrayList<String>) FileUtils.read(settings);
		for (final String s : content)
		{
			if (!containsFriend(s))
			{
				addFriend(s);
			}
		}
	}
}
