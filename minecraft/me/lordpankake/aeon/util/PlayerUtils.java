package me.lordpankake.aeon.util;

import net.minecraft.network.play.client.C01PacketChatMessage;
import net.minecraft.util.ChatComponentText;

public class PlayerUtils extends UtilBase
{
	public static void addChatMessage(String s)
	{
		mc.thePlayer.addChatComponentMessage(new ChatComponentText(s));
	}

	public static void sendChatMessage(String s)
	{
		mc.thePlayer.sendQueue.addToSendQueue(new C01PacketChatMessage(s));
	}
}
