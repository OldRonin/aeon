package me.lordpankake.aeon.util;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;

public class ScreenUtils extends UtilBase
{
	private static double zLevel;


	public int getMouseX()
	{
		return Mouse.getX() * getScreenWidth() / mc.displayWidth;
	}

	public int getMouseY()
	{
		return getScreenHeight() - Mouse.getY() * getScreenHeight() / mc.displayHeight - 1;
	}

	public int getScreenWidth()
	{
		return getScaledResolution().getScaledWidth();
	}

	public int getScreenHeight()
	{
		return getScaledResolution().getScaledHeight();
	}

	public ScaledResolution getScaledResolution()
	{
		return new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
	}

	public static void drawTexturedModalRect(int x, int y, int par3, int par4, int width, int height)
	{
		final float var7 = 0.00390625F;
		final float var8 = 0.00390625F;
		final Tessellator var9 = Tessellator.instance;
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		var9.startDrawingQuads();
		var9.addVertexWithUV(x + 0, y + height, 0, (par3 + 0) * var7, (par4 + height) * var8);
		var9.addVertexWithUV(x + width, y + height, 0, (par3 + width) * var7, (par4 + height) * var8);
		var9.addVertexWithUV(x + width, y + 0, 0, (par3 + width) * var7, (par4 + 0) * var8);
		var9.addVertexWithUV(x + 0, y + 0, 0, (par3 + 0) * var7, (par4 + 0) * var8);
		var9.draw();
	}
	
	public static void drawTexturedModalRectD(double x, double y, double par3, double par4, double width, double height)
	{
		final float var7 = 0.00390625F;
		final float var8 = 0.00390625F;
		final Tessellator var9 = Tessellator.instance;
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		var9.startDrawingQuads();
		var9.addVertexWithUV(x + 0, y + height, 0, (par3 + 0) * var7, (par4 + height) * var8);
		var9.addVertexWithUV(x + width, y + height, 0, (par3 + width) * var7, (par4 + height) * var8);
		var9.addVertexWithUV(x + width, y + 0, 0, (par3 + width) * var7, (par4 + 0) * var8);
		var9.addVertexWithUV(x + 0, y + 0, 0, (par3 + 0) * var7, (par4 + 0) * var8);
		var9.draw();
	}

	public static void drawTexturedModalRectNormal(int X, int Y, int par3, int par4, int width, int height)
	{
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		// GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		final float var7 = 0.00390625F;
		final float var8 = 0.00390625F;
		final Tessellator var9 = Tessellator.instance;
		var9.startDrawingQuads();
		var9.addVertexWithUV(X + 0, Y + height, zLevel, (par3 + 0) * var7, (par4 + height) * var8);
		var9.addVertexWithUV(X + width, Y + height, zLevel, (par3 + width) * var7, (par4 + height) * var8);
		var9.addVertexWithUV(X + width, Y + 0, zLevel, (par3 + width) * var7, (par4 + 0) * var8);
		var9.addVertexWithUV(X + 0, Y + 0, zLevel, (par3 + 0) * var7, (par4 + 0) * var8);
		var9.draw();
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
	}

	public void drawFullscreenTexture(ResourceLocation resLoc)
	{
		final ScaledResolution scaledResolution = new ScaledResolution(UtilBase.mc, UtilBase.mc.displayWidth, UtilBase.mc.displayHeight);
		final int width = scaledResolution.getScaledWidth();
		final int height = scaledResolution.getScaledHeight();
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		UtilBase.mc.getTextureManager().bindTexture(resLoc);
		final Tessellator var3 = Tessellator.instance;
		var3.startDrawingQuads();
		var3.addVertexWithUV(0.0D, height, -90.0D, 0.0D, 1.0D);
		var3.addVertexWithUV(width, height, -90.0D, 1.0D, 1.0D);
		var3.addVertexWithUV(width, 0.0D, -90.0D, 1.0D, 0.0D);
		var3.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
		var3.draw();
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
	}
}
