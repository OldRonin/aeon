package me.lordpankake.aeon.util;
public final class NanoTimerUtils extends TimerUtils
{
	@Override
	public long getCurrentTime()
	{
		return System.nanoTime() / 1000000L;
	}
}
