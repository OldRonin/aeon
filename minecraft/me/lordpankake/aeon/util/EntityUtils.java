package me.lordpankake.aeon.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.core.handling.module.Initlizer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.StringUtils;

public class EntityUtils extends UtilBase
{
	public PriorityType priorityType = PriorityType.CLOSEST_MOUSE;

	public List<EntityLivingBase> getLivingEntities()
	{
		synchronized (mc.theWorld.loadedEntityList)
		{
			final List tempList = Collections.synchronizedList(new ArrayList());
			for (int var2 = 0; var2 < mc.theWorld.loadedEntityList.size(); ++var2)
			{
				final Entity e = (Entity) mc.theWorld.loadedEntityList.get(var2);
				if (e instanceof EntityLivingBase)
				{
					if (e != mc.thePlayer)
					{
						tempList.add(e);
					}
				}
			}
			return tempList;
		}
	}

	public List<EntityPlayer> getPlayers()
	{
		final List tempList = Collections.synchronizedList(new ArrayList());
		for (final Entity e : getLivingEntities())
		{
			if (e instanceof EntityPlayer)
			{
				tempList.add(e);
			}
		}
		return tempList;
	}

	public boolean canAttackFromAngle(EntityLivingBase target, double angle)
	{
		final double x = target.posX - mc.thePlayer.posX;
		final double z = target.posZ - mc.thePlayer.posZ;
		final float newYaw = (float) (Math.toDegrees(Math.atan2(z, x)) - 90);
		final float currentYaw = mc.thePlayer.rotationYaw;
		double difference = getDistanceBetweenAngles(newYaw, currentYaw);
		if (difference >= angle && difference <= 180)
		{
			return false;
		}
		difference = 360 - difference;
		if (difference >= angle && difference <= 180)
		{
			return false;
		}
		return true;
	}
	
	public float getDistanceBetweenAngles(final float par1, final float par2)
	{
		return Math.abs(par1 % 360 - par2 % 360) % 360;
	}

	public EntityLivingBase getClosestEntity(Class<? extends EntityLivingBase> entityType)
	{
		EntityLivingBase closestSpecifiedEntity = null;
		for (final EntityLivingBase entity : getLivingEntities())
		{
			if (isAliveNotUs(entity))
			{
				if (entity.getClass() == entityType)
				{
					if (closestSpecifiedEntity != null)
					{
						if (isCloser(closestSpecifiedEntity, entity))
						{
							closestSpecifiedEntity = entity;
						}
					} else
					{
						closestSpecifiedEntity = entity;
					}
				}
			}
		}
		return closestSpecifiedEntity;
	}

	public boolean isBiggerThreat(EntityLivingBase currentEntity, EntityLivingBase otherEntity, float distance, float extender)
	{
		boolean isPriority = false;
		final boolean currentIsThreat = isPlayer(currentEntity) || isMob(currentEntity);
		final boolean otherIsThreat = isPlayer(otherEntity) || isMob(otherEntity);
		if (!isWithinDistance(otherEntity, distance + extender))
		{
			return false;
		}
		if (!isWithinDistance(currentEntity, distance) && isWithinDistance(otherEntity, distance))
		{
			return true;
		}
		isPriority = isCloser(currentEntity, otherEntity);
		return isPriority;
	}

	public boolean isAliveNotUs(EntityLivingBase entity)
	{
		if (entity != null)
		{
			if (entity != mc.thePlayer && entity.getHealth() > 0F)
			{
				return true;
			} else
			{
				return false;
			}
		}
		return entity != null;
	}

	public boolean hasBetterArmor(EntityLivingBase currentEntity, EntityLivingBase otherEntity)
	{
		final ItemStack[] currentEntityArmor = getEntityArmor(currentEntity);
		final ItemStack[] otherEntityArmor = getEntityArmor(otherEntity);
		return false;
	}

	public ItemStack[] getEntityArmor(EntityLivingBase entity)
	{
		final ItemStack[] entityArmor = new ItemStack[4];
		if (entity instanceof EntityLivingBase)
		{
			final EntityLivingBase entityLivingBase = entity;
			for (int i = 0; i < 4; i++)
			{
				entityArmor[i] = entityLivingBase.getEquipmentInSlot(i);
			}
		}
		return entityArmor;
	}

	public List<EntityLivingBase> getLivingEntitiesInRange(Float range)
	{
		synchronized (mc.theWorld.loadedEntityList)
		{
			final List tempList = Collections.synchronizedList(new ArrayList());
			for (int i = 0; i < mc.theWorld.loadedEntityList.size(); ++i)
			{
				final Entity e = (Entity) mc.theWorld.loadedEntityList.get(i);
				if (e instanceof EntityLivingBase & mc.thePlayer.getDistanceToEntity(e) <= range & e != mc.thePlayer)
				{
					tempList.add(e);
				}
			}
			return tempList;
		}
	}

	public EntityLivingBase getClosestEntity(boolean checkThreat, float distance, float extender)
	{
		EntityLivingBase closestEntity = null;
		for (final EntityLivingBase entity : getLivingEntities())
		{
			if (isAliveNotUs(entity) && mc.thePlayer.getDistanceToEntity(entity) <= distance + extender)
			{
				if (!(entity instanceof EntityPlayer) || !Aeon.getInstance().getUtils().getFriend().containsFriend(StringUtils.stripControlCodes(entity.getCommandSenderName())))
				{
					if (closestEntity != null)
					{
						if ((checkThreat ? isBiggerThreat(closestEntity, entity, distance, extender) : isCloser(closestEntity, entity)) && isWithinDistance(entity, distance))
						{
							closestEntity = entity;
						}
					} else
					{
						closestEntity = entity;
					}
				}
			}
		}
		return closestEntity;
	}

	public boolean isCloser(EntityLivingBase specifiedEntity, EntityLivingBase otherEntity)
	{
		return mc.thePlayer.getDistanceToEntity(otherEntity) < mc.thePlayer.getDistanceToEntity(specifiedEntity);
	}

	public boolean isMob(EntityLivingBase entity)
	{
		return entity instanceof IMob;
	}

	public boolean isPlayer(EntityLivingBase entity)
	{
		return entity instanceof EntityPlayer;
	}

	public boolean isWithinDistance(EntityLivingBase entity, float distance)
	{
		return mc.thePlayer.getDistanceToEntity(entity) < distance;
	}

	public synchronized void faceEntity(EntityLivingBase entity)
	{
		final float[] rotations = getRotationsNeeded(entity);
		if (rotations != null)
		{
			mc.thePlayer.rotationYaw = rotations[0];
			mc.thePlayer.rotationPitch = rotations[1] + 1.0F;// 14
		}
	}

	public float[] getRotationsNeeded(Entity entity)
	{
		if (entity == null)
		{
			return null;
		}
		final double diffX = entity.posX - mc.thePlayer.posX;
		final double diffZ = entity.posZ - mc.thePlayer.posZ;
		double diffY;
		if (entity instanceof EntityLivingBase)
		{
			final EntityLivingBase entityLivingBase = (EntityLivingBase) entity;
			diffY = entityLivingBase.posY + entityLivingBase.getEyeHeight() - (mc.thePlayer.posY + mc.thePlayer.getEyeHeight());
		} else
		{
			diffY = (entity.boundingBox.minY + entity.boundingBox.maxY) / 2.0D - (mc.thePlayer.posY + mc.thePlayer.getEyeHeight());
		}
		final double dist = MathHelper.sqrt_double(diffX * diffX + diffZ * diffZ);
		final float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / 3.141592653589793D) - 90.0F;
		final float pitch = (float) -(Math.atan2(diffY, dist) * 180.0D / 3.141592653589793D);
		return new float[]
		{ mc.thePlayer.rotationYaw + MathHelper.wrapAngleTo180_float(yaw - mc.thePlayer.rotationYaw), mc.thePlayer.rotationPitch + MathHelper.wrapAngleTo180_float(pitch - mc.thePlayer.rotationPitch) };
	}

	public boolean isHittableNotNull(EntityLivingBase entity, boolean justLook)
	{
		final Initlizer hack = Aeon.getInstance().getUtils().getHack().getLoader();
		if (isAliveNotUs(entity))
		{
			if (isWithinDistance(entity, justLook ? hack.aura.reach + 2.5F : hack.aura.reach) && mc.thePlayer.canEntityBeSeen(entity))
			{
				return true;
			}
		}
		return false;
	}

	public final float limitAngleChange(float current, float intended, float maxChange)
	{
		float change = intended - current;
		if (change > maxChange)
		{
			change = maxChange;
		} else if (change < -maxChange)
		{
			change = -maxChange;
		}
		return current + change;
	}

	public float getDistanceFromMouse(EntityLivingBase entity)
	{
		final float[] neededRotations = getRotationsNeeded(entity);
		if (neededRotations != null)
		{
			final float neededYaw = mc.thePlayer.rotationYaw - neededRotations[0];
			final float neededPitch = mc.thePlayer.rotationPitch - neededRotations[1];
			final float distanceFromMouse = MathHelper.sqrt_float(neededYaw * neededYaw + neededPitch * neededPitch);
			return distanceFromMouse;
		}
		return -1.0F;
	}

	public float getDamageVsEntity(ItemStack itemStack)
	{
		if (itemStack != null)
		{
			if (itemStack.getItem() instanceof ItemTool)
			{
				final ItemTool item = (ItemTool) itemStack.getItem();
				final float i = item.func_150913_i().getDamageVsEntity();
				return i;
			}
			if (itemStack.getItem() instanceof ItemSword)
			{
				final ItemSword item = (ItemSword) itemStack.getItem();
				final float i = item.toolmaterial.getDamageVsEntity();
				return i;
			}
		}
		return 1.0F;
	}

	public EntityLivingBase getClosestEntityToCursor(final float angle)
	{
		float distance = angle; // localized distance variable
		EntityLivingBase tempEntity = null;
		for (final Entity entity : (List<Entity>) mc.theWorld.loadedEntityList)
		{
			if (!(entity instanceof EntityLivingBase) || entity.equals(mc.thePlayer))
			{
				continue;
			}
			final EntityLivingBase living = (EntityLivingBase) entity;
			if (!isHittableNotNull(living, true))
			{
				continue;
			}
			final float[] angles = getAngles(living);
			final float curDistance = getDistanceBetweenAngles(mc.thePlayer.rotationYawHead, angles[0]) + getDistanceBetweenAngles(mc.thePlayer.rotationPitch, angles[1]) / 2;
			if (curDistance <= distance)
			{
				distance = curDistance;
				tempEntity = living;
			}
		}
		return tempEntity;
	}

	

	public float[] getAngles(final EntityLivingBase entityLiving)
	{
		final double difX = entityLiving.posX - mc.thePlayer.posX, difY = entityLiving.posY - mc.thePlayer.posY + entityLiving.getEyeHeight() / 1.4F, difZ = entityLiving.posZ - mc.thePlayer.posZ;
		final double helper = Math.sqrt(difX * difX + difZ * difZ);
		final float yaw = (float) (Math.atan2(difZ, difX) * 180 / Math.PI) - 90;
		final float pitch = (float) -(Math.atan2(difY, helper) * 180 / Math.PI);
		return new float[]
		{ yaw, pitch };
	}

	public static Object[] updateRotation(float current, float target, int maxIncrease)
	{
		float angle = MathHelper.wrapAngleTo180_float(target - current);
		boolean aiming = true;
		if (angle > maxIncrease)
		{
			angle = maxIncrease;
			aiming = false;
		}
		if (angle < -maxIncrease)
		{
			angle = -maxIncrease;
			aiming = false;
		}
		return new Object[]
		{ current + angle, aiming };
	}
	
	public static enum PriorityType
	{
		CLOSEST, CLOSEST_MOUSE;
	}

	public void silent(EntityLivingBase entity, int max)
	{
		final float[] rotations = getRotationsNeeded(entity);
		Object[] updatedRotation = updateRotation(mc.thePlayer.rotationYaw,rotations[0], max);
	
		float yaw = (Float) updatedRotation[0];
		float pitch = rotations[1];
		if (rotations != null)
		{
			mc.thePlayer.rotationYaw = yaw;
			mc.thePlayer.rotationPitch = pitch;
			mc.thePlayer.setRotation(yaw, rotations[1]);
		}
	}
}
