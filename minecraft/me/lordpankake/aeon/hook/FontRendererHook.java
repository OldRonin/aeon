package me.lordpankake.aeon.hook;

import java.awt.Font;
import java.util.List;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.AeonSettings;
import me.lordpankake.aeon.ui.font.CustomFontRenderer;
import me.lordpankake.aeon.ui.font.FontFormat;
import me.lordpankake.aeon.ui.font.FontManager;
import me.lordpankake.aeon.ui.font.FontStyle;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.ResourceLocation;

public final class FontRendererHook extends FontRenderer
{
	public FontRendererHook(GameSettings par1GameSettings, ResourceLocation par2ResourceLocation, TextureManager par3TextureManager, boolean par4)
	{
		super(par1GameSettings, par2ResourceLocation, par3TextureManager, par4);
	}

	@Override
	public int getStringWidth(String par1Str)
	{
		if (par1Str == null)
		{
			return 0;
		}
		if (FontManager.instance().isGlobalFont())
		{
			return (int) FontManager.instance().globalFont.stringWidth(par1Str, FontFormat.NONE);
		}
		return super.getStringWidth(par1Str);
	}

	@Override
	public int drawStringWithShadow(String par1Str, int par2, int par3, int par4)
	{
		if (FontManager.instance().isGlobalFont())
		{
			return (int) FontManager.instance().globalFont.drawString(par1Str, par2, par3, FontStyle.THIN_SHADOW, par4, 0xFF000000, true);
		}
		return super.drawStringWithShadow(par1Str, par2, par3, par4);
	}

	@Override
	public int drawString(String par1Str, int par2, int par3, int par4)
	{
		if (FontManager.instance().isGlobalFont())
		{
			return (int) FontManager.instance().globalFont.drawString(par1Str, par2, par3, FontStyle.NORMAL, par4, 0xFF000000, true);
		}
		return super.drawString(par1Str, par2, par3, par4);
	}

	@Override
	public void drawSplitString(String par1Str, int par2, int par3, int par4, int par5)
	{
		if (FontManager.instance().isGlobalFont())
		{
			final List<String> list = FontManager.instance().globalFont.listFormattedStringToWidth(par1Str, par4, FontFormat.NONE);
			int yOffset = 0;
			for (final String s : list)
			{
				FontManager.instance().globalFont.drawString(s, par2, par3 + yOffset, FontStyle.NORMAL, par5, 0xFF000000, true);
				yOffset += FontManager.instance().globalFont.stringHeight(par1Str, FontFormat.NONE);
			}
			return;
		}
		super.drawSplitString(par1Str, par2, par3, par4, par5);
	}

	@Override
	public String trimStringToWidth(String par1Str, int par2, boolean par3)
	{
		if (FontManager.instance().isGlobalFont())
		{
			final StringBuilder var4 = new StringBuilder();
			float size = 0.0F;
			final int startPosition = par3 ? par1Str.length() - 1 : 0;
			final int increment = par3 ? -1 : 1;
			boolean var8 = false;
			boolean var9 = false;
			for (int index = startPosition; index >= 0 && index < par1Str.length() && size < par2; index += increment)
			{
				final char character = par1Str.charAt(index);
				final float charWidth = getStringWidth(String.valueOf(character)) * 1.1f;
				if (var8)
				{
					var8 = false;
					if (character != 'l' && character != 'L')
					{
						if (character == 'r' || character == 'R')
						{
							var9 = false;
						}
					} else
					{
						var9 = true;
					}
				} else if (charWidth < 0.0F)
				{
					var8 = true;
				} else
				{
					size += charWidth;
					if (var9)
					{
						size += 1.0F;
					}
				}
				if (size > par2)
				{
					break;
				}
				if (par3)
				{
					var4.insert(0, character);
				} else
				{
					var4.append(character);
				}
			}
			return var4.toString();
		}
		return super.trimStringToWidth(par1Str, par2, par3);
	}

	public void setFont(Font f)
	{
		FontManager.instance().globalFont.setFont(f);
	}
	
	public int getLargeGuiStringWidth(String par1Str)
	{
		if (par1Str == null)
		{
			return 0;
		}
		return (int) Aeon.getInstance().getUtils().kek.theme.guiLargeFont.stringWidth(par1Str, FontFormat.NONE);
	}

	public int getGuiStringWidth(String par1Str)
	{
		if (par1Str == null)
		{
			return 0;
		}
		return (int) Aeon.getInstance().getUtils().kek.theme.guiFont.stringWidth(par1Str, FontFormat.NONE);
	}
	
	public int getSmallGuiStringWidth(String par1Str)
	{
		if (par1Str == null)
		{
			return 0;
		}
		return (int) Aeon.getInstance().getUtils().kek.theme.guiSmallFont.stringWidth(par1Str, FontFormat.NONE);
	}
}