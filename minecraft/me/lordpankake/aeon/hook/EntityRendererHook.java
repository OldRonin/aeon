package me.lordpankake.aeon.hook;

import me.lordpankake.aeon.core.Aeon;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.resources.IResourceManager;

public class EntityRendererHook extends EntityRenderer
{
	public EntityRendererHook(Minecraft mc, IResourceManager irs)
	{
		super(mc, irs);
	}

	public void setupFog(int i, float f)
	{
		if (!Aeon.getInstance().getUtils().getHack().getLoader().xray.getState() || !Aeon.getInstance().getUtils().getHack().getLoader().bright.getState())
		{
			super.setupFog(i, f);
		}
	}
}
