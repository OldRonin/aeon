package me.lordpankake.aeon.hook;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.glEnable;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.AeonSettings;
import me.lordpankake.aeon.core.update.Updater;
import me.lordpankake.aeon.ui.click.guiscreens.AeonOptions;
import me.lordpankake.aeon.util.ScreenUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.*;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.resources.I18n;
import net.minecraft.realms.RealmsBridge;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.demo.DemoWorldServer;
import net.minecraft.world.storage.ISaveFormat;
import net.minecraft.world.storage.WorldInfo;
import org.apache.commons.io.Charsets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Project;

public class MainMenuHook extends GuiScreen implements GuiYesNoCallback
{
	private static final AtomicInteger atmInt = new AtomicInteger(0);
	private static final Logger logging = LogManager.getLogger();
	static int white = 0xffffff;
	static int dark = 0xaf000000;
	static int darkgrey = 0xaf191919;
	static int grey = 0xaf444444;
	/** The RNG used by the Main Menu Screen. */
	private static final Random rand = new Random();
	/** Counts the number of screen updates. */
	private float updateCounter;
	/** The splash message. */
	public static String splashText;
	private String secondarySplashText;
	private GuiButton buttonResetDemo;
	/** Timer used to rotate the panorama, increases every tick. */
	public int panoramaTimer;
	/**
	 * Texture allocated for the current viewport of the main menu's panorama
	 * background.
	 */
	private DynamicTexture viewportTexture;
	private final boolean attemptRealms = false;
	private static boolean hasAttemptedRealms;
	private static boolean field_bool;
	private final Object synchedObj = new Object();
	private String graphicsMessage;
	private String openGLOutdatedMessage;
	private String minecraftSysRequirementsLink;
	private static final ResourceLocation splashTexts = new ResourceLocation("texts/splashes.txt");
	private static ResourceLocation minecraftTitleTextures = new ResourceLocation("Aeon/aeon.png");
	private static ResourceLocation overlayTexture = new ResourceLocation("Aeon/Overlay.png");
	private static ResourceLocation uiText = new ResourceLocation("Aeon/ui.png");
	public static String oldSplashText;
	private static final ResourceLocation fullscreenBG = new ResourceLocation("Aeon/Combine/citadel.png");
	private static final ResourceLocation metalBG = new ResourceLocation("Aeon/mTexture.png");
	/** An array of all the paths to the panorama pictures. */
	private static final ResourceLocation titlePanoramaPaths = new ResourceLocation("textures/gui/title/background/panorama.png");
	public static final String field_96138_a = "Please click " + EnumChatFormatting.UNDERLINE + "here" + EnumChatFormatting.RESET + " for more information.";
	private int field_92024_r;
	private int field_92023_s;
	private int field_92022_t;
	private int field_92021_u;
	private int field_92020_v;
	private int field_92019_w;
	private boolean showSplash;
	private ResourceLocation dynamicTexture;
	private static final String __OBFID = "CL_00001154";
	public static MainMenuHook mainMenu;
	public static boolean started;

	public MainMenuHook()
	{
		setupMainMenu();
		if (!started)
		{
			Updater.loadText();
			Aeon.getInstance().getUtils().onClientStart();
			started = true;
		}
		
	}

	/**
	 * Adds the buttons (and other controls) to the screen in question.
	 */
	@Override
	public void initGui()
	{
		this.viewportTexture = new DynamicTexture(256, 256);
		this.dynamicTexture = this.mc.getTextureManager().getDynamicTextureLocation("background", this.viewportTexture);
		// this.realms();
		addButtons();
		final Object var4 = this.synchedObj;
		synchronized (this.synchedObj)
		{
			this.field_92023_s = this.fontRendererObj.getStringWidth(this.graphicsMessage);
			this.field_92024_r = this.fontRendererObj.getStringWidth(this.openGLOutdatedMessage);
			final int var5 = Math.max(this.field_92023_s, this.field_92024_r);
			this.field_92022_t = (this.width - var5) / 2;
			this.field_92021_u = ((GuiButton) this.buttonList.get(0)).posY - 24;
			this.field_92020_v = this.field_92022_t + var5;
			this.field_92019_w = this.field_92021_u + 24;
		}
	}

	private void addButtons()
	{
		final int buttonY = this.height / 4 + 48;
		this.buttonList.add(new GuiButton(0, this.width / 2 - 100, buttonY + 84, 66, 20, I18n.format("menu.options", new Object[0])));
		this.buttonList.add(new GuiButton(4, this.width / 2 + 34, buttonY + 84, 66, 20, I18n.format("menu.quit", new Object[0])));
		this.buttonList.add(new GuiButton(666, this.width / 2 - 32, buttonY + 84, 62, 20, I18n.format("Website", new Object[0])));
		this.buttonList.add(new GuiButton(9001, this.width / 2 - 100, buttonY + 60, "Aeon Config & Alts"));
		if (!Updater.upToDate() && Updater.connected())
		{
			this.buttonList.add(new GuiButton(69, this.width / 2 + 104, buttonY + 84, 42, 20, I18n.format("Update", new Object[0])));
		}
		this.buttonList.add(new GuiButtonLanguage(5, this.width / 2 - 124, buttonY + 72 + 12));
		if (this.mc.isDemo())
		{
			this.buttonList.add(new GuiButton(11, this.width / 2 - 100, buttonY, I18n.format("menu.playdemo", new Object[0])));
			this.buttonList.add(this.buttonResetDemo = new GuiButton(12, this.width / 2 - 100, buttonY + 24 * 1, I18n.format("menu.resetdemo", new Object[0])));
			final ISaveFormat save = this.mc.getSaveLoader();
			final WorldInfo wrldInfo = save.getWorldInfo("Demo_World");
			if (wrldInfo == null)
			{
				this.buttonResetDemo.enabled = false;
			}
		} else
		{
			this.buttonList.add(new GuiButton(1, this.width / 2 - 100, buttonY, I18n.format("menu.singleplayer", new Object[0])));
			this.buttonList.add(new GuiButton(2, this.width / 2 - 100, buttonY + 24 * 1, 99, 20, I18n.format("menu.multiplayer", new Object[0])));
			this.buttonList.add(new GuiButton(14, this.width / 2 + 1, buttonY + 24 * 1, 99, 20, I18n.format("menu.online", new Object[0])));
		}
	}

	/**
	 * Draws the screen and all the components in it.
	 */
	@Override
	public void drawScreen(int par1, int par2, float par3)
	{
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		this.renderSkybox(par1, par2, par3);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		renderFullscreenTexture();
		renderMenuLogo();
		if (showSplash)
		{
			renderSplashText();
		}
		renderMenuText();
		super.drawScreen(par1, par2, par3);
	}

	private void renderMenuLogo()
	{
		final ScreenUtils util = Aeon.getInstance().getUtils().getuScreen();
		final int var6 = this.width / 2 - 137;
		final int var7 = 30;
		this.mc.getTextureManager().bindTexture(minecraftTitleTextures);
		ScreenUtils.drawTexturedModalRectNormal(var6 + 9, 20, 0, 0, 250, 80);
	}

	private void renderSplashText()
	{
		GL11.glPushMatrix();
		GL11.glTranslatef(this.width / 2 + 120, 70.0F, 0.0F);
		GL11.glRotatef(-20.0F, 0.0F, 0.0F, 1.0F);
		float var8 = 1.8F - MathHelper.abs(MathHelper.sin(Minecraft.getSystemTime() % 1000L / 1000.0F * (float) Math.PI * 2.0F) * 0.1F);
		var8 = var8 * 100.0F / (this.fontRendererObj.getStringWidth(MainMenuHook.splashText) + 10);// secondarySplashText
		GL11.glScalef(var8, var8, var8);
		this.drawCenteredString(this.fontRendererObj, MainMenuHook.splashText, 0, -20, 0xffffff);
		this.drawCenteredString(this.fontRendererObj, this.secondarySplashText, 0, -8, 0xffffff);
		GL11.glPopMatrix();
	}

	private void renderMenuText()
	{
		String minecraftVer = AeonSettings.mcVersion;
		final String clientVer = "Aeon [" + AeonSettings.version + "]";
		final String copyright = "Minecraft | Mojang AB.";
		final String extra = ">USEFULL INFORMATION";
		if (this.mc.isDemo())
		{
			minecraftVer = minecraftVer + " Demo";
		}
		this.drawString(this.fontRendererObj, clientVer, 2, this.height - 10, -1);
		this.drawString(this.fontRendererObj, copyright, this.width - this.fontRendererObj.getStringWidth(copyright) - 2, this.height - 10, -1);
		if (this.graphicsMessage != null && this.graphicsMessage.length() > 0)
		{
			this.drawString(this.fontRendererObj, this.graphicsMessage, this.field_92022_t, this.field_92021_u, -1);
			this.drawString(this.fontRendererObj, this.openGLOutdatedMessage, (this.width - this.field_92024_r) / 2, ((GuiButton) this.buttonList.get(0)).posY - 12, -1);
		}
	}

	private void renderFullscreenTexture()
	{
		final ScaledResolution varRes = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
		final int width = varRes.getScaledWidth();
		final int height = varRes.getScaledHeight();
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		this.mc.getTextureManager().bindTexture(overlayTexture);
		final Tessellator var3 = Tessellator.instance;
		var3.startDrawingQuads();
		var3.addVertexWithUV(0.0D, height, -90.0D, 0.0D, 1.0D);
		var3.addVertexWithUV(width, height, -90.0D, 1.0D, 1.0D);
		var3.addVertexWithUV(width, 0.0D, -90.0D, 1.0D, 0.0D);
		var3.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
		var3.draw();
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		if (button.buttonID == 0)
		{
			this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
		}
		if (button.buttonID == 9001)
		{
			this.mc.displayGuiScreen(new AeonOptions(this));
		}
		if (button.buttonID == 14)
		{
			this.func_140005_i();
		}
		if (button.buttonID == 666)
		{
			final Desktop d = Desktop.getDesktop();
			try
			{
				if (Desktop.isDesktopSupported())
				{
					d.browse(new URI(Updater.websiteURL));
				} else
				{
					MainMenuHook.splashText = "Error opening URL!";
				}
			} catch (final IOException e)
			{
				e.printStackTrace();
			} catch (final URISyntaxException e)
			{
				e.printStackTrace();
			}
		}
		if (button.buttonID == 69)
		{
			final Desktop d = Desktop.getDesktop();
			try
			{
				if (Desktop.isDesktopSupported())
				{
					d.browse(new URI(Updater.downloadURL));
				} else
				{
					MainMenuHook.splashText = "Download not supported!";
				}
			} catch (final IOException e)
			{
				e.printStackTrace();
			} catch (final URISyntaxException e)
			{
				e.printStackTrace();
			}
		}
		if (button.buttonID == 5)
		{
			this.mc.displayGuiScreen(new GuiLanguage(this, this.mc.gameSettings, this.mc.getLanguageManager()));
		}
		if (button.buttonID == 1)
		{
			this.mc.displayGuiScreen(new GuiSelectWorld(this));
		}
		if (button.buttonID == 2)
		{
			this.mc.displayGuiScreen(new GuiMultiplayer(this));
		}
		/*
		 * if (button.buttonID == 14 && this.minecraftRealmsButton.visible) {
		 * this.func_140005_i(); }
		 */
		if (button.buttonID == 4)
		{
			this.mc.shutdown();
		}
		if (button.buttonID == 11)
		{
			this.mc.launchIntegratedServer("Demo_World", "Demo_World", DemoWorldServer.demoWorldSettings);
		}
		if (button.buttonID == 12)
		{
			final ISaveFormat var2 = this.mc.getSaveLoader();
			final WorldInfo var3 = var2.getWorldInfo("Demo_World");
			if (var3 != null)
			{
				final GuiYesNo var4 = GuiSelectWorld.staticYesNo(this, var3.getWorldName(), 12);
				this.mc.displayGuiScreen(var4);
			}
		}
	}

	/**
	 * Called from the main game loop to update the screen.
	 */
	@Override
	public void updateScreen()
	{
		++this.panoramaTimer;
	}

	/**
	 * Returns true if this GUI should pause the game when it is displayed in
	 * single-player
	 */
	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}

	/**
	 * Fired when a key is typed. This is the equivalent of
	 * KeyListener.keyTyped(KeyEvent e).
	 */
	@Override
	protected void keyTyped(char par1, int par2)
	{
	}

	private void renderSkybox(int par1, int par2, float par3)
	{
		this.mc.getFramebuffer().unbindFramebuffer();
		GL11.glViewport(0, 0, 256, 256);
		this.drawPanorama(par1, par2, par3);
		this.rotateAndBlurSkybox(par3);
		this.rotateAndBlurSkybox(par3);
		this.rotateAndBlurSkybox(par3);
		// this.rotateAndBlurSkybox(par3);
		// this.rotateAndBlurSkybox(par3);
		// this.rotateAndBlurSkybox(par3);
		// this.rotateAndBlurSkybox(par3);
		this.mc.getFramebuffer().bindFramebuffer(true);
		GL11.glViewport(0, 0, this.mc.displayWidth, this.mc.displayHeight);
		final Tessellator var4 = Tessellator.instance;
		var4.startDrawingQuads();
		final float var5 = this.width > this.height ? 120.0F / this.width : 120.0F / this.height;
		final float var6 = this.height * var5 / 256.0F;
		final float var7 = this.width * var5 / 256.0F;
		var4.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F);
		final int var8 = this.width;
		final int var9 = this.height;
		var4.addVertexWithUV(0.0D, var9, this.zLevel, 0.5F - var6, 0.5F + var7);
		var4.addVertexWithUV(var8, var9, this.zLevel, 0.5F - var6, 0.5F - var7);
		var4.addVertexWithUV(var8, 0.0D, this.zLevel, 0.5F + var6, 0.5F - var7);
		var4.addVertexWithUV(0.0D, 0.0D, this.zLevel, 0.5F + var6, 0.5F + var7);
		var4.draw();
	}

	@Override
	public void confirmClicked(boolean par1, int par2)
	{
		if (par1 && par2 == 12)
		{
			final ISaveFormat var6 = this.mc.getSaveLoader();
			var6.flushCache();
			var6.deleteWorldDirectory("Demo_World");
			this.mc.displayGuiScreen(this);
		} else if (par2 == 13)
		{
			if (par1)
			{
				try
				{
					final Class var3 = Class.forName("java.awt.Desktop");
					final Object var4 = var3.getMethod("getDesktop", new Class[0]).invoke((Object) null, new Object[0]);
					var3.getMethod("browse", new Class[]
					{ URI.class }).invoke(var4, new Object[]
					{ new URI(this.minecraftSysRequirementsLink) });
				} catch (final Throwable var5)
				{
					logging.error("Couldn\'t open link", var5);
				}
			}
			this.mc.displayGuiScreen(this);
		}
	}

	private void rotateAndBlurSkybox(float par1)
	{
		this.mc.getTextureManager().bindTexture(this.dynamicTexture);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glCopyTexSubImage2D(GL11.GL_TEXTURE_2D, 0, 0, 0, 0, 0, 256, 256);
		GL11.glEnable(GL11.GL_BLEND);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glColorMask(true, true, true, false);
		final Tessellator var2 = Tessellator.instance;
		var2.startDrawingQuads();
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		final byte var3 = 2;
		for (int var4 = 0; var4 < var3; ++var4)
		{
			var2.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F / (var4 + 1));
			final int var5 = this.width;
			final int var6 = this.height;
			final float var7 = (var4 - var3 / 2) / 256.0F;
			var2.addVertexWithUV(var5, var6, this.zLevel, 0.0F + var7, 1.0D);
			var2.addVertexWithUV(var5, 0.0D, this.zLevel, 1.0F + var7, 1.0D);
			var2.addVertexWithUV(0.0D, 0.0D, this.zLevel, 1.0F + var7, 0.0D);
			var2.addVertexWithUV(0.0D, var6, this.zLevel, 0.0F + var7, 0.0D);
		}
		var2.draw();
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glColorMask(true, true, true, true);
	}

	private void drawPanorama(int par1, int par2, float par3)
	{
		final Tessellator var4 = Tessellator.instance;
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		Project.gluPerspective(120.0F, 1.0F, 0.05F, 10.0F);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
		GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		final byte var5 = 8;
		for (int var6 = 0; var6 < var5 * var5; ++var6)
		{
			GL11.glPushMatrix();
			final float var7 = ((float) (var6 % var5) / (float) var5 - 0.5F) / 64.0F;
			final float var8 = ((float) (var6 / var5) / (float) var5 - 0.5F) / 64.0F;
			final float var9 = 0.0F;
			GL11.glTranslatef(var7, var8, var9);
			GL11.glRotatef(MathHelper.sin((this.panoramaTimer + par3) / 400.0F) * 25.0F + 20.0F, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(-(this.panoramaTimer + par3) * 0.1F, 0.0F, 1.0F, 0.0F);
			for (int var10 = 0; var10 < 6; ++var10)
			{
				GL11.glPushMatrix();
				if (var10 == 1)
				{
					GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
				}
				if (var10 == 2)
				{
					GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
				}
				if (var10 == 3)
				{
					GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
				}
				if (var10 == 4)
				{
					GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
				}
				if (var10 == 5)
				{
					GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
				}
				this.mc.getTextureManager().bindTexture(titlePanoramaPaths);
				var4.startDrawingQuads();
				var4.setColorRGBA_I(16777215, 255 / (var6 + 1));
				final float var11 = 0.0F;
				var4.addVertexWithUV(-1.0D, -1.0D, 1.0D, 0.0F + var11, 0.0F + var11);
				var4.addVertexWithUV(1.0D, -1.0D, 1.0D, 1.0F - var11, 0.0F + var11);
				var4.addVertexWithUV(1.0D, 1.0D, 1.0D, 1.0F - var11, 1.0F - var11);
				var4.addVertexWithUV(-1.0D, 1.0D, 1.0D, 0.0F + var11, 1.0F - var11);
				var4.draw();
				GL11.glPopMatrix();
			}
			GL11.glPopMatrix();
			GL11.glColorMask(true, true, true, false);
		}
		var4.setTranslation(0.0D, 0.0D, 0.0D);
		GL11.glColorMask(true, true, true, true);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPopMatrix();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glPopMatrix();
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

	/**
	 * Called when the mouse is clicked.
	 */
	@Override
	protected void mouseClicked(int par1, int par2, int par3)
	{
		super.mouseClicked(par1, par2, par3);
		final Object var4 = this.synchedObj;
		synchronized (this.synchedObj)
		{
			if (this.graphicsMessage.length() > 0 && par1 >= this.field_92022_t && par1 <= this.field_92020_v && par2 >= this.field_92021_u && par2 <= this.field_92019_w)
			{
				final GuiConfirmOpenLink var5 = new GuiConfirmOpenLink(this, this.minecraftSysRequirementsLink, 13, true);
				var5.func_146358_g();
				this.mc.displayGuiScreen(var5);
			}
		}
	}

	private void func_140005_i()
	{
		final RealmsBridge var1 = new RealmsBridge();
		var1.switchToRealms(this);
	}

	private void setupMainMenu()
	{
		this.openGLOutdatedMessage = field_96138_a;
		MainMenuHook.splashText = "Up-to-date!";
		BufferedReader var1 = null;
		try
		{
			final ArrayList var2 = new ArrayList();
			var1 = new BufferedReader(new InputStreamReader(Minecraft.getMinecraft().getResourceManager().getResource(splashTexts).getInputStream(), Charsets.UTF_8));
			String var3;
			while ((var3 = var1.readLine()) != null)
			{
				var3 = var3.trim();
				if (!var3.isEmpty())
				{
					var2.add(var3);
				}
			}
			if (!var2.isEmpty() && Updater.upToDate())
			{
				do
				{
					MainMenuHook.splashText = (String) var2.get(rand.nextInt(var2.size()));
				} while (MainMenuHook.splashText.hashCode() == 125780783);
			} else
			{
				showSplash = true;
				if (Updater.connected())
				{
					MainMenuHook.splashText = "Grab the new Aeon update!";
					this.secondarySplashText = "Click ''Update'' to download!";
				} else
				{
					MainMenuHook.splashText = "Couldn't connect to update URL!";
					this.secondarySplashText = "Check the ''Website'' button to manually update!";
				}
			}
			oldSplashText = MainMenuHook.splashText;
		} catch (final IOException var12)
		{
			;
		} finally
		{
			if (var1 != null)
			{
				try
				{
					var1.close();
				} catch (final IOException var11)
				{
					;
				}
			}
		}
		this.updateCounter = rand.nextFloat();
		this.graphicsMessage = "";
		if (!OpenGlHelper.shadersSupported)
		{
			this.graphicsMessage = "Old graphics card detected; this may prevent you from";
			this.openGLOutdatedMessage = "playing in the far future as OpenGL 2.1 will be required.";
			this.minecraftSysRequirementsLink = "https://help.mojang.com/customer/portal/articles/325948?ref=game";
		}
	}
}
