package me.lordpankake.aeon.modules.mod.render;

import java.util.ArrayList;
import java.util.List;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.util.other.Alias;
import net.minecraft.client.gui.FontRenderer;

public class NameProtect extends Module
{
	public String sharedName = "Nope";
	public boolean useSharedName;
	public static ArrayList hiddenNames = new ArrayList<String>();
	public static ArrayList replaceNames = new ArrayList<String>();
	@AIgnore
	public static ArrayList aliases = new ArrayList<Alias>();

	public NameProtect(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		loadSettings();
		if (hiddenNames.size() == replaceNames.size())
		{
			for (int i = 0; i < hiddenNames.size(); i++)
			{
				final String hide = (String) hiddenNames.get(i);
				final String replace = (String) replaceNames.get(i);
				final Alias a = new Alias(hide, replace);
				aliases.add(a);
			}
		} else
		{
			final FontRenderer fr = mc.fontRenderer;
			fr.drawStringWithShadow("NameProtect list sizes do not match!", 2, 32, 0xffffff);
		}
	}

	@Override
	public void onDisable()
	{
		aliases.clear();
	}

	public static List<Alias> getAliases()
	{
		return aliases;
	}
}
