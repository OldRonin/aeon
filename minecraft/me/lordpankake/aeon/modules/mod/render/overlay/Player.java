package me.lordpankake.aeon.modules.mod.render.overlay;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventRenderWorld;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import net.minecraft.client.renderer.entity.RenderManager;

public class Player extends Module
{
	public Player(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onRenderWorld(EventRenderWorld event)
	{
		final int what = 10;
		final int doesthisdo = 10;
		final float unknownFloat = 1.5F;
		final float WhatDoesThisDo = 21.2F;
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
		// GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_DEPTH_TEST);// glDepthMask,
		GL11.glDepthMask(true);
		GL11.glPushMatrix();
		GL11.glTranslatef(what, what, 50.0F);
		GL11.glScalef(-doesthisdo, doesthisdo, doesthisdo);
		// GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
		final float var6 = mc.thePlayer.renderYawOffset;
		final float var7 = mc.thePlayer.rotationYaw;
		final float var8 = mc.thePlayer.rotationPitch;
		final float var9 = mc.thePlayer.prevRotationYawHead;
		final float var10 = mc.thePlayer.rotationYawHead;
		GL11.glRotatef(-30.0F, 0.0F, 1.0F, 0.0F);
		// RenderHelper.enableStandardItemLighting();
		GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(-((float) Math.atan(unknownFloat / 40.0F)) * 20.0F, 1.0F, 0.0F, 0.0F);
		mc.thePlayer.renderYawOffset = (float) Math.atan(WhatDoesThisDo / 40.0F) * 20.0F;
		mc.thePlayer.rotationYaw = (float) Math.atan(WhatDoesThisDo / 40.0F) * 40.0F;
		mc.thePlayer.rotationPitch = -((float) Math.atan(unknownFloat / 40.0F)) * 20.0F;
		mc.thePlayer.rotationYawHead = mc.thePlayer.rotationYaw;
		mc.thePlayer.prevRotationYawHead = mc.thePlayer.rotationYaw;
		// GL11.glTranslatef(0.0F, mc.thePlayer.yOffset, 0.0F);
		RenderManager.instance.playerViewY = 180.0F;
		RenderManager.instance.renderEntityWithPosYaw(mc.thePlayer, 1.0D, 0.0D, 0.0D, 0.0F, 0.7F);
		mc.thePlayer.renderYawOffset = var6;
		mc.thePlayer.rotationYaw = var7;
		mc.thePlayer.rotationPitch = var8;
		mc.thePlayer.prevRotationYawHead = var9;
		mc.thePlayer.rotationYawHead = var10;
		GL11.glPopMatrix();
		// RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		// OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		// GL11.glDisable(GL11.GL_TEXTURE_2D);
		// OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}
}
