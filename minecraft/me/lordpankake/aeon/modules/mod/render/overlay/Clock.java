package me.lordpankake.aeon.modules.mod.render.overlay;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Clock extends Module
{
	@AIgnore
	private final RenderItem itemRenderer = new RenderItem();

	public Clock(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (mc.currentScreen != null)
		{
			if (!(mc.currentScreen instanceof GuiChat)  && !(mc.currentScreen instanceof PankakeGUI))
			{
				return;
			}
		}
		draw(Item.getItemById(347), 63, 1);
	}

	public void draw(Item item, int x, int y)
	{
		itemRenderer.renderItemIntoGUI(mc.fontRenderer, mc.getTextureManager(), new ItemStack(item, 1, 0), x, y);
	}
}
