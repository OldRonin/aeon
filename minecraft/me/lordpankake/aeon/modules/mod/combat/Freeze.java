package me.lordpankake.aeon.modules.mod.combat;

import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.server.S12PacketEntityVelocity;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPacketReceive;

public class Freeze extends Module
{
	@AIgnore
	private int ticks;
	
	public Freeze(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onPacketReceive(EventPacketReceive event)
	{
		//Log.write("Packet received:" + event.storedPacket.getClass());
		if (event.storedPacket instanceof S12PacketEntityVelocity)
		{
			//mc.thePlayer.isDead = true;
			event.nullifyPacket();
			
		}
	}

	@Override
	public void onEnable()
	{
		 mc.thePlayer.isDead = true;
	}

	@Override
	public void onDisable()
	{
		mc.thePlayer.isDead = false;
	}
}