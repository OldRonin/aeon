package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.EntityUtils;
import net.minecraft.entity.EntityLivingBase;

public class Lockview extends Module
{
	@AIgnore
	public EntityLivingBase target;
	public static float range = 4.5F;
	public boolean locktarget = true;

	public Lockview(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		final EntityUtils util = Aeon.getInstance().getUtils().getEntity();
		if (Aeon.getInstance().getUtils().getHack().getLoader().aura.getState())
		{
			if (Aeon.getInstance().getUtils().getHack().getLoader().aura.target != null)
			{
				this.target = Aeon.getInstance().getUtils().getHack().getLoader().aura.target;
			}
		} else
		{
			this.target = util.getClosestEntity(true, range, 1F);
		}
		if (this.target != null && mc.thePlayer.getDistanceToEntity(this.target) <= range && mc.thePlayer.canEntityBeSeen(this.target))
		{
			if (Aeon.getInstance().getUtils().getFriend().containsFriend(this.target.getCommandSenderName()))
			{
				return;
			}
			if (Aeon.getInstance().getUtils().getHack().getLoader().aura.getState())
			{
				if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
				{
					if (util.canAttackFromAngle(this.target, Aeon.getInstance().getUtils().getHack().getLoader().aura.safety))
					{
						util.faceEntity(this.target);
					}
				} else
				{
					util.faceEntity(this.target);
				}
			} else
			{
				util.faceEntity(this.target);
			}
		}
	}

	@Override
	public void onDisable()
	{
		this.target = null;
	}
}
