package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.EntityUtils;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.entity.player.EntityPlayer;

public class TriggerClick extends Module
{
	public Float speed = 5.5F;
	public Float reach = 4.3F;
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();
	@AIgnore
	private boolean hit;

	public TriggerClick(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@EventTarget
	@Override
	public void onGameTick(EventTick event)
	{
		final EntityUtils util = Aeon.getInstance().getUtils().getEntity();
		if (this.timer.check(1000.0F / this.speed) && mc.objectMouseOver != null && mc.objectMouseOver.entityHit != null)
		{
			if (!(mc.objectMouseOver.entityHit instanceof EntityPlayer) || !Aeon.getInstance().getUtils().getFriend().containsFriend(mc.objectMouseOver.entityHit.getCommandSenderName()))
			{
				mc.thePlayer.swingItem();
				mc.playerController.attackEntity(mc.thePlayer, mc.objectMouseOver.entityHit);
				timer.reset();
			}
		}
	}
}
