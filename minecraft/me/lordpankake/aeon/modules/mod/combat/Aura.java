package me.lordpankake.aeon.modules.mod.combat;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventManager;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.annotations.IncludeGUI;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPacketReceive;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPacketSend;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPostTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventPreTick;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.EntityUtils;
import me.lordpankake.aeon.util.NanoTimerUtils;
import me.lordpankake.aeon.util.TimerUtils;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.network.play.client.C03PacketPlayer.C05PacketPlayerLook;

public class Aura extends Module
{
	@AIgnore
	public EntityLivingBase target;
	@AIgnore
	private final TimerUtils timer = new NanoTimerUtils();
	@AIgnore
	private final TimerUtils timer2 = new NanoTimerUtils();
	public String mode = "kill";
	@IncludeGUI(min = 0.1F, max = 20)
	public float speed = 5.5F;
	@IncludeGUI(min = 1F, max = 6F)
	public float reach = 4.3F;
	@IncludeGUI(min = 10, max = 160)
	public float safety = 60;
	public float turnRate = 5.0F;
	public boolean safe = true;
	public boolean silent = false;
	public boolean locktarget = false;

	public Aura(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@EventTarget
	@Override
	public void onPreTick(EventPreTick event)
	{
		this.target = getTarget();
		if (this.target != null)
		{
			if (mc.thePlayer.canEntityBeSeen(this.target) && silent && this.timer2.check((this.turnRate * 50)))
			{
				Aeon.getInstance().getUtils().getEntity().silent(this.target, (int) turnRate * 10);
				this.timer2.reset();
			}
		}
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (this.target != null)
		{
			if (mode.contains("kill"))
			{
				aura();
			}
		}
		if (mode.contains("mass"))
		{
			mass();
		}
	}

	private void mass()
	{
		final EntityUtils util = Aeon.getInstance().getUtils().getEntity();
		for (final EntityLivingBase targetEntities : util.getLivingEntitiesInRange(this.reach))
		{
			if (Aeon.getInstance().getUtils().getFriend().containsFriend(targetEntities.getCommandSenderName()))
			{
				return;
			}
			if (this.safe && canAttackWithSafety())
			{
				return;
			}
			EventManager.eventMouseClick.call(0);
			mc.thePlayer.swingItem();
			mc.playerController.attackEntity(mc.thePlayer, targetEntities);
		}
	}

	private void aura()
	{
		final EntityUtils util = Aeon.getInstance().getUtils().getEntity();
		if (doAttack() && this.timer.check(1000.0F / this.speed))
		{
			if (Aeon.getInstance().getUtils().getFriend().containsFriend(this.target.getCommandSenderName()))
			{
				return;
			}
			if (this.safe && !canAttackWithSafety())
			{
				 return;
			}
			EventManager.eventMouseClick.call(0);
			mc.thePlayer.swingItem();
			mc.playerController.attackEntity(mc.thePlayer, this.target);
			this.timer.reset();
		}
		
	}

	private boolean canAttackWithSafety()
	{
		return Aeon.getInstance().getUtils().getEntity().canAttackFromAngle(this.target, this.safety);
	}

	private EntityLivingBase getTarget()
	{
		final EntityUtils util = Aeon.getInstance().getUtils().getEntity();
		if (this.locktarget)
		{
			if (this.target == null || this.target.isDead || mc.thePlayer.getDistanceToEntity(this.target) >= this.reach)
			{
				return util.getClosestEntity(true, this.reach, 0);
			}
		} else
		{
			return util.getClosestEntity(true, this.reach, 0);
		}
		return null;
	}

	private boolean doAttack()
	{
		final EntityUtils util = Aeon.getInstance().getUtils().getEntity();
		if (util.isHittableNotNull(this.target, false))
		{
			if (canAttackWithSafety() && (this.target != null && !this.target.isDead && mc.thePlayer.getDistanceToEntity(this.target) <= this.reach))
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void onEnable()
	{
		loadSettings();
	}

	@Override
	public void onDisable()
	{
		this.target = null;
	}
}
