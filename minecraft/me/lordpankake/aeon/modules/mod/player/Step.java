package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.IncludeGUI;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import net.minecraft.block.Block;
import net.minecraft.block.BlockAir;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;

public class Step extends Module
{
	@IncludeGUI(min = 1, max = 10 )
	public static float height = 1.02f;

	public Step(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}
	
	@Override
	public void onEnable()
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.boundingBox.minY + 0.1D, mc.thePlayer.posY + 0.1D, mc.thePlayer.posZ, false));
			mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.boundingBox.minY + 0.1D, mc.thePlayer.posY + 0.1D, mc.thePlayer.posZ, false));
			mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.boundingBox.minY - 4.0D, mc.thePlayer.posY - 4.0D, mc.thePlayer.posZ, false));
			mc.thePlayer.setPosition(mc.thePlayer.posX, mc.thePlayer.posY - 4.0D, mc.thePlayer.posZ);
		}
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		// ReflectionUtils utils = new ReflectionUtils();
		// utils.findMethods(EntityPlayer.class);
		if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			if (mc.thePlayer.isCollidedHorizontally && mc.thePlayer.onGround && !mc.thePlayer.isInWater())
			{
				mc.thePlayer.boundingBox.offset(0.0D, height + 0.05, 0.0D);
				mc.thePlayer.motionY = -0.07D;
				mc.thePlayer.isCollidedHorizontally = false;
			}
		} else
		{
			mc.thePlayer.stepHeight = height;
		}
	}

	private boolean isInsideBlock(EntityPlayer player)
	{
		for (int x = MathHelper.floor_double(player.boundingBox.minX); x < MathHelper.floor_double(player.boundingBox.maxX) + 1; x++)
		{
			for (int y = MathHelper.floor_double(player.boundingBox.minY); y < MathHelper.floor_double(player.boundingBox.maxY) + 1; y++)
			{
				for (int z = MathHelper.floor_double(player.boundingBox.minZ); z < MathHelper.floor_double(player.boundingBox.maxZ) + 1; z++)
				{
					final Block block = mc.theWorld.getBlock(x, y, z);
					if (block == null || block instanceof BlockAir)
					{
						continue;
					}
					final AxisAlignedBB boundingBox = block.getCollisionBoundingBoxFromPool(Minecraft.getMinecraft().theWorld, x, y, z);
					if (boundingBox != null && player.boundingBox.intersectsWith(boundingBox))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	public void onDisable()
	{
		if (mc.thePlayer != null)
		{
			mc.thePlayer.stepHeight = 0.5F;
		}
	}
}
