package me.lordpankake.aeon.modules.mod.player;

import java.util.ArrayList;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiChest;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventDisplayScreen;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.util.other.BlockPlaceholder;

public class Pillager extends Module
{
	@AIgnore
	public static ArrayList<BlockPlaceholder> chests = new ArrayList<BlockPlaceholder>();

	public Pillager(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onScreenDisplay(EventDisplayScreen event)
	{
		if (event.screen instanceof GuiChest)
		{
			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					for (int i = 0; i < 27; i++)
					{
						if (mc.thePlayer.openContainer.getSlot(i) != null && mc.thePlayer.openContainer.getSlot(i).getHasStack())
						{
							mc.playerController.windowClick(mc.thePlayer.openContainer.windowId, i, 0, 1, mc.thePlayer);
							try
							{
								if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
								{
									Thread.sleep(100);
								}else
								{
									Thread.sleep(45);
								}
								
							} catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						}
					}
					mc.thePlayer.closeScreen();
				
				}
			}).start();
		}
	}
}
