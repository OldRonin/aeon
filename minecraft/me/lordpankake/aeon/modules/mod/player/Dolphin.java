package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import net.minecraft.block.material.Material;

public class Dolphin extends Module
{
	public float speed = 1.1F;

	public Dolphin(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if ((mc.thePlayer.isInWater() || mc.thePlayer.isInsideOfMaterial(Material.lava)) && !mc.gameSettings.keyBindSneak.isPressed() && !mc.gameSettings.keyBindJump.isPressed())
		{
			mc.thePlayer.jump();
			mc.thePlayer.jumpMovementFactor = speed;
		}
	}
}
