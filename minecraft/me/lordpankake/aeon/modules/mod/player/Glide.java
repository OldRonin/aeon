package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import net.minecraft.block.material.Material;
import net.minecraft.network.play.client.C03PacketPlayer;

public class Glide extends Module
{
	public Glide(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onEnable()
	{
		if (Aeon.getInstance().getUtils().getHack().getLoader().nocheat.getState())
		{
			mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.boundingBox.minY + 0.1D, mc.thePlayer.posY + 0.1D, mc.thePlayer.posZ, false));
			mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.boundingBox.minY + 0.1D, mc.thePlayer.posY + 0.1D, mc.thePlayer.posZ, false));
			mc.thePlayer.sendQueue.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(mc.thePlayer.posX, mc.thePlayer.boundingBox.minY - 4.0D, mc.thePlayer.posY - 4.0D, mc.thePlayer.posZ, false));
			mc.thePlayer.setPosition(mc.thePlayer.posX, mc.thePlayer.posY - 4.0D, mc.thePlayer.posZ);
		}
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (mc.thePlayer.motionY < 0.0D && mc.thePlayer.isAirBorne && !mc.thePlayer.isInWater() && !mc.thePlayer.isOnLadder() && !mc.thePlayer.isInsideOfMaterial(Material.lava))
		{
			mc.thePlayer.motionY = -0.125D;
			mc.thePlayer.jumpMovementFactor *= 1.21337F;
		}
	}
}
