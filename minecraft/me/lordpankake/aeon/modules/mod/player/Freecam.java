package me.lordpankake.aeon.modules.mod.player;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import net.minecraft.client.entity.EntityOtherPlayerMP;

public class Freecam extends Module
{
	@AIgnore
	private double x;
	@AIgnore
	private double y;
	@AIgnore
	private double z;
	@AIgnore
	private float pitch;
	@AIgnore
	private float yaw;
	@AIgnore
	private boolean set;
	@AIgnore
	private double camStartPosX;
	@AIgnore
	private double camStartPosY;
	@AIgnore
	private double camStartPosZ;
	@AIgnore
	private float camStartYaw;
	@AIgnore
	private float camStartPitch;

	public Freecam(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	public void onDisable()
	{
		set = false;
		mc.thePlayer.setPosition(x, y, z);
		mc.thePlayer.rotationPitch = pitch;
		mc.thePlayer.rotationYaw = yaw;
		mc.theWorld.removeEntityFromWorld(101);
	}

	@Override
	public void onEnable()
	{
		if (!set)
		{
			x = mc.thePlayer.posX;
			y = mc.thePlayer.posY;
			z = mc.thePlayer.posZ;
			pitch = mc.thePlayer.rotationPitch;
			yaw = mc.thePlayer.rotationYaw;
		}
		set = true;
		final EntityOtherPlayerMP spawnSelf = new EntityOtherPlayerMP(mc.theWorld, mc.session.getGameProfile());
		mc.theWorld.addEntityToWorld(101, spawnSelf);
		spawnSelf.setPositionAndRotation(x, y - 1.5D, z, yaw, pitch);
	}
}
