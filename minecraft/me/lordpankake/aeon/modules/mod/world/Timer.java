package me.lordpankake.aeon.modules.mod.world;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.IncludeGUI;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;

public class Timer extends Module
{
	@IncludeGUI(min = 0.1F, max = 20F)
	public static float speed = 2f;

	public Timer(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		mc.timer.timerSpeed = Timer.speed;
	}

	@Override
	public void onDisable()
	{
		mc.timer.timerSpeed = 1f;
	}
}
