package me.lordpankake.aeon.modules.mod.world.radar;

import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.AIgnore;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventTick;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiChat;

public class Radar extends Module
{
	@AIgnore
	private final JMTradar radar;

	public Radar(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
		radar = new JMTradar(mc, new Gui());
	}

	@Override
	public void onEnable()
	{
		JMTradar.enabled = true;
	}

	@Override
	public void onDisable()
	{
		JMTradar.enabled = false;
	}

	@Override
	@EventTarget
	public void onGameTick(EventTick event)
	{
		if (mc.currentScreen != null)
		{
			if (!(mc.currentScreen instanceof GuiChat) && !(mc.currentScreen instanceof PankakeGUI))
			{
				return;
			}
		}
		radar.drawRadar();
	}
}
