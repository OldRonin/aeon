package me.lordpankake.aeon.modules.mod.world.radar;

import org.lwjgl.opengl.GL11;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.EntityLiving;

public class EntityHandler
{
	private final int radius;
	private final Minecraft mc;
	private final Gui g;

	public EntityHandler(Minecraft mc, Gui g, int radius)
	{
		this.radius = radius;
		this.mc = mc;
		this.g = g;
	}

	public void renderSurroundingEntities(int centerX, int centerY)
	{
		final EntityTextures ets = new EntityTextures(mc, g);
		for (int as = 0; as < mc.theWorld.loadedEntityList.size(); as++)
		{
			if (!(mc.theWorld.loadedEntityList.get(as) instanceof EntityLiving))
			{
				continue;
			}
			final EntityLiving ent = (EntityLiving) mc.theWorld.loadedEntityList.get(as);
			final double xx = mc.thePlayer.posX - ent.posX;
			final double zz = mc.thePlayer.posZ - ent.posZ;
			if (xx * xx + zz * zz > radius * radius)
			{
				continue;
			}
			GL11.glColor4f(255, 255, 255, 255);
			GL11.glPushMatrix();
			GL11.glTranslated(centerX + xx, centerY + zz, 0);
			GL11.glRotatef(mc.thePlayer.rotationYaw, 0, 0, 1);
			GL11.glScalef(0.5F, 0.5F, 0);
			ets.renderTexture(ent, -4, -4);
			// g.drawTexturedModalRect(-4, -4, 0, 0, 8, 8);
			GL11.glScalef(2, 2, 0);
			GL11.glTranslated(-xx, -zz, 0);
			GL11.glPopMatrix();
		}
	}
}
