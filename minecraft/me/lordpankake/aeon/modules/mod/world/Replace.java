package me.lordpankake.aeon.modules.mod.world;

import net.minecraft.item.ItemBlock;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventBlockDamage;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventBlockDestroy;

public class Replace extends Module
{
	public Replace(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
	}
	
	@Override
	@EventTarget
	public void onBlockDestroy(EventBlockDestroy event)
	{
		if (mc.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemBlock)
		{
			mc.thePlayer.sendQueue.addToSendQueue(new C08PacketPlayerBlockPlacement(event.x, event.y -1 , event.z, 1, mc.thePlayer.inventory.getCurrentItem(), 0, 0, 0));
		}
	}
}
