package me.lordpankake.aeon.modules.mod.chat;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.ModuleType;
import me.lordpankake.aeon.modules.eventapi.EventTarget;
import me.lordpankake.aeon.modules.eventapi.annotations.ACommand;
import me.lordpankake.aeon.modules.eventapi.events.aeon.EventCommand;
import me.lordpankake.aeon.modules.mod.chat.commands.misc.*;
import me.lordpankake.aeon.modules.mod.chat.commands.settings.combat.*;
import me.lordpankake.aeon.modules.mod.chat.commands.settings.player.*;
import me.lordpankake.aeon.modules.mod.chat.commands.settings.render.*;
import me.lordpankake.aeon.modules.mod.chat.commands.settings.world.*;
import me.lordpankake.aeon.ui.InGame;

public class CommandListener extends Module
{
	public static String commandKey = "-";
	public ArrayList commandList = new ArrayList<Command>();
	public RemoveAlias remalias = new RemoveAlias("RemAlias", "Removes a name/repalcement from NameProtect. Syntax is -RemAlias [Name]");
	public AddAlias addalias = new AddAlias("Alias", "Adds a name/repalcement to NameProtect. Syntax is -Alias [Name] [Replace]");
	public XrayCom xray = new XrayCom("Xray", "Adds/Removes a block name to/from Xray. Syntax is -Xray [Add|Rem] [textureName]");
	public SetAuraMode auramode = new SetAuraMode("AuraMode", "Sets aura mode. Parameters are [mass|kill]");
	public SetAuraReach aurareach = new SetAuraReach("AuraReach", "Sets Aura reach");
	public SetAuraSafety aurasafety = new SetAuraSafety("AuraSafety", "Either set the angle with a number, to set the safety on or off with [on|off]");
	public SetAuraSilenting aurasilenting = new SetAuraSilenting("AuraSilent", "Aura Silent Aimbot: Parameters are [on|off]");
	public SetAuraSpeed auraspeed = new SetAuraSpeed("AuraSpeed", "Sets Aura speed");
	public SetAutoBowMode autobowmode = new SetAutoBowMode("AutoBowMode", "Sets the AutoBow fire method. [Auto|Click|Burst]");
	public SetAutoBowSpeed autobowspeed = new SetAutoBowSpeed("AutoBowSpeed", "Sets speed of AutoBow's fire rate.");
	public SetBuildMode buildmode = new SetBuildMode("BuildMode", "Sets Build mode [carpet|pole|floor|path]");
	public SetBuildRange buildrange = new SetBuildRange("BuildRange", "Sets Build radius");
	public SetDisconnectHealth disconnecthealth = new SetDisconnectHealth("DiscHealth", "Sets AutoDisconnect's health value.");
	public SetESPMode espmode = new SetESPMode("ESPMode", "Sets ESP targets [Mob|Player]. Both may be used in one command");
	public SetFastmineSpeed fastminespeed = new SetFastmineSpeed("FastmineSpeed", "Sets Fastmine speed");
	public SetFastmineMode fastminemode = new SetFastmineMode("FastmineMode", "Sets Fastmine mode [delay|set|auto]");
	public SetNukerMode nukermode = new SetNukerMode("NukerMode", "Sets Nuker mode [id|click|tick]");
	public SetNukerRange nukerrange = new SetNukerRange("NukerRange", "Sets Nuker Radius");
	public SetSearchRange searchrange = new SetSearchRange("SearchRange", "Sets the range for Search's block highlighting. Default is 20.");
	public SetSoupSafe soupsafe = new SetSoupSafe("SafeSoup", "Safely soup on KitPvP using only hotbar soup");
	public SetSoupHealth souphealth = new SetSoupHealth("SoupHealth", "Sets autosoup health level");
	public SetStepHeight stepheight = new SetStepHeight("StepHeight", "Sets Step's height. 3.5 is the default.");
	public SetTimerSpeed timerspeed = new SetTimerSpeed("TimerSpeed", "Set the Timer hack's speed. Default is 2");
	public SetTracerMode tracermode = new SetTracerMode("TracerMode", "Sets tracer line targets [Mob|Player]. Both may be used in one command");
	// miscs
	public Bind bind = new Bind("Bind", "Sets a hack to a key");
	public Clear clear = new Clear("Clear", "Clears chat.");
	public Enchant enchant = new Enchant("Enchant", "Enchants an item with all enchants to the level you specify. Requires creative. -Enchant [list] for list of enchants.");
	public Friend friend = new Friend("Friend", "[add|del] Adds or removes friends.");
	public Help halppls = new Help("Help", "Shows a list of chat commands");
	public Keybinds keybinds = new Keybinds("Keys", "Displays a list of all loaded modules and their keybinds.");
	public Reload reload = new Reload("Reload", "Reloads the client settings.");
	public Scrape scrape = new Scrape("Scrape", "Writes all usernames of a server to a text file");

	// public Waypoint waypoint = new Waypoint("Point",
	// "Makes a highlighted point in the world. -point [add|remove] [name]");
	// public SetWaypointTracers waypointtracer = new
	// SetWaypointTracers("PointTracer",
	// "Sets tracers to waypoints [on] or [off] (no []'s)");
	//
	public CommandListener(String name, int keybind, ModuleType type, String description)
	{
		super(name, keybind, type, description);
		setDrawToUI(false);
		loadCommands();
	}

	private void loadCommands()
	{
		// module settings
		// registerFromAnnotations();
		addCommand(addalias);
		addCommand(auramode);
		addCommand(aurareach);
		addCommand(aurasafety);
		addCommand(aurasilenting);
		addCommand(auraspeed);
		addCommand(autobowmode);
		addCommand(autobowspeed);
		addCommand(buildmode);
		addCommand(buildrange);
		addCommand(disconnecthealth);
		addCommand(espmode);
		addCommand(fastminespeed);
		addCommand(fastminemode);
		addCommand(nukermode);
		addCommand(nukerrange);
		addCommand(remalias);
		addCommand(searchrange);
		addCommand(soupsafe);
		addCommand(souphealth);
		addCommand(stepheight);
		addCommand(tracermode);
		addCommand(timerspeed);
		addCommand(xray);
		// settings
		addCommand(bind);
		addCommand(clear);
		addCommand(enchant);
		addCommand(friend);
		addCommand(halppls);
		addCommand(keybinds);
		addCommand(reload);
		addCommand(scrape);
		if (commandList.size() < 1)
		{
			// misc
			// addCommand(toggleAIcontrol);
			// addCommand(waypoint);
			// addCommand(waypointtracer);
		}
	}

	private void registerFromAnnotations()
	{
		for (final Module m : Aeon.getInstance().getUtils().getHack().getModules())
		{
			if (m.getClass().getDeclaredFields().length > 0)
			{
				for (final Field field : m.getClass().getDeclaredFields())
				{
					try
					{
						field.setAccessible(true);
						final Object value = field.get(m);
						if (field.isAnnotationPresent(ACommand.class))
						{
							addCommand(new AnnCommand(field.getAnnotation(ACommand.class).commandName(), field.getAnnotation(ACommand.class).description(), field.getAnnotation(ACommand.class).parameters(), field, m.getClass()));
						}
					} catch (final IllegalArgumentException e)
					{
						// e.printStackTrace();
					} catch (final IllegalAccessException e)
					{
						// e.printStackTrace();
					}
				}
			}
		}
	}

	private void addCommand(Command com)
	{
		commandList.add(com);
	}

	public List<Command> getCommands()
	{
		return commandList;
	}

	@Override
	@EventTarget
	public void onAeonCommand(EventCommand event)
	{
		final String command = event.getMessage().toLowerCase().substring(1);
		if (event.isAeonCommand())
		{
			for (final Command com : getCommands())
			{
				if (command.startsWith(com.getName().toLowerCase()))
				{
					com.onFire(event.getMessage().toLowerCase().split(" "));
					InGame.first = false;
				}
			}
		}
	}
}
