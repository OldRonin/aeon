package me.lordpankake.aeon.modules.mod.chat.commands.settings.render;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.render.Tracer;
import me.lordpankake.aeon.util.PlayerUtils;

public class SetTracerMode extends Command
{
	public SetTracerMode(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		String temp = "";
		if (s.length >= 3)
		{
			if (s[1].contains("mob") || s[1].contains("player"))
			{
				temp += s[1];
			}
			if (s[2].contains("player") || s[2].contains("mob"))
			{
				temp += " " + s[2];
			}
		} else if (s.length >= 2)
		{
			if (s[1].contains("mob") || s[1].contains("player"))
			{
				temp += s[1];
			}
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax. Use -TM [Param] [OptionalParam]");
		}
		Tracer.mode = temp;
		hack.tracer.writeSettings();
		PlayerUtils.addChatMessage("Tracer mode set to: " + Tracer.mode);
	}
}
