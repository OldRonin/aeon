package me.lordpankake.aeon.modules.mod.chat.commands.settings.combat;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;

public class SetSoupSafe extends Command
{
	public SetSoupSafe(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (s[1].contains("on"))
			{
				hack.soup.safeSoup = true;
			} else
			{
				hack.soup.safeSoup = false;
			}
			PlayerUtils.addChatMessage("Soup Safety: " + hack.soup.safeSoup);
		} else
		{
			PlayerUtils.addChatMessage("Invalid Syntax! Syntax is -SafeSoup [On|Off]");
		}
	}
}
