package me.lordpankake.aeon.modules.mod.chat.commands.settings.player;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.player.Step;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetStepHeight extends Command
{
	public SetStepHeight(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (StringUtils.isFloat(s[1]))
			{
				Step.height = Float.parseFloat(s[1]);
				PlayerUtils.addChatMessage("Step height: " + s[1]);
			}
		}
	}
}
