package me.lordpankake.aeon.modules.mod.chat.commands.settings.world;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.world.Build;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetBuildRange extends Command
{
	public SetBuildRange(String name, String desc)
	{
		super("BuildRange", "Sets Build radius");
	}

	@Override
	public void onFire(String[] s)
	{
		if (StringUtils.isInteger(s[1]))
		{
			if (Integer.parseInt(s[1]) > 10)
			{
				Build.range = 10;
			} else
			{
				Build.range = Integer.parseInt(s[1]);
			}
			PlayerUtils.addChatMessage("Build Range: " + s[1]);
		}
	}
}
