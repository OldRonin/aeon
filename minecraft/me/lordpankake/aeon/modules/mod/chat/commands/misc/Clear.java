package me.lordpankake.aeon.modules.mod.chat.commands.misc;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.mod.chat.Command;

public class Clear extends Command
{
	public Clear(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		mc.ingameGUI.getChatGUI().clearChatMessages();
	}
}
