package me.lordpankake.aeon.modules.mod.chat.commands.settings.render;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.render.NameProtect;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.other.Alias;

public class RemoveAlias extends Command
{
	public RemoveAlias(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		final String temp = "";
		if (hack.nameprotect.getState())
		{
			PlayerUtils.addChatMessage("You can't remove aliases with NameProtect enabled!");
			return;
		}
		if (s.length >= 2)
		{
			final String name = s[1];
			if (NameProtect.hiddenNames.contains(name))
			{
				final int removedIndex = NameProtect.hiddenNames.indexOf(name);
				NameProtect.hiddenNames.remove(name);
				NameProtect.replaceNames.remove(removedIndex);
				if (hack.nameprotect.getState())
				{
					Alias remove = null;
					for (final Alias a : NameProtect.getAliases())
					{
						if (a.name.equalsIgnoreCase(name))
						{
							remove = a;
						}
					}
					if (remove != null)
					{
						NameProtect.aliases.remove(remove);
					}
				}
				hack.nameprotect.writeSettings();
			}
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax! Use -RemAlias [Name]");
		}
	}
}
