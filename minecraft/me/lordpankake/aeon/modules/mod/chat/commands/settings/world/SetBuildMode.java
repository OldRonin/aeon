package me.lordpankake.aeon.modules.mod.chat.commands.settings.world;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.world.Build;
import me.lordpankake.aeon.util.PlayerUtils;

public class SetBuildMode extends Command
{
	public SetBuildMode(String name, String desc)
	{
		super(name, desc);// [carpet|pole|floor|path]
	}

	@Override
	public void onFire(String[] s)
	{
		String temp = "";
		if (s.length >= 2)
		{
			if (s[1].contains("pole") || s[1].contains("floor") || s[1].contains("replace"))
			{
				temp += s[1];
			}
		}
		if (s.length >= 3)
		{
			if (s[2].contains("pole") || s[2].contains("floor") || s[2].contains("replace"))
			{
				temp += s[2];
			}
		}
		if (s.length >= 4)
		{
			if (s[3].contains("carpet") || s[3].contains("pole") || s[3].contains("replace"))
			{
				temp += s[3];
			}
		}/*
		 * if (s.length >= 5) { if (s[4].contains("carpet") ||
		 * s[4].contains("pole") || s[4].contains("floor") ||
		 * s[4].contains("path")) { temp += s[4]; } }
		 */
		if (temp == "")
		{
			PlayerUtils.addChatMessage("Error, invalid input. Use -BM [Pole] or [Floor] without []'s");
		} else
		{
			Build.mode = temp;
			PlayerUtils.addChatMessage("Build Mode: " + Build.mode);
		}
	}
}
