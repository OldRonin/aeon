package me.lordpankake.aeon.modules.mod.chat.commands.settings.render;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.render.NameProtect;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.other.Alias;

public class AddAlias extends Command
{
	public AddAlias(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		final String temp = "";
		if (s.length >= 3)
		{
			final String name = s[1];
			final String replace = s[2];
			if (hack.nameprotect.getState())
			{
				NameProtect.hiddenNames.add(name);
				NameProtect.replaceNames.add(replace);
				NameProtect.aliases.add(new Alias(name, replace));
			} else
			{
				NameProtect.hiddenNames.add(name);
				NameProtect.replaceNames.add(replace);
			}
			hack.nameprotect.writeSettings();
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax! Use -Alias [Name] [Replace]");
		}
	}
}
