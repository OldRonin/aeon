package me.lordpankake.aeon.modules.mod.chat.commands.settings.combat;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;

public class SetAuraMode extends Command
{
	public SetAuraMode(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		String temp = "";
		if (s.length >= 2)
		{
			if (s[1].contains("kill") || s[1].contains("mass"))
			{
				temp += s[1];
			}
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax. Parameters are [mass|kill]");
		}
		if (temp.length() > 3)
		{
			hack.aura.mode = temp;
			hack.aura.writeSettings();
			PlayerUtils.addChatMessage("Aura mode set to: " + temp);
		} else
		{
			PlayerUtils.addChatMessage("Invalid syntax. Parameters are [mass|kill]");
		}
	}
}
