package me.lordpankake.aeon.modules.mod.chat.commands.misc;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;

public class Bind extends Command
{
	public Bind(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 3)
		{
			for (final Module t : Aeon.getInstance().getUtils().getHack().getModules())
			{
				if (s[1].contains(t.getName().toLowerCase()))
				{
					t.setKey(Aeon.getInstance().getUtils().getKey().getKeyFromString(s[2]));
					Aeon.getInstance().getUtils().getKey().writeSettings();
					PlayerUtils.addChatMessage(t.getName() + " keybind set to: " + s[2].toUpperCase());
				}
			}
		} else
		{
			PlayerUtils.addChatMessage("Error: Invalid Syntax. Use -Bind [Hackname] [Key] with no []'s");
		}
	}
}
