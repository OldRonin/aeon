package me.lordpankake.aeon.modules.mod.chat.commands.settings.combat;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetAuraSafety extends Command
{
	public SetAuraSafety(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (StringUtils.isDouble(s[1]))
			{
				hack.aura.safety = Float.parseFloat(s[1]);
				PlayerUtils.addChatMessage("Aura Safety: " + s[1]);
			}
			if (s[1].contains("on"))
			{
				hack.aura.safe = true;
				PlayerUtils.addChatMessage("Aura Safety: On");
			}
			if (s[1].contains("off"))
			{
				hack.aura.safe = false;
				PlayerUtils.addChatMessage("Aura Safety: Off");
			}
			hack.aura.writeSettings();
		}
	}
}
