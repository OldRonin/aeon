package me.lordpankake.aeon.modules.mod.chat.commands.settings.world;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.world.Nuker;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetNukerRange extends Command
{
	public SetNukerRange(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length > 2)
		{
			if (StringUtils.isInteger(s[1]))
			{
				Nuker.radius = Float.parseFloat(s[1]);
				PlayerUtils.addChatMessage("Nuker Range: " + s[1]);
			}
		}
	}
}
