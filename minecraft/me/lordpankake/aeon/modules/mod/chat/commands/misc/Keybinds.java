package me.lordpankake.aeon.modules.mod.chat.commands.misc;

import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.modules.Module;
import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.util.PlayerUtils;

public class Keybinds extends Command
{
	public Keybinds(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		for (final Module t : Aeon.getInstance().getUtils().getHack().getModules())
		{
			if (!t.getName().contains("Aeon"))
			{
				PlayerUtils.addChatMessage(Aeon.getInstance().getUtils().getKey().getKeyFromInteger(t.getKey()) + " || " + t.getType().toString() + " | " + t.getName());
			}
		}
	}
}
