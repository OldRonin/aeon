package me.lordpankake.aeon.modules.mod.chat.commands.settings.world;

import me.lordpankake.aeon.modules.mod.chat.Command;
import me.lordpankake.aeon.modules.mod.world.Timer;
import me.lordpankake.aeon.util.PlayerUtils;
import me.lordpankake.aeon.util.StringUtils;

public class SetTimerSpeed extends Command
{
	public SetTimerSpeed(String name, String desc)
	{
		super(name, desc);
	}

	@Override
	public void onFire(String[] s)
	{
		if (s.length >= 2)
		{
			if (StringUtils.isFloat(s[1]))
			{
				Timer.speed = Float.parseFloat(s[1]);
				PlayerUtils.addChatMessage("Timer speed set to: " + s[1]);
			} else
			{
				PlayerUtils.addChatMessage("Invalid Syntax: -TS [FloatValue]");
			}
		}
	}
}
