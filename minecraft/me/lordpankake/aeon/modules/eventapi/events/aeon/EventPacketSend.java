package me.lordpankake.aeon.modules.eventapi.events.aeon;

import net.minecraft.network.Packet;
import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;

public class EventPacketSend extends NeuEvent
{
	public Packet storedPacket;

	public void call(Packet pack)
	{
		this.storedPacket = pack;
		super.call();
	}

	public void nullifyPacket()
	{
		this.storedPacket = null;
	}
}
