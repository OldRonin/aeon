package me.lordpankake.aeon.modules.eventapi.events.aeon;

import me.lordpankake.aeon.modules.eventapi.events.NeuEvent;

public class EventBlockClick extends NeuEvent
{
	public int x, y, z, side;

	public void call(int x, int y, int z, int side)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.side = side;
		super.call();
	}
}
