package me.lordpankake.aeon.modules;

import me.lordpankake.aeon.modules.eventapi.events.aeon.*;

public class ModuleMethods
{
	public void onEnable()
	{
	}

	public void onDisable()
	{
	}

	public void onPostTick(EventPostTick event)
	{
	}

	public void onGameTick(EventTick event)
	{
	}

	public void onPreTick(EventPreTick event)
	{
	}

	public void onBlockClick(EventBlockClick event)
	{
	}

	public void onBlockPlace(EventBlockPlace event)
	{
	}

	public void onBlockDamage(EventBlockDamage event)
	{
	}

	public void onBlockDestroy(EventBlockDestroy event)
	{
	}

	public void onPreEntityHit(EventPreEntityHit event)
	{
	}

	public void onPostEntityHit(EventPostEntityHit event)
	{
	}

	public void onMouseClick(EventMouseClick event)
	{
	}

	public void onRenderWorld(EventRenderWorld event)
	{
	}

	public void onAeonCommand(EventCommand event)
	{
	}

	public void onChatMessage(EventSendMessage event)
	{
	}

	public void onReadMessage(EventProcessMessage event)
	{
	}

	public void onVelocitySet(EventVelocity event)
	{
	}

	public void onPacketReceive(EventPacketReceive event)
	{
	}

	public void onPacketSend(EventPacketSend event)
	{
	}

	public void onHealthChange(EventHealthChange event)
	{
	}

	public void onScreenDisplay(EventDisplayScreen event)
	{
	}
}
