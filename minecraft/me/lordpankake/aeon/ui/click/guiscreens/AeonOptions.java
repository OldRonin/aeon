package me.lordpankake.aeon.ui.click.guiscreens;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.glEnable;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.Project;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.hook.MainMenuHook;
import me.lordpankake.aeon.ui.click.guiscreens.alts.GuiAltList;
import me.lordpankake.aeon.ui.click.guiscreens.module.ManageModules;
import me.lordpankake.aeon.util.ScreenUtils;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

public class AeonOptions extends GuiScreen
{
	private static ResourceLocation settings = new ResourceLocation("Aeon/settings.png");
	private static ResourceLocation overlayTexture = new ResourceLocation("Aeon/Overlay.png");
	private static final ResourceLocation titlePanoramaPaths = new ResourceLocation("textures/gui/title/background/panorama.png");
	private ResourceLocation dynamicTexture;
	private final MainMenuHook parentScreen;
	private int panoramaTimer;

	public AeonOptions(MainMenuHook mainMenuHook)
	{
		this.parentScreen = mainMenuHook;
		this.panoramaTimer = this.parentScreen.panoramaTimer;
	}

	@Override
	public void initGui()
	{
		this.dynamicTexture = this.mc.getTextureManager().getDynamicTextureLocation("background", new DynamicTexture(256, 256));
		final int buttonY = this.height / 4 + 48;
		final int defaultWidth = 200;
		this.buttonList.add(new GuiButton(9001, this.width / 2 - 100, buttonY, defaultWidth / 2 - 1, 20, "Alt Manager"));
		this.buttonList.add(new GuiButton(9002, this.width / 2 + 1, buttonY, defaultWidth / 2 - 1, 20, "Change Font"));
		this.buttonList.add(new GuiButton(9003, this.width / 2 - 100, buttonY + 24, defaultWidth / 2 - 1, 20, "Manage Modules"));
		this.buttonList.add(new GuiButton(9004, this.width / 2 + 1, buttonY + 24, defaultWidth / 2 - 1, 20, "Open Setting Directory"));
		this.buttonList.add(new GuiButton(9005, this.width / 2 - 100, buttonY + 48, defaultWidth, 20, "Manage GUI Display"));
		this.buttonList.add(new GuiButton(1, this.width / 2 - 100, buttonY + 72, defaultWidth, 20, "Back"));
	}

	@Override
	public void drawScreen(int par1, int par2, float par3)
	{
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		this.renderSkybox(par1, par2, par3);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		renderFullscreenTexture();
		renderMenuLogo();
		super.drawScreen(par1, par2, par3);
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		if (button.buttonID == 1)
		{
			this.parentScreen.panoramaTimer = this.panoramaTimer;
			this.mc.displayGuiScreen(this.parentScreen);
		}
		if (button.buttonID == 9001)
		{
			this.mc.displayGuiScreen(new GuiAltList(this));
		}
		if (button.buttonID == 9002)
		{
			this.mc.displayGuiScreen(new ChangeFont(this));
		}
		if (button.buttonID == 9003)
		{
			this.mc.displayGuiScreen(new ManageModules(this));
		}
		if (button.buttonID == 9005)
		{
			this.mc.displayGuiScreen(new GuiCreator(this));
		}
		if (button.buttonID == 9004)
		{
			final File saveLocation = new File("Aeon");
			final Desktop dk = Desktop.getDesktop();
			try
			{
				if (saveLocation.exists())
				{
					dk.open(new File(saveLocation.toString()));
				}
			} catch (final IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	private void renderFullscreenTexture()
	{
		final ScaledResolution varRes = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
		final int width = varRes.getScaledWidth();
		final int height = varRes.getScaledHeight();
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		this.mc.getTextureManager().bindTexture(overlayTexture);
		final Tessellator var3 = Tessellator.instance;
		var3.startDrawingQuads();
		var3.addVertexWithUV(0.0D, height, -90.0D, 0.0D, 1.0D);
		var3.addVertexWithUV(width, height, -90.0D, 1.0D, 1.0D);
		var3.addVertexWithUV(width, 0.0D, -90.0D, 1.0D, 0.0D);
		var3.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
		var3.draw();
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
	}

	private void renderMenuLogo()
	{
		final ScreenUtils util = Aeon.getInstance().getUtils().getuScreen();
		final int xCoords = this.width / 2 - 137;
		this.mc.getTextureManager().bindTexture(settings);
		ScreenUtils.drawTexturedModalRectNormal(xCoords + 9, 10, 0, 0, 250, 80);
	}

	private void renderSkybox(int par1, int par2, float par3)
	{
		this.mc.getFramebuffer().unbindFramebuffer();
		GL11.glViewport(0, 0, 256, 256);
		this.drawPanorama(par1, par2, par3);
		this.rotateAndBlurSkybox(par3);
		this.rotateAndBlurSkybox(par3);
		this.rotateAndBlurSkybox(par3);
		// this.rotateAndBlurSkybox(par3);
		// this.rotateAndBlurSkybox(par3);
		// this.rotateAndBlurSkybox(par3);
		// this.rotateAndBlurSkybox(par3);
		this.mc.getFramebuffer().bindFramebuffer(true);
		GL11.glViewport(0, 0, this.mc.displayWidth, this.mc.displayHeight);
		final Tessellator var4 = Tessellator.instance;
		var4.startDrawingQuads();
		final float var5 = this.width > this.height ? 120.0F / this.width : 120.0F / this.height;
		final float var6 = this.height * var5 / 256.0F;
		final float var7 = this.width * var5 / 256.0F;
		var4.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F);
		final int var8 = this.width;
		final int var9 = this.height;
		var4.addVertexWithUV(0.0D, var9, this.zLevel, 0.5F - var6, 0.5F + var7);
		var4.addVertexWithUV(var8, var9, this.zLevel, 0.5F - var6, 0.5F - var7);
		var4.addVertexWithUV(var8, 0.0D, this.zLevel, 0.5F + var6, 0.5F - var7);
		var4.addVertexWithUV(0.0D, 0.0D, this.zLevel, 0.5F + var6, 0.5F + var7);
		var4.draw();
	}

	private void drawPanorama(int par1, int par2, float par3)
	{
		final Tessellator var4 = Tessellator.instance;
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		Project.gluPerspective(120.0F, 1.0F, 0.05F, 10.0F);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
		GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		final byte var5 = 8;
		for (int var6 = 0; var6 < var5 * var5; ++var6)
		{
			GL11.glPushMatrix();
			final float var7 = ((float) (var6 % var5) / (float) var5 - 0.5F) / 64.0F;
			final float var8 = ((float) (var6 / var5) / (float) var5 - 0.5F) / 64.0F;
			final float var9 = 0.0F;
			GL11.glTranslatef(var7, var8, var9);
			GL11.glRotatef(MathHelper.sin((this.panoramaTimer + par3) / 400.0F) * 25.0F + 20.0F, 1.0F, 0.0F, 0.0F);
			GL11.glRotatef(-(this.panoramaTimer + par3) * 0.1F, 0.0F, 1.0F, 0.0F);
			for (int var10 = 0; var10 < 6; ++var10)
			{
				GL11.glPushMatrix();
				if (var10 == 1)
				{
					GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
				}
				if (var10 == 2)
				{
					GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
				}
				if (var10 == 3)
				{
					GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
				}
				if (var10 == 4)
				{
					GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
				}
				if (var10 == 5)
				{
					GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
				}
				this.mc.getTextureManager().bindTexture(titlePanoramaPaths);
				var4.startDrawingQuads();
				var4.setColorRGBA_I(16777215, 255 / (var6 + 1));
				final float var11 = 0.0F;
				var4.addVertexWithUV(-1.0D, -1.0D, 1.0D, 0.0F + var11, 0.0F + var11);
				var4.addVertexWithUV(1.0D, -1.0D, 1.0D, 1.0F - var11, 0.0F + var11);
				var4.addVertexWithUV(1.0D, 1.0D, 1.0D, 1.0F - var11, 1.0F - var11);
				var4.addVertexWithUV(-1.0D, 1.0D, 1.0D, 0.0F + var11, 1.0F - var11);
				var4.draw();
				GL11.glPopMatrix();
			}
			GL11.glPopMatrix();
			GL11.glColorMask(true, true, true, false);
		}
		var4.setTranslation(0.0D, 0.0D, 0.0D);
		GL11.glColorMask(true, true, true, true);
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPopMatrix();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glPopMatrix();
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
	}

	@Override
	public void updateScreen()
	{
		++this.panoramaTimer;
	}

	private void rotateAndBlurSkybox(float par1)
	{
		this.mc.getTextureManager().bindTexture(this.dynamicTexture);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);
		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		GL11.glCopyTexSubImage2D(GL11.GL_TEXTURE_2D, 0, 0, 0, 0, 0, 256, 256);
		GL11.glEnable(GL11.GL_BLEND);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glColorMask(true, true, true, false);
		final Tessellator var2 = Tessellator.instance;
		var2.startDrawingQuads();
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		final byte var3 = 2;
		for (int var4 = 0; var4 < var3; ++var4)
		{
			var2.setColorRGBA_F(1.0F, 1.0F, 1.0F, 1.0F / (var4 + 1));
			final int var5 = this.width;
			final int var6 = this.height;
			final float var7 = (var4 - var3 / 2) / 256.0F;
			var2.addVertexWithUV(var5, var6, this.zLevel, 0.0F + var7, 1.0D);
			var2.addVertexWithUV(var5, 0.0D, this.zLevel, 1.0F + var7, 1.0D);
			var2.addVertexWithUV(0.0D, 0.0D, this.zLevel, 1.0F + var7, 0.0D);
			var2.addVertexWithUV(0.0D, var6, this.zLevel, 0.0F + var7, 0.0D);
		}
		var2.draw();
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glColorMask(true, true, true, true);
	}
}
