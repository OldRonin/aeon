package me.lordpankake.aeon.ui.click.guiscreens.alts;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.gui.GuiYesNoCallback;
import net.minecraft.util.Session;
import net.minecraft.util.StringTranslate;

public class GuiAltList extends GuiScreen implements GuiYesNoCallback
{
	public String dispErrorString = "";
	public boolean deleteMenuOpen = false;
	public GuiScreen backScreen;

	public GuiAltList(GuiScreen guiMainMenu)
	{
		backScreen = guiMainMenu;
		Manager.loadAlts();
	}

	public FontRenderer getLocalFontRenderer()
	{
		return this.fontRendererObj;
	}

	@Override
	public void onGuiClosed()
	{
		Manager.saveAlts();
		super.onGuiClosed();
	}
	private SlotAlt tSlot;

	@Override
	public void initGui()
	{
		buttonList.clear();
		buttonList.add(new GuiButton(1, width / 2 - 100, height - 47, 66, 20, "Add"));
		buttonList.add(new GuiButton(2, width / 2 - 33, height - 47, 65, 20, "Login"));
		buttonList.add(new GuiButton(3, width / 2 + 32, height - 47, 69, 20, "Remove"));
		buttonList.add(new GuiButton(4, width / 2 - 100, height - 26, 99, 20, "Back"));
		buttonList.add(new GuiButton(5, width / 2, height - 26, 100, 20, "Direct Login"));
		tSlot = new SlotAlt(this.mc, this);
		tSlot.registerScrollButtons(7, 8);
	}

	@Override
	public void confirmClicked(boolean flag, int i1)
	{
		super.confirmClicked(flag, i1);
		if (deleteMenuOpen)
		{
			deleteMenuOpen = false;
			if (flag)
			{
				Manager.altList.remove(i1);
				Manager.saveAlts();
			}
			mc.displayGuiScreen(this);
		}
	}

	@Override
	public void actionPerformed(GuiButton button)
	{
		super.actionPerformed(button);
		if (button.buttonID == 1)
		{
			final GuiAltAdd gaa = new GuiAltAdd(this);
			mc.displayGuiScreen(gaa);
		}
		if (button.buttonID == 2)
		{
			try
			{
				final Alt a1 = Manager.altList.get(tSlot.getSelected());
				if (a1.isPremium())
				{
					mc.session = Session.loginPassword(a1.getUsername(), a1.getPassword());
					Manager.altScreen.dispErrorString = "";
				} else
				{
					this.mc.session = new Session(a1.getUsername(), "-", "-", "Legacy");
					Manager.altScreen.dispErrorString = "";
				}
			} catch (final Exception e)
			{
			}
		}
		if (button.buttonID == 3)
		{
			try
			{
				final StringTranslate stringtranslate = StringTranslate.getInstance();
				final String s1 = "Are you sure you want to delete the alt: " + "\"" + Manager.altList.get(tSlot.getSelected()).getUsername() + "\"" + "?";
				final String s3 = "Delete";
				final String s4 = "Cancel";
				final GuiYesNo guiyesno = new GuiYesNo(this, s1, "", s3, s4, tSlot.getSelected());
				deleteMenuOpen = true;
				mc.displayGuiScreen(guiyesno);
			} catch (final Exception e)
			{
			}
		}
		if (button.buttonID == 4)
		{
			mc.displayGuiScreen(backScreen);
		}
		if (button.buttonID == 5)
		{
			final GuiDirectLogin gdl = new GuiDirectLogin(this);
			mc.displayGuiScreen(gdl);
		}
	}

	@Override
	public void updateScreen()
	{
		super.updateScreen();
		// if(!(mc.currentScreen instanceof GuiYesNo) && deleteMenuOpen)
		// {
		// deleteMenuOpen = false;
		// }
	}

	@Override
	public void drawScreen(int i, int j, float f)
	{
		tSlot.drawScreen(i, j, f);
		drawCenteredString(fontRendererObj, "Alts: \2477" + Manager.altList.size(), width / 2, 13, 0xFFFFFF);
		if (mc.session != null)
		{
			fontRendererObj.drawStringWithShadow("Username: \2477" + mc.session.username, 3, 3, 0xFFFFFF);
		} else
		{
			fontRendererObj.drawStringWithShadow("Username: \2477" + "ERROR", 3, 3, 0xFFFFFF);
		}
		fontRendererObj.drawStringWithShadow(dispErrorString, 3, 13, 0xFFFFFF);
		super.drawScreen(i, j, f);
	}
}
