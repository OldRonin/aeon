package me.lordpankake.aeon.ui.click.guiscreens.alts;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import org.lwjgl.input.Keyboard;

public class GuiAltAdd extends GuiScreen
{
	public GuiScreen parent;
	public GuiTextField usernameBox;
	public PasswordField passwordBox;

	public GuiAltAdd(GuiScreen paramScreen)
	{
		this.parent = paramScreen;
	}

	@Override
	public void initGui()
	{
		Keyboard.enableRepeatEvents(true);
		buttonList.add(new GuiButton(1, width / 2 - 100, height / 4 + 96 + 12, "Add"));
		buttonList.add(new GuiButton(2, width / 2 - 100, height / 4 + 96 + 36, "Back"));
		usernameBox = new GuiTextField(fontRendererObj, width / 2 - 100, 76, 200, 20);
		passwordBox = new PasswordField(fontRendererObj, width / 2 - 100, 116, 200, 20);
		usernameBox.setMaxStringLength(200);
		passwordBox.setMaxStringLength(128);
	}

	@Override
	public void onGuiClosed()
	{
		Keyboard.enableRepeatEvents(false);
	}

	@Override
	public void updateScreen()
	{
		usernameBox.updateCursorCounter();
		passwordBox.updateCursorCounter();
	}

	@Override
	public void mouseClicked(int x, int y, int b)
	{
		usernameBox.mouseClicked(x, y, b);
		passwordBox.mouseClicked(x, y, b);
		super.mouseClicked(x, y, b);
	}

	@Override
	public void actionPerformed(GuiButton button)
	{
		if (button.buttonID == 1)
		{
			if (!usernameBox.getText().trim().isEmpty())
			{
				if (passwordBox.getText().trim().isEmpty())
				{
					final Alt theAlt = new Alt(usernameBox.getText().trim());
					if (!Manager.altList.contains(theAlt))
					{
						Manager.altList.add(theAlt);
						Manager.saveAlts();
					}
				} else
				{
					final Alt theAlt = new Alt(usernameBox.getText().trim(), passwordBox.getText().trim());
					if (!Manager.altList.contains(theAlt))
					{
						Manager.altList.add(theAlt);
						Manager.saveAlts();
					}
				}
			}
			mc.displayGuiScreen(parent);
		} else if (button.buttonID == 2)
		{
			mc.displayGuiScreen(parent);
		}
	}

	@Override
	protected void keyTyped(char c, int i)
	{
		usernameBox.textboxKeyTyped(c, i);
		passwordBox.textboxKeyTyped(c, i);
		if (c == '\t')
		{
			if (usernameBox.isFocused())
			{
				usernameBox.setFocused(false);
				passwordBox.setFocused(true);
			} else
			{
				usernameBox.setFocused(true);
				passwordBox.setFocused(false);
			}
		}
		if (c == '\r')
		{
			actionPerformed((GuiButton) buttonList.get(0));
		}
	}

	@Override
	public void drawScreen(int x, int y, float f)
	{
		drawDefaultBackground();
		drawString(fontRendererObj, "Username", width / 2 - 100, 63, 0xa0a0a0);
		drawString(fontRendererObj, "Password", width / 2 - 100, 104, 0xa0a0a0);
		try
		{
			usernameBox.drawTextBox();
			passwordBox.drawTextBox();
		} catch (final Exception err)
		{
			err.printStackTrace();
		}
		super.drawScreen(x, y, f);
	}
}
