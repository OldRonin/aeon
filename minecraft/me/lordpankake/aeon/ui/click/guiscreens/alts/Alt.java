package me.lordpankake.aeon.ui.click.guiscreens.alts;
public class Alt
{
	private final String aUserName;
	private final String aPassword;
	private final boolean premium;

	public Alt(String username, String password)
	{
		this.premium = true;
		this.aUserName = username;
		this.aPassword = password;
	}

	public Alt(String username)
	{
		this.premium = false;
		this.aUserName = username;
		this.aPassword = "N/A";
	}

	public String getFileLine()
	{
		if (this.premium)
		{
			return this.aUserName.concat(":").concat(this.aPassword);
		} else
		{
			return this.aUserName;
		}
	}

	public String getUsername()
	{
		return this.aUserName;
	}

	public String getPassword()
	{
		if (this.premium)
		{
			return this.aPassword;
		} else
		{
			return "";
		}
	}

	public boolean isPremium()
	{
		return this.premium;
	}
}
