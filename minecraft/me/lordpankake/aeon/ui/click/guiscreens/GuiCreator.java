package me.lordpankake.aeon.ui.click.guiscreens;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glDisable;
import java.io.File;
import java.util.ArrayList;
import org.lwjgl.opengl.GL11;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.util.FileUtils;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.ResourceLocation;

public class GuiCreator extends GuiScreen
{
	private static ResourceLocation overlayTexture = new ResourceLocation("Aeon/MetalBG.png");
	public static ArrayList<Frame> frames = new ArrayList<Frame>();
	private final GuiScreen parentScreen;

	public GuiCreator(AeonOptions options)
	{
		this.parentScreen = options;
		PankakeGUI.loadFrames();
		loadFrames();
	}

	@Override
	public void initGui()
	{
		final int defaultWidth = 200;
		this.buttonList.add(new GuiButton(1, 0, this.height-20, 60, 20, "Back"));
		//this.buttonList.add(new GuiButton(2, 82, buttonY, 60, 20, "Ssss"));
		PankakeGUI.loadFrames();
		GuiCreator.frames = PankakeGUI.frames;
		loadFrames();
	}

	@Override
	protected void actionPerformed(GuiButton button)
	{
		if (button.buttonID == 1)
		{
			this.mc.displayGuiScreen(this.parentScreen);
			PankakeGUI.frames = GuiCreator.frames;
			PankakeGUI.saveFrames();
		} else
		{
			for (final Frame f : frames)
			{
				frames.get(0).setXY(55, 55);
			}
		}
	}

	@Override
	public void mouseClicked(int x, int y, int mode)
	{
		int i = -1;
		boolean found = false;
		for (final Frame f : frames)
		{
			// f.setXY(x, y);
			if (f.isMouseOver())
			{
				if (frames.indexOf(f) == frames.size() - 1)
				{
					found = true;
					for (final Frame f2 : frames)
					{
						if (f2 != f)
						{
							f2.isTopFrame = false;
							f2.isMouseOnGrip = false;
						}
					}
					f.isTopFrame = true;
					// f.sendClick(x, y);
					// return; //replace found boolean with return statement?
				} else
				{
					i = frames.indexOf(f);
				}
			}
		}
		Frame temp = null;
		if (i != -1 && !found)
		{
			temp = frames.get(i);
			frames.remove(i);
			frames.add(temp);
			frames.get(frames.size() - 1).isTopFrame = true;
			// frames.get(frames.size() - 1).sendClick(x, y);
		}
		super.mouseClicked(x, y, mode);
	}

	@Override
	public void drawScreen(int par1, int par2, float par3)
	{
		glEnable(GL_BLEND);
		renderFullscreenTexture();
		glDisable(GL_BLEND);
		for (final Frame f : frames)
		{
			f.update();
		}
		super.drawScreen(par1, par2, par3);
	}

	private void renderFullscreenTexture()
	{
		final ScaledResolution varRes = new ScaledResolution(this.mc, this.mc.displayWidth, this.mc.displayHeight);
		final int width = varRes.getScaledWidth();
		final int height = varRes.getScaledHeight();
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(false);
		OpenGlHelper.glBlendFunc(770, 771, 1, 0);
		GL11.glDisable(GL11.GL_ALPHA_TEST);
		this.mc.getTextureManager().bindTexture(overlayTexture);
		final Tessellator var3 = Tessellator.instance;
		var3.startDrawingQuads();
		var3.addVertexWithUV(0.0D, height, -90.0D, 0.0D, 1.0D);
		var3.addVertexWithUV(width, height, -90.0D, 1.0D, 1.0D);
		var3.addVertexWithUV(width, 0.0D, -90.0D, 1.0D, 0.0D);
		var3.addVertexWithUV(0.0D, 0.0D, -90.0D, 0.0D, 0.0D);
		var3.draw();
		GL11.glDepthMask(true);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
	}

	public static void loadFrames()
	{
		Log.write("Loading frames");
		final ArrayList<String> penis = (ArrayList<String>) FileUtils.read(new File("Aeon" + File.separator + "GUI" + File.separator + "Frames.txt"));
		for (final String line : penis)
		{
			try
			{
				final String[] splitInfo = line.toLowerCase().split(":");
				final String frameName = splitInfo[0];
				final int frameX = Integer.parseInt(splitInfo[1]);
				final int frameY = Integer.parseInt(splitInfo[2]);
				final boolean isOpen = Boolean.parseBoolean(splitInfo[3]);
				for (final Frame f : frames)
				{
					if (f.getName().equalsIgnoreCase(frameName))
					{
						f.setXY(frameX, frameY);
						f.setOpen(isOpen);
					}
				}
			} catch (final Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
