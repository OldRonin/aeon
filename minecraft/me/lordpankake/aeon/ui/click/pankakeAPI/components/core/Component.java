package me.lordpankake.aeon.ui.click.pankakeAPI.components.core;


import java.awt.Color;
import java.awt.Rectangle;
import me.lordpankake.aeon.util.RenderUtils;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Mouse;

public class Component
{
	public Color compColor = new Color(34, 38, 40, 111);
	public Color compOutlineColor = new Color(11, 11, 11, 60);
	protected Rectangle area;
	protected final String name;
	protected Component parent;
	protected boolean record, recordLock;

	public Component(Rectangle rekt, String name)
	{
		this.area = rekt;
		this.name = name;
		this.parent = null;
	}

	public Component(Rectangle rekt, String name, Component parent)
	{
		this.area = rekt;
		this.name = name;
		this.parent = parent;
	}

	public String getName()
	{
		return name;
	}

	public int getX()
	{
		return area.x;
	}

	public int getY()
	{
		return area.y;
	}

	public int getWidth()
	{
		return area.width;
	}

	public int getHeight()
	{
		return area.height;
	}

	public Rectangle getArea()
	{
		return this.area;
	}
	
	public Component getParent()
	{
		return this.parent;
	}

	public void setArea(Rectangle rekt)
	{
		this.area = rekt;
	}

	public void setXY(int x, int y)
	{
		this.area = new Rectangle(x, y, this.area.width, this.area.height);
	}

	public void setHeight(int height)
	{
		this.area = new Rectangle(this.area.x, this.area.y, this.area.width, height);
	}

	public void setColors(Color inner, Color outer)
	{
		this.compColor = inner;
		this.compOutlineColor = outer;
	}

	public boolean isMouseOver()
	{
		return isMouseOver(0, 0);
	}

	public boolean isMouseOver(int leewayX, int leewayY)
	{
		int offsetX = 0;
		int offsetY = 0;
		if (parent != null)
		{
			offsetX = parent.getX();
			offsetY = parent.getY();
		}
		final int mX = Mouse.getEventX() * Minecraft.getMinecraft().currentScreen.width / Minecraft.getMinecraft().displayWidth;
		final int mY = Minecraft.getMinecraft().currentScreen.height - Mouse.getEventY() * Minecraft.getMinecraft().currentScreen.height / Minecraft.getMinecraft().displayHeight - 1;
		if (mX + 1 > this.getX() + offsetX - leewayX && mX < this.getX() + this.getWidth() + offsetX + leewayX)
		{
			if (mY > this.getY() + offsetY - leewayY && mY < this.getY() + this.getHeight() + offsetY + 1 + leewayY)
			{
				return true;
			}
		}
		return false;
	}

	public void render()
	{
	}

	protected void update()
	{
	}
}
