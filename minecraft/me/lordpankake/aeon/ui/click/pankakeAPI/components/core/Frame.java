package me.lordpankake.aeon.ui.click.pankakeAPI.components.core;

import static org.lwjgl.opengl.GL11.glVertex2d;
import java.awt.Rectangle;
import java.util.ArrayList;
import net.minecraft.client.Minecraft;
import org.lwjgl.input.Mouse;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeGUI;
import me.lordpankake.aeon.ui.click.pankakeAPI.ThemeBase;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleButton;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ThemeButton;
import me.lordpankake.aeon.ui.click.pankakeAPI.theme.Simple;
import me.lordpankake.aeon.util.RenderUtils;

public class Frame extends Component
{
	private int savedX, savedY, savedMX, savedMY;
	public boolean childInUse;
	public final int frameGripHeight;
	public boolean isMouseOnGrip, isTopFrame;
	public boolean isOpen = true;
	public final ArrayList<Component> components = new ArrayList<Component>();

	public Frame(Rectangle rekt, String name)
	{
		super(rekt, name);
		frameGripHeight = PankakeGUI.theme.frameGripHeight;
		this.savedX = rekt.x;
		this.savedY = rekt.y;
		isOpen = false;
	}

	@Override
	public void setXY(int x, int y)
	{
		this.savedX = x;
		this.savedY = y;
	}

	public void addComponent(Component c)
	{
		components.add(c);
		this.setHeight(this.getHeight() + c.getHeight() + 2);
	}

	@Override
	public void render()
	{
		PankakeGUI.theme.onRender(this);
		
		// // is a fucking bitch
	}

	public void moveFrame()
	{
		final int mX = Mouse.getX() * Minecraft.getMinecraft().currentScreen.width / Minecraft.getMinecraft().displayWidth;
		final int mY = Minecraft.getMinecraft().currentScreen.height - Mouse.getY() * Minecraft.getMinecraft().currentScreen.height / Minecraft.getMinecraft().displayHeight - 1;
		if (!childInUse && this.isMouseOver() && isTopFrame && isMouseOnGrip && Mouse.isButtonDown(0) && !recordLock)
		{
			record = true;
			recordLock = true;
			savedMX = mX - this.getX();
			savedMY = mY - this.getY();
		}

		if (record)
		{
			if (PankakeGUI.frames.indexOf(this) == PankakeGUI.frames.size() - 1)
			{
				savedX = this.getX() - (this.getX() - mX) - savedMX;
				savedY = this.getY() - (this.getY() - mY) - savedMY;
			}
		}
		if (!Mouse.isButtonDown(0))
		{
			recordLock = false;
			record = false;
		}
		if (this.area.x != savedX || this.area.y != savedY)
		{
			this.area.x = savedX;
			this.area.y = savedY;
		}
	}

	@Override
	public Rectangle getArea()
	{
		if (this.getOpened())
		{
			return super.getArea();
		} else
		{
			return new Rectangle(this.getX(), this.getY(), this.getWidth(), PankakeGUI.theme.frameGripHeight + 2);
		}
	}

	public void update()
	{
		this.updateComponents();
		this.moveFrame();
		this.render();
	}

	private void updateComponents()
	{
		for (final Component c : components)
		{
			c.update();
		}
	}

	@Override
	public boolean isMouseOver()
	{
		final int mX = Mouse.getEventX() * Minecraft.getMinecraft().currentScreen.width / Minecraft.getMinecraft().displayWidth;
		final int mY = Minecraft.getMinecraft().currentScreen.height - Mouse.getEventY() * Minecraft.getMinecraft().currentScreen.height / Minecraft.getMinecraft().displayHeight - 1;
		if (mX > this.getX() && mX < this.getX() + this.getWidth())
		{
			if (mY > this.getY() && mY < this.getY() + frameGripHeight)
			{
				isMouseOnGrip = true;
				if (this.getOpened())
				{
					return true;
				}
			} else if (mY > this.getY() && mY < this.getY() + this.getHeight())
			{
				isMouseOnGrip = false;
				if (!this.getOpened())
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return super.isMouseOver();
	}

	public void sendClick(int x, int y)
	{
		//x - 4
		//width - 18
		if ( x > this.getX() + this.getWidth() - PankakeGUI.theme.toggleRect.width - PankakeGUI.theme.toggleRect.x && x < this.getX() + this.getWidth() - PankakeGUI.theme.toggleRect.x)
		{
			if (y > this.getArea().y + PankakeGUI.theme.toggleRect.y && y < this.getArea().y + PankakeGUI.theme.toggleRect.y + PankakeGUI.theme.toggleRect.height)
			{
				this.isOpen = !this.isOpen;
			}
		}
		if (this.getOpened())
		{
			for (final Component c : components)
			{
				if (c.isMouseOver())
				{
					if (c instanceof Button)
					{
						((Button) c).onClicked();
					}
					if (c instanceof Slider)
					{
						((Slider) c).update = true;
						((Slider) c).sliding = true;
					}
				} else if (c instanceof Slider)
				{
					if (!((Slider) c).sliding)
					{
						((Slider) c).update = false;
					}
				}
			}
		}
	}

	public boolean getOpened()
	{
		return this.isOpen;
	}

	public void setOpen(boolean value)
	{
		this.isOpen = value;
	}
}
