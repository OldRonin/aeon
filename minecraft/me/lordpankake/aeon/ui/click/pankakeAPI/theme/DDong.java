package me.lordpankake.aeon.ui.click.pankakeAPI.theme;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2d;
import static org.lwjgl.opengl.GL11.glVertex2f;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import net.minecraft.client.Minecraft;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.ThemeBase;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleSlider;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Button;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Slider;
import me.lordpankake.aeon.util.RenderUtils;

public class DDong extends ThemeBase
{
	// Go back in Conversion/Simple and update to use PankakeDraw methods
	public DDong()
	{
		frameGripHeight = 12;
		buttonHeight = 15;
		frameWidth = 80;
		toggleRect = new Rectangle(4, 3, frameGripHeight - 2, frameGripHeight - 2);
		
			this.setFonts(new Font("Segoe UI", Font.PLAIN, 18), new Font("Segoe UI SemiBold", Font.PLAIN, 18), new Font("Segoe UI", Font.PLAIN, 15));
		
	}

	@Override
	public void onFrameRender(Frame f)
	{
		PankakeDraw.enableNoTextures();
		RenderUtils.setColor(new Color(77, 77, 77, 222));
		PankakeDraw.fillRoundedRect(f.getArea().x - 1, f.getArea().y - 1, f.getArea().x + f.getArea().width + 0.5F, f.getArea().y + f.getArea().height + 1, 8, new Color(42, 44, 44, 255));
		PankakeDraw.drawRoundedRect(f.getArea().x - 1, f.getArea().y - 1, f.getArea().x + f.getArea().width + 1, f.getArea().y + f.getArea().height + 1, 8, new Color(11, 11, 11, 255));
		PankakeDraw.fillRect(f.getArea().x - 1, f.getArea().y + f.getArea().height - 8, f.getArea().x + f.getArea().width + 0.5F, f.getArea().y + f.getArea().height + 1, new Color(42, 44, 44, 255));
		PankakeDraw.drawLine(f.getArea().x - 1, f.getArea().y + f.getArea().height - 8, f.getArea().x - 1, f.getArea().y + f.getArea().height + 1, new Color(11, 11, 11, 255));
		PankakeDraw.drawLine(f.getArea().x + f.getArea().width + 0.5F, f.getArea().y + f.getArea().height - 8, f.getArea().x + f.getArea().width + 1, f.getArea().y + f.getArea().height + 1, new Color(11, 11, 11, 255));
		PankakeDraw.drawLine(f.getArea().x - 1.5F, f.getArea().y + f.getArea().height + 1, f.getArea().x + f.getArea().width + 1, f.getArea().y + f.getArea().height + 1, new Color(11, 11, 11, 255));
		PankakeDraw.fillRoundedRect(f.getArea().x + f.getArea().width - toggleRect.x - toggleRect.width, f.getArea().y + toggleRect.y - 0.5F, f.getArea().x + f.getArea().width - toggleRect.x + 0.5F, f.getArea().y + toggleRect.y + toggleRect.height, 3, new Color(1, 1, 1, 50));
		glBegin(GL_LINES);
		{
			RenderUtils.setColor(new Color(255, 255, 255, 0));
			glVertex2f(f.getArea().x + 3, f.getArea().y - 0.5F);
			RenderUtils.setColor(new Color(255, 255, 255, 100));
			glVertex2f(f.getArea().x + f.getArea().width / 2, f.getArea().y - 0.5F);
		}
		glEnd();
		glBegin(GL_LINES);
		{
			glVertex2f(f.getArea().x + f.getArea().width / 2, f.getArea().y - 0.5F);
			RenderUtils.setColor(new Color(255, 255, 255, 0));
			glVertex2f(f.getArea().x + f.getArea().width - 3, f.getArea().y - 0.5F);
		}
		glEnd();
		PankakeDraw.disableNoTextures();
		PankakeDraw.drawLargeString(f.getName(), f.getX() + 2.5F, f.getY() + 3.5F, 0x222222);
		PankakeDraw.drawLargeString(f.getName(), f.getX() + 2, f.getY() + 3, 0xffffff);
		if (f.getOpened())
		{
			PankakeDraw.drawString("O", f.getX() + f.getWidth() - toggleRect.x - toggleRect.width + 1F, f.getY() + toggleRect.y + 1F, 0xffffff, false);
		} else
		{
			PankakeDraw.drawString("_", f.getX() + f.getWidth() - toggleRect.x - toggleRect.width + 2.5F, f.getY() - toggleRect.y + 2.5F, 0xffffff, false);
		}
	}

	@Override
	protected void onButtonRender(Button b)
	{
		int offsetX = b.getParent().getX() + b.getX();
		int offsetY = b.getParent().getY();
		PankakeDraw.enableNoTextures();
		Color glowColor = new Color(222, 222, 222, 100);
		Color glowColorTrans = new Color(222, 222, 222, 33);
		if (!b.getState())
		{
			glowColor = new Color(22, 22, 22, 188);
			glowColorTrans = new Color(188, 188, 188, 44);
		}
		PankakeDraw.fillGradientRect(offsetX, offsetY + b.getY() + 1, offsetX + b.getWidth() - 1, offsetY + b.getY() + b.getHeight(), new Color(55, 55, 55, 255), new Color(22, 24, 26, 255));
		PankakeDraw.drawRoundedRect(offsetX, offsetY + b.getY() + 0.5F, offsetX + b.getWidth() - 0.5F, offsetY + b.getY() + b.getHeight(), 1, glowColor);
		PankakeDraw.drawRoundedRect(offsetX, offsetY + b.getY() + 1F, offsetX + b.getWidth(), offsetY + b.getY() + b.getHeight() - 0.5F, 1, glowColorTrans);
		PankakeDraw.drawRoundedRect(offsetX, offsetY + b.getY() + 1F, offsetX + b.getWidth(), offsetY + b.getY() + b.getHeight() - 0.5F, 1, glowColorTrans);
		final int centeredX = offsetX + b.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getStringWidth(b.getName()) / 2;
		final int centeredY = b.getY() + offsetY + b.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (b.getState())
		{
			PankakeDraw.drawString(b.getName(), centeredX + 0.5F, centeredY + 0.5F, RenderUtils.toRGBA(new Color(0, 0, 0, 244)), false);
			PankakeDraw.drawString(b.getName(), centeredX, centeredY, RenderUtils.toRGBA(new Color(222, 222, 222, 244)), false);
		} else
		{
			PankakeDraw.drawString(b.getName(), centeredX + 0.5F, centeredY + 0.5F, RenderUtils.toRGBA(new Color(0, 0, 0, 244)), false);
			PankakeDraw.drawString(b.getName(), centeredX, centeredY, RenderUtils.toRGBA(new Color(88, 88, 88, 244)), false);
		}
	}

	@Override
	protected void onSliderRender(Slider s)
	{
		int offsetX = s.getParent().getX();
		int offsetY = s.getParent().getY();
		PankakeDraw.enableNoTextures();
		// PankakeDraw.fillGradientRect(offsetX, offsetY + b.getY() + 1, offsetX
		// + b.getWidth() - 1, offsetY + b.getY() + b.getHeight(), new Color(55,
		// 55, 55, 255), new Color(22, 24, 26, 255));
		final double sliderPercentage = (s.getValue() - s.getMin()) / (s.getMax() - s.getMin());
		glBegin(GL_QUADS);
		{
			RenderUtils.setColor(new Color(222, 222, 222, 55));
			glVertex2f(-0.5F + offsetX + s.getArea().x, offsetY + 0.5F + s.getArea().y);
			glVertex2f((float) (-0.5F + offsetX + s.getArea().x + 0.5F + (s.getArea().width * sliderPercentage)), 0.5F + offsetY + s.getArea().y);
			RenderUtils.setColor(new Color(188, 188, 188, 22));
			glVertex2f((float) (-0.5F + offsetX + s.getArea().x + 0.5F + (s.getArea().width * sliderPercentage)), 0.5F + offsetY + s.getArea().y + s.getArea().height);
			glVertex2f(-0.5F + offsetX + s.getArea().x, 0.5F + offsetY + s.getArea().y + s.getArea().height);
		}
		glEnd();
		PankakeDraw.drawRoundedRect(offsetX + s.getArea().x, offsetY + s.getArea().y, offsetX + s.getArea().x + s.getArea().width, offsetY + s.getArea().y + s.getArea().height, 1, new Color(222, 222, 222, 33));
		final int centeredX = s.getX() + offsetX + s.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(s.getName()) / 2;
		final int centeredY = s.getY() + (-1) + offsetY + s.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (s instanceof ModuleSlider)
		{
			PankakeDraw.drawSmallString(((ModuleSlider) s).getModule().getName() + ":" + s.getName(), offsetX + 3, centeredY + 2, 0xffffff);
		} else
		{
			PankakeDraw.drawSmallString(s.getName(), offsetX + 2, centeredY + 2, 0xffffff);
		}
		// String percent = ((this.value / this.max) * 100) + "";
		String dispText = ((s.getValue() - s.getMin()) / (s.getMax() - s.getMin())) * 100 + "";
		if (s.drawPercent && dispText.length() > 5)
		{
			dispText = dispText.substring(0, 5) + "%";
		} else
		{
			dispText = s.getValue() + "";
			if (dispText.length() > 5)
			{
				dispText = dispText.substring(0, 5);
			}
		}
		PankakeDraw.drawSmallString(dispText, offsetX + s.getWidth() - 3 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(dispText), centeredY + 2, 0xffffff);
	}
}
