package me.lordpankake.aeon.ui.click.pankakeAPI.theme;

import static org.lwjgl.opengl.GL11.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import me.lordpankake.aeon.core.Aeon;
import me.lordpankake.aeon.ui.click.pankakeAPI.PankakeDraw;
import me.lordpankake.aeon.ui.click.pankakeAPI.ThemeBase;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleButton;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.ModuleSlider;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Button;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Component;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Frame;
import me.lordpankake.aeon.ui.click.pankakeAPI.components.core.Slider;
import me.lordpankake.aeon.util.RenderUtils;
import net.minecraft.client.Minecraft;

public class Conversion extends ThemeBase
{
	public Color frameBGTop = new Color(66, 66, 66, 255);
	public Color frameBGTopBlack = new Color(22, 22, 22, 255);
	public Color frameBGTopSection = new Color(44, 44, 44, 255);
	public Color btnOffHigh = new Color(111, 111, 111, 255);
	public Color btnOffLow = new Color(66, 66, 66, 255);
	public Color btnOnHigh = new Color(99, 133, 155, 255);
	public Color btnOnLow = new Color(55, 77, 144, 255);

	public Conversion()
	{
		frameGripHeight = 18;
		buttonHeight = 14;
		sliderHeight = 16;
		frameWidth = 80;
		toggleRect = new Rectangle(4, 3, frameGripHeight - 4, frameGripHeight - 4);
		this.setFonts(new Font("Segoe UI", Font.PLAIN, 18), new Font("Segoe UI", Font.PLAIN, 30), new Font("Segoe UI", Font.PLAIN, 15));
	}

	@Override
	public void onFrameRender(Frame f)
	{
		PankakeDraw.enableNoTextures();
		RenderUtils.setColor(frameBGTopSection);
		glBegin(GL_QUADS);
		{
			glVertex2d(f.getArea().x, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y);
			glVertex2d(f.getArea().x + f.getArea().width, f.getArea().y + f.getArea().height);
			glVertex2d(f.getArea().x, f.getArea().y + f.getArea().height);
		}
		glEnd();
		int tiny = frameGripHeight / 4;
		glBegin(GL_QUADS);
		{
			RenderUtils.setColor(frameBGTop);
			glVertex2f(f.getArea().x, f.getArea().y);
			glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y);
			RenderUtils.setColor(frameBGTopBlack);
			glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y + (tiny * 2));
			glVertex2f(f.getArea().x, f.getArea().y + (tiny * 2));
		}
		glEnd();
		glBegin(GL_QUADS);
		{
			glVertex2f(f.getArea().x, f.getArea().y + (tiny * 2));
			glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y + (tiny * 2));
			glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y + (tiny * 3) + 1);
			glVertex2f(f.getArea().x, f.getArea().y + (tiny * 3) + 1);
		}
		glEnd();
		glBegin(GL_QUADS);
		{
			if (f.getOpened())
			{
				glVertex2f(f.getArea().x, f.getArea().y + (tiny * 3) + 1);
				glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y + (tiny * 3) + 1);
				RenderUtils.setColor(frameBGTopSection);
				glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y + (tiny * 6));
				glVertex2f(f.getArea().x, f.getArea().y + (tiny * 6));
			} else
			{
				glVertex2f(f.getArea().x, f.getArea().y + (tiny * 3) + 1);
				glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y + (tiny * 3) + 1);
				RenderUtils.setColor(new Color(37, 37, 37, 255));
				glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y + (tiny * 5));
				glVertex2f(f.getArea().x, f.getArea().y + (tiny * 5));
			}
		}
		glEnd();
		RenderUtils.setColor(f.compOutlineColor);
		glColor4f(0f, 0f, 0f, 1f);
		glBegin(GL_LINE_LOOP);
		{
			glVertex2f(f.getArea().x, f.getArea().y);
			glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y);
			RenderUtils.setColor(btnOffHigh);
			glVertex2f(f.getArea().x + f.getArea().width, f.getArea().y + f.getArea().height);
			glColor4f(0f, 0f, 0f, 1f);
			glVertex2f(f.getArea().x, f.getArea().y + f.getArea().height);
		}
		glEnd();
		final int letterWidth = Minecraft.getMinecraft().fontRenderer.getLargeGuiStringWidth(f.getName().substring(0, 1));
		final int stringWidth = Minecraft.getMinecraft().fontRenderer.getGuiStringWidth(f.getName().substring(1));
		glColor4f(0.1f, 0.1f, 0.1f, 0.5f);
		glBegin(GL_QUADS);
		{
			glVertex2d(f.getArea().x + 4, f.getArea().y + 3);
			glVertex2d(f.getArea().x + 8 + letterWidth + stringWidth, f.getArea().y + 3);
			glVertex2d(f.getArea().x + 8 + letterWidth + stringWidth, f.getArea().y + 17);
			glVertex2d(f.getArea().x + 4, f.getArea().y + 17);
		}
		glEnd();
		glColor4f(1f, 1f, 1f, 0.1f);
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(f.getArea().x + 4, f.getArea().y + 3);
			glVertex2d(f.getArea().x + 8 + letterWidth + stringWidth, f.getArea().y + 3);
			glVertex2d(f.getArea().x + 8 + letterWidth + stringWidth, f.getArea().y + 17);
			glVertex2d(f.getArea().x + 4, f.getArea().y + 17);
		}
		glEnd();
		glColor4f(1f, 1f, 1f, 0.1f);
		glBegin(GL_LINE_LOOP);
		{
			glVertex2d(f.getX() + f.getWidth() - toggleRect.x, f.getArea().y + toggleRect.y);
			glVertex2d(f.getX() + f.getWidth() - toggleRect.x - toggleRect.width, f.getArea().y + toggleRect.y);
			glVertex2d(f.getX() + f.getWidth() - toggleRect.x - toggleRect.width, f.getArea().y + toggleRect.y + toggleRect.height);
			glVertex2d(f.getX() + f.getWidth() - toggleRect.x, f.getArea().y + toggleRect.y + toggleRect.height);
		}
		glEnd();
		glBegin(GL_LINES);
		{
			RenderUtils.setColor(new Color(199, 199, 199, 60));
			glVertex2d(f.getArea().x + 14 + f.getArea().width / 2, f.getArea().y + 1);
			RenderUtils.setColor(new Color(199, 199, 199, 0));
			glVertex2d(f.getArea().x + 14 + f.getArea().width / 2, f.getArea().y + 20);
		}
		glEnd();
		glBegin(GL_LINES);
		{
			RenderUtils.setColor(new Color(199, 199, 199, 60));
			glVertex2d(f.getArea().x + 16 + f.getArea().width / 2, f.getArea().y + 1);
			RenderUtils.setColor(new Color(199, 199, 199, 0));
			glVertex2d(f.getArea().x + 16 + f.getArea().width / 2, f.getArea().y + 20);
		}
		glEnd();
		glBegin(GL_LINES);
		{
			RenderUtils.setColor(new Color(199, 199, 199, 60));
			glVertex2d(f.getArea().x + 18 + f.getArea().width / 2, f.getArea().y + 1);
			RenderUtils.setColor(new Color(199, 199, 199, 0));
			glVertex2d(f.getArea().x + 18 + f.getArea().width / 2, f.getArea().y + 20);
		}
		glEnd();
		PankakeDraw.disableNoTextures();
		final int centeredX = f.getX() + 8 - (letterWidth / 2);
		if (f.getOpened())
		{
			PankakeDraw.drawLargeString("O", f.getX() + f.getWidth() - 17F, f.getY() + 2.5F, 0x000000);
			PankakeDraw.drawLargeString("O", f.getX() + f.getWidth() - 17.5F, f.getY() + 2, 0xffffff);
		} else
		{
			PankakeDraw.drawLargeString("_", f.getX() + f.getWidth() - 14.5F, f.getY() - 3.5F, 0x000000);
			PankakeDraw.drawLargeString("_", f.getX() + f.getWidth() - 15, f.getY() - 4, 0xffffff);
		}
		PankakeDraw.drawLargeString(f.getName().substring(0, 1), f.getX() + 4.5F, f.getY() + 3, 0x000000);
		PankakeDraw.drawLargeString(f.getName().substring(0, 1), f.getX() + 4, f.getY() + 2, 0xffffff);
		PankakeDraw.drawString(f.getName().substring(1), f.getX() + letterWidth + 6, f.getY() + 8.5F, 0x000000, false);
		PankakeDraw.drawString(f.getName().substring(1), f.getX() + letterWidth + 5, f.getY() + 8, 0xffffff, false);
	}

	@Override
	protected void onButtonRender(Button b)
	{
		int offsetX = b.getParent().getX() + b.getX();
		int offsetY = b.getParent().getY();
		PankakeDraw.enableNoTextures();
		glBegin(GL_QUADS);
		{
			if (b.getState())
			{
				RenderUtils.setColor(btnOnLow);
				glVertex2f(offsetX, offsetY + b.getArea().y);
				glVertex2f(offsetX + b.getArea().width, offsetY + b.getArea().y);
				RenderUtils.setColor(btnOnHigh);
				glVertex2f(offsetX + b.getArea().width, offsetY + b.getArea().y + b.getArea().height);
				glVertex2f(offsetX, offsetY + b.getArea().y + b.getArea().height);
			} else
			{
				RenderUtils.setColor(btnOffLow);
				glVertex2f(offsetX, offsetY + b.getArea().y);
				glVertex2f(offsetX + b.getArea().width, offsetY + b.getArea().y);
				RenderUtils.setColor(btnOffHigh);
				glVertex2f(offsetX + b.getArea().width, offsetY + b.getArea().y + b.getArea().height);
				glVertex2f(offsetX, offsetY + b.getArea().y + b.getArea().height);
			}
		}
		glEnd();
		RenderUtils.setColor(b.compOutlineColor);
		glColor4f(0f, 0f, 0f, 1f);
		glBegin(GL_LINE_LOOP);
		{
			if (b.getState())
			{
				RenderUtils.setColor(btnOnHigh);
				glVertex2f(offsetX, offsetY + b.getArea().y);
				glVertex2f(offsetX + b.getArea().width, offsetY + b.getArea().y);
				RenderUtils.setColor(new Color(88, 88, 88, 255));
				glVertex2f(offsetX + b.getArea().width, offsetY + b.getArea().y + b.getArea().height);
				glVertex2f(offsetX, offsetY + b.getArea().y + b.getArea().height);
			} else
			{
				RenderUtils.setColor(btnOffHigh);
				glVertex2f(offsetX, offsetY + b.getArea().y);
				glVertex2f(offsetX + b.getArea().width, offsetY + b.getArea().y);
				RenderUtils.setColor(new Color(88, 88, 88, 255));
				glVertex2f(offsetX + b.getArea().width, offsetY + b.getArea().y + b.getArea().height);
				glVertex2f(offsetX, offsetY + b.getArea().y + b.getArea().height);
			}
		}
		glEnd();
		final int centeredX = offsetX + b.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getGuiStringWidth(b.getName()) / 2;
		final int centeredY = 1 + b.getY() + offsetY + b.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		PankakeDraw.drawString(b.getName(), centeredX, centeredY - 0.5F, 0x222222, false);
		PankakeDraw.drawString(b.getName(), centeredX, centeredY, 0xffffff, false);
	}

	@Override
	protected void onSliderRender(Slider s)
	{
		int offsetX = s.getParent().getX();
		int offsetY = s.getParent().getY();
		PankakeDraw.enableNoTextures();
		final int drawY = s.getY() + (2) + offsetY + s.getArea().height / 2;
		RenderUtils.setColor(frameBGTopSection);
		offsetY = offsetY - 2;
		glBegin(GL_LINES);
		{
			glVertex2f(offsetX + s.getArea().x, drawY + 0.5F);
			RenderUtils.setColor(frameBGTopBlack);
			glVertex2f(offsetX + s.getArea().x + (s.getArea().width / 2), drawY + +0.5F);
		}
		glEnd();
		glBegin(GL_LINES);
		{
			glVertex2f(offsetX + s.getArea().x + (s.getArea().width / 2), drawY + +0.5F);
			RenderUtils.setColor(frameBGTopSection);
			glVertex2f(offsetX + s.getArea().x + (s.getArea().width), drawY + +0.5F);
		}
		glEnd();
		// /
		RenderUtils.setColor(frameBGTopSection);
		glBegin(GL_LINES);
		{
			glVertex2f(offsetX + s.getArea().x, drawY + 1);
			RenderUtils.setColor(btnOffHigh);
			glVertex2f(offsetX + s.getArea().x + (s.getArea().width / 2), drawY + 1);
		}
		glEnd();
		glBegin(GL_LINES);
		{
			glVertex2f(offsetX + s.getArea().x + (s.getArea().width / 2), drawY + 1);
			RenderUtils.setColor(frameBGTopSection);
			glVertex2f(offsetX + s.getArea().x + (s.getArea().width), drawY + 1);
		}
		glEnd();
		final double sliderPercentage = (s.getValue() - s.getMin()) / (s.getMax() - s.getMin());
		RenderUtils.setColor(btnOnHigh);
		glBegin(GL_QUADS);
		{
			glVertex2d(offsetX + s.getArea().x, drawY);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage), drawY);
			glVertex2d(offsetX + s.getArea().x + (s.getArea().width * sliderPercentage), drawY + 1);
			glVertex2d(offsetX + s.getArea().x, drawY + 1);
		}
		glEnd();
		PankakeDraw.disableNoTextures();
		final int centeredX = s.getX() - 2 + offsetX + s.getArea().width / 2 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(s.getName()) / 2;
		final int centeredY = s.getY() - 2 + offsetY + s.getArea().height / 2 - Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT / 2;
		if (s instanceof ModuleSlider)
		{
			PankakeDraw.drawSmallString(((ModuleSlider) s).getModule().getName() + ":" + s.getName(), offsetX + 3, centeredY - 0.5F, 0x000000);
			PankakeDraw.drawSmallString(((ModuleSlider) s).getModule().getName() + ":" + s.getName(), offsetX + 3, centeredY, 0xffffff);
		} else
		{
			PankakeDraw.drawSmallString(s.getName(), offsetX + 2, centeredY - 0.5F, 0x000000);
			PankakeDraw.drawSmallString(s.getName(), offsetX + 2, centeredY, 0xffffff);
		}
		// String percent = ((this.value / this.max) * 100) + "";
		String dispText = ((s.getValue() - s.getMin()) / (s.getMax() - s.getMin())) * 100 + "";
		if (s.drawPercent && dispText.length() > 5)
		{
			dispText = dispText.substring(0, 5) + "%";
		} else
		{
			dispText = s.getValue() + "";
			if (dispText.length() > 5)
			{
				dispText = dispText.substring(0, 5);
			}
		}
		PankakeDraw.drawSmallString(dispText, offsetX + s.getWidth() - 3 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(dispText), centeredY - 0.5F, 0x000000);
		PankakeDraw.drawSmallString(dispText, offsetX + s.getWidth() - 3 - Minecraft.getMinecraft().fontRenderer.getSmallGuiStringWidth(dispText), centeredY, 0xffffff);
		PankakeDraw.enableNoTextures();
		glBegin(GL_QUADS);
		{
			RenderUtils.setColor(new Color(166, 166, 166, 255));
			glVertex2f((float) (offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) - 1), (float) (8 + offsetY + s.getArea().y));
			glVertex2f((float) (offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) + 1), (float) (8 + offsetY + s.getArea().y));
			RenderUtils.setColor(new Color(111, 111, 111, 255));
			glVertex2f((float) (offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) + 1), (float) (offsetY + s.getArea().y + s.getArea().height));
			glVertex2f((float) (offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) - 1), (float) (offsetY + s.getArea().y + s.getArea().height));
		}
		glEnd();
		glColor4f(0f, 0f, 0f, 1f);
		glBegin(GL_LINE_LOOP);
		{
			RenderUtils.setColor(new Color(177, 177, 177, 255));
			glVertex2f((float) (offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) - 1), (float) (8 + offsetY + s.getArea().y));
			glVertex2f((float) (offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) + 1), (float) (8 + offsetY + s.getArea().y));
			RenderUtils.setColor(new Color(88, 88, 88, 255));
			glVertex2f((float) (offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) + 1), (float) (offsetY + s.getArea().y + s.getArea().height));
			glVertex2f((float) (offsetX + s.getArea().x + (s.getArea().width * sliderPercentage) - 1), (float) (offsetY + s.getArea().y + s.getArea().height));
		}
		glEnd();
		PankakeDraw.disableNoTextures();
	}
}
