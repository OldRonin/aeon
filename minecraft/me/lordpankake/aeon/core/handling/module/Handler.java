package me.lordpankake.aeon.core.handling.module;

import java.util.List;
import me.lordpankake.aeon.core.Log;
import me.lordpankake.aeon.modules.Module;

public class Handler
{
	// make get / set methods for this and use it for events
	// setter will be accesed from Template's baseEnable/baseDisable and the
	// below callEvent will loop only through that list
	public Initlizer hl;

	public Handler()
	{
		hl = new Initlizer();
	}

	public Initlizer getLoader()
	{
		return this.hl;
	}

	public Module getModule(String name)
	{
		for (final Module t : getModules())
		{
			if (t.getName().equalsIgnoreCase(name))
			{
				return t;
			}
		}
		return null;
	}

	// ---------------------------//
	// ------- Hack Lists --------//
	// ---------------------------//
	public List<Module> getModules()
	{
		return getLoader().hackList;
	}

	public List<Module> getRegisteredModules()
	{
		return getLoader().hackRegisteredList;
	}

	// ---------------------------//
	// --- Add / Remove Lists ----//
	// ---------------------------//
	public void replaceModule(Module hack)
	{
		for (final Module mod : getModules())
		{
			if (mod.getName().contains(hack.getName()))
			{
				final int index = this.getLoader().hackList.indexOf(mod);
				this.getLoader().hackList.remove(index);
				this.getLoader().hackList.add(index, hack);
				return;
			}
		}
	}

	public void addToRegistered(Module t)
	{
		Log.write(t.getName() + " is now registered!");
		getLoader().hackRegisteredList.add(t);
	}

	public void removeFromRegistered(Module t)
	{
		if (getLoader().hackRegisteredList.contains(t))
		{
			getLoader().hackRegisteredList.remove(t);
		}
	}
}
