package me.lordpankake.aeon.core.update;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import me.lordpankake.aeon.core.AeonSettings;
import me.lordpankake.aeon.core.Log;

public class Updater
{
	private static String versionURL = "https://dl.dropboxusercontent.com/s/0zxrxct4cykmrqn/version.txt";
	public static String version = AeonSettings.version;
	public static String downloadURL = "https://dl.dropbox.com/s/9lhwuqlx9t6t95x/urlError.html";
	public static String websiteURL = "https://bitbucket.org/lordpankake/aeon/wiki/Home";

	public static boolean upToDate()
	{
		Log.write("VERSION: " + Double.parseDouble(AeonSettings.version) + ":" + Double.parseDouble(version));
		if (Double.parseDouble(AeonSettings.version) >= Double.parseDouble(version))
		{
			return true;
		}
		return false;
	}

	public static boolean connected()
	{
		return !downloadURL.contains("Error.html");
	}

	public static boolean testConnectivity(String site)
	{
		final Socket sock = new Socket();
		final InetSocketAddress addr = new InetSocketAddress(site, 80);
		try
		{
			sock.connect(addr, 3000);
			return true;
		} catch (final IOException e)
		{
			return false;
		} finally
		{
			try
			{
				sock.close();
			} catch (final IOException e)
			{
			}
		}
	}

	public static void loadText()
	{
		try
		{
			final String url = versionURL;
			final URL obj = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
			conn.setReadTimeout(5000);
			conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			conn.addRequestProperty("User-Agent", "Mozilla");
			conn.addRequestProperty("Referer", "google.com");
			Log.write("Request URL ... " + url);
			boolean redirect = false;
			// normally, 3xx is redirect
			final int status = conn.getResponseCode();
			if (status != HttpURLConnection.HTTP_OK)
			{
				if (status == HttpURLConnection.HTTP_MOVED_TEMP || status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)
				{
					redirect = true;
				}
			}
			Log.write("Response Code ... " + status);
			if (redirect)
			{
				// get redirect url from "location" header field
				final String newUrl = conn.getHeaderField("Location");
				// get the cookie if need, for login
				final String cookies = conn.getHeaderField("Set-Cookie");
				// open the new connnection again
				conn = (HttpURLConnection) new URL(newUrl).openConnection();
				conn.setRequestProperty("Cookie", cookies);
				conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
				conn.addRequestProperty("User-Agent", "Mozilla");
				conn.addRequestProperty("Referer", "google.com");
				Log.write("Redirect to URL : " + newUrl);
			}
			final BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			final StringBuffer html = new StringBuffer();
			while ((line = in.readLine()) != null)
			{
				try
				{
					if (line.toLowerCase().contains("version"))
					{
						version = line.split(";")[1];
					}
					if (line.toLowerCase().contains("downloadurl"))
					{
						downloadURL = line.split(";")[1];
					}
				} catch (final Exception e)
				{
					e.printStackTrace();
				}
			}
			in.close();
		} catch (final Exception e)
		{
			e.printStackTrace();
		}
	}
}
